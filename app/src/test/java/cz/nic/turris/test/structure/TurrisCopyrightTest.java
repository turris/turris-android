/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.test.structure;

import java.util.ArrayList;
import java.util.List;

import cz.nic.util.test.structure.AbstractCopyrightTest;

public class TurrisCopyrightTest extends AbstractCopyrightTest {

    private static final List<String>   EXCLUDED_FILES      = new ArrayList<String>() {{
        add("build");
        add("R.java");
        add("BuildConfig.java");
        add("gradle-wrapper.properties");
        add("Sentry-Client");
    }};
    private static final String[]       PATHS             = new String[]{"../", "src/", "../", "../"};
    private static final String[]       EXTENSIONS        = new String[]{JAVA_EXTENSION, XML_EXTENSION, GRADLE_EXTENSION, YML_EXTENSION};

    public TurrisCopyrightTest() {
        super(PATHS, EXTENSIONS, EXCLUDED_FILES);
    }
}
