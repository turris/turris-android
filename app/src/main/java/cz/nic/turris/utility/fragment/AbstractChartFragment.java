/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.utility.fragment;

import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import cz.nic.turris.R;
import cz.nic.turris.utility.Utility;

public abstract class AbstractChartFragment extends Fragment {

    private static final int                CHART_LABELS_MAX_X_COUNT        = 5;
    private static final int                CHART_LABELS_MAX_Y_COUNT        = 5;
    private static final float              CHART_LABELS_TEXT_SIZE          = 8f;
    private static final int                CHART_Y_MINIMUM_VALUE           = 0;
    private static final int                CHART_INNER_HORIZONTAL_SPACING  = 20;
    private static final int                CHART_DESCRIPTION_POSITION_X    = 45;
    private static final int                CHART_DESCRIPTION_POSITION_Y    = 20;
    private static final Paint.Align        CHART_DESCRIPTION_ALIGN         = Paint.Align.LEFT;
    private static final float              CHART_GRID_DASH_SPACING         = 10f;
    private static final float              CHART_LINE_WIDTH                = 1.2f;
    private static final LineDataSet.Mode   CHART_LINE_MODE                 = LineDataSet.Mode.HORIZONTAL_BEZIER;
    private static final int                CHART_ANIMATION_DURATION        = 1000;

    private LineChart   chart;
    private boolean     chartInitialized;


//////////////////////////// ANDROID LIFECYCLE

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initChart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.chart_fragment, container, false);
        chart = (LineChart) layout.findViewById(R.id.chart_fragment_chart);

        return layout;
    }


//////////////////////////// ABSTRACT CHART

    protected abstract int prepareTitle();


//////////////////////////// CHART AXIS

    private IAxisValueFormatter prepareXValueFormatter() {
        return (value, axis) -> getXAxisFormattedValues(value);
    }

    private IAxisValueFormatter prepareYValueFormatter() {
        return (value, axis) -> value == 0 ? Utility.EMPTY_STRING : getYAxisFormattedValues(value);
    }

    protected String getXAxisFormattedValues(float value) {
        return String.valueOf(value);
    }

    protected String getYAxisFormattedValues(float value) {
        return String.valueOf(value);
    }


//////////////////////////// CHART

    protected static class ChartData {

        private final String                name;
        private final Map<Double, Double>   series;
        private final int                   color;
        private final boolean               filled;

        public ChartData(String name, Map<Double, Double> series, int color, boolean filled) {
            this.name = name;
            this.series = series;
            this.color = color;
            this.filled = filled;
        }
    }

    protected int prepareChartMaxXCount() {
        return CHART_LABELS_MAX_X_COUNT;
    }

    protected Long prepareChartMaximum() {
        return null;
    }

    protected void updateChartMaximum(Long maximum) {
        if (maximum != null) {
            chart.getAxisRight().setAxisMaximum(maximum);
        }
    }

    protected Entry prepareChartEntry(Double x, Double y) {
        return new Entry(x.floatValue(), y.floatValue());
    }

    protected void updateChart(List<ChartData> chartDataList, boolean checkVisibility) {
        if (!checkVisibility || isVisible()) {
            updateViewInUIThread(chartDataList);
        }
    }

    private void updateViewInUIThread(List<ChartData> chartDataList) {
        getActivity().runOnUiThread(() -> updateView(chartDataList));
    }

    protected void updateView(List<ChartData> chartDataList) {
        if (isAdded()) {
            LineData data = new LineData();
            int chartXCount = 0;
            for (ChartData chartData : chartDataList) {

                List<Entry> entries = new ArrayList<>();
                List<Double> keyList = new ArrayList<>(chartData.series.keySet());

                if (keyList.size() > 0) {
                    Collections.sort(keyList);
                    //noinspection Convert2streamapi
                    for (Double valueKey: keyList) {
                        entries.add(prepareChartEntry(valueKey, chartData.series.get(valueKey)));
                    }

                    LineDataSet dataSet = new LineDataSet(entries, chartData.name);
                    int color = ContextCompat.getColor(getContext(), chartData.color);
                    dataSet.setColor(color);
                    dataSet.setHighlightEnabled(false);
                    dataSet.setDrawHighlightIndicators(false);
                    dataSet.setDrawCircles(false);
                    dataSet.setDrawValues(false);
                    dataSet.setMode(CHART_LINE_MODE);
                    dataSet.setDrawFilled(chartData.filled);
                    dataSet.setLineWidth(CHART_LINE_WIDTH);
                    dataSet.setFillColor(color);
                    dataSet.setAxisDependency(YAxis.AxisDependency.RIGHT);
                    data.addDataSet(dataSet);

                    chart.setData(data);

                    if (chartData.series.size() > chartXCount) {
                        chartXCount = chartData.series.size();
                        if (chartXCount > prepareChartMaxXCount()) {
                            chartXCount = prepareChartMaxXCount();
                        }
                    }
                }
            }
            chart.getXAxis().setLabelCount(chartXCount, true);
            refreshChart();
        }
    }

    private void refreshChart() {
        if (chartInitialized) {
            chart.invalidate();
            return;
        }
        chartInitialized = true;
        chart.animateX(CHART_ANIMATION_DURATION);
    }

    private void initChart() {
        /* left axis settings */
        YAxis axisLeft = chart.getAxisLeft();
        axisLeft.setDrawLabels(false);
        axisLeft.setDrawGridLines(false);
        axisLeft.setDrawAxisLine(false);

        /* right axis settings */
        YAxis axisRight = chart.getAxisRight();
        axisRight.setTextColor(ContextCompat.getColor(getContext(), R.color.chart_labels));
        axisRight.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        axisRight.setValueFormatter(prepareYValueFormatter());
        axisRight.setLabelCount(CHART_LABELS_MAX_Y_COUNT, true);
        axisRight.setTextSize(CHART_LABELS_TEXT_SIZE);
        axisRight.setAxisMinimum(CHART_Y_MINIMUM_VALUE);
        axisRight.setDrawAxisLine(false);
        updateChartMaximum(prepareChartMaximum());

        /* x axis settings */
        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setValueFormatter(prepareXValueFormatter());
        xAxis.setTextSize(CHART_LABELS_TEXT_SIZE);
        xAxis.setDrawGridLines(true);
        xAxis.enableGridDashedLine(CHART_GRID_DASH_SPACING, CHART_GRID_DASH_SPACING, CHART_GRID_DASH_SPACING);
        xAxis.setGridColor(ContextCompat.getColor(getContext(), R.color.chart_grid_vertical));
        xAxis.setDrawAxisLine(false);
        xAxis.setSpaceMax(CHART_INNER_HORIZONTAL_SPACING);
        xAxis.setSpaceMin(CHART_INNER_HORIZONTAL_SPACING);
        xAxis.setAvoidFirstLastClipping(true);
        xAxis.setTextColor(ContextCompat.getColor(getContext(), R.color.chart_labels));

        /* legend settings */
        chart.getLegend().setTextColor(ContextCompat.getColor(getContext(), R.color.chart_legend));

        /* description settings */
        Description desc = new Description();
        desc.setTextColor(ContextCompat.getColor(getContext(), R.color.chart_description));
        desc.setText(getString(prepareTitle()));
        desc.setTextAlign(CHART_DESCRIPTION_ALIGN);
        desc.setPosition(CHART_DESCRIPTION_POSITION_X, CHART_DESCRIPTION_POSITION_Y);

        /* chart settings */
        chart.setTouchEnabled(false);
        chart.setDragEnabled(false);
        chart.setScaleEnabled(false);
        chart.setDescription(desc);
        chart.setDrawBorders(false);
        chart.setDrawGridBackground(false);
        chart.setNoDataText(getString(R.string.chart_no_data_message));
        chart.setNoDataTextColor(ContextCompat.getColor(getContext(), R.color.chart_no_data_text));
        chart.invalidate();
    }
}
