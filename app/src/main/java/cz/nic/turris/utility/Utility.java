/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.utility;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.LongSparseArray;
import android.text.Html;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.Locale;

import cz.nic.turris.BuildConfig;
import cz.nic.turris.R;
import cz.nic.turris.TurrisLog;

public final class Utility {

    public static final int 	LOAD_MAXIMAL_VALUE  = 100;
    public static final int     MILLIS_COUNT        = 1000;
    public static final String  EMPTY_STRING        = "";
    public static final String  SPACE_STRING        = " ";
    public static final String  LINE_SEPARATOR      = System.getProperty("line.separator");

    private static final DecimalFormat  ONE_DECIMAL_NUMBER_FORMATTER    = new DecimalFormat("#.#");
    private static final int            STYLE_FALLBACK_COLOR            = Color.BLACK;

    private Utility() {}

    public static boolean isStringEmpty(String string) {
        return string == null || string.length() <= 0;
    }

    public static boolean isNotStringEmpty(String string) {
        return !isStringEmpty(string);
    }

    public static String getOneDecimalFormattedNumber(Number value) {
        return ONE_DECIMAL_NUMBER_FORMATTER.format(value);
    }

    public static int getColorFromStyle(Context context, int styleResourceId) {
        int[] attr = {android.R.attr.textColor};
        return context.obtainStyledAttributes(styleResourceId, attr).getColor(0, STYLE_FALLBACK_COLOR);
    }

    public static <T> boolean equalsLongSparseArrays(LongSparseArray<T> sparseArray1, LongSparseArray<T> sparseArray2) {
        if (sparseArray1 == null || sparseArray2 == null) {
            return false;
        }
        if (sparseArray1.size() != sparseArray2.size()) {
            return false;
        }
        boolean result = true;
        for (int i = 0; i < sparseArray1.size(); i++) {
            long key = sparseArray1.keyAt(i);
            result = sparseArray1.get(key).equals(sparseArray2.get(key));
            if (!result) {
                break;
            }
        }
        return result;
    }

    @TargetApi(Build.VERSION_CODES.N)
    public static Locale getCurrentLocale(Context context){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            return context.getResources().getConfiguration().getLocales().get(0);
        } else{
            //noinspection deprecation
            return context.getResources().getConfiguration().locale;
        }
    }

    @TargetApi(Build.VERSION_CODES.N)
    public static CharSequence fromHtml(String html){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            //noinspection deprecation
            return Html.fromHtml(html);
        }

    }

    public static float getFloatFromDimenItem(Resources resources, int resourceId) {
        TypedValue outValue = new TypedValue();
        resources.getValue(resourceId, outValue, true);
        return outValue.getFloat();
    }

    public static int getDimensionWithoutDensity(Resources resources, int dimension) {
        return (int) (resources.getDimension(dimension) / resources.getDisplayMetrics().density);
    }

    public static String getApplicationVersionString() {
        return BuildConfig.APPLICATION_FULL_NAME + "-" + BuildConfig.VERSION_NAME;
    }

    public enum SnackbarNotification {

        OK      (R.color.snackbar_ok, R.color.snackbar_ok_action) {
            @Override
            public Snackbar showSnackbar(Context context, View parentView, String message, Class clazz, int duration) {
                TurrisLog.info(clazz, SNACKBAR_NOTIFICATION_PREFIX + message);
                return super.showSnackbar(context, parentView, message, clazz, duration);
            }
        },
        INFO    (R.color.snackbar_info, R.color.snackbar_info_action) {
            @Override
            public Snackbar showSnackbar(Context context, View parentView, String message, Class clazz, int duration) {
                TurrisLog.info(clazz, SNACKBAR_NOTIFICATION_PREFIX + message);
                return super.showSnackbar(context, parentView, message, clazz, duration);
            }
        },
        ERROR   (R.color.snackbar_error, R.color.snackbar_error_action) {
            @Override
            public Snackbar showSnackbar(Context context, View parentView, String message, Class clazz, int duration) {
                //TODO mates: log message in default language
                TurrisLog.error(clazz, message);
                return super.showSnackbar(context, parentView, message, clazz, duration);
            }
        };

        private static final String SNACKBAR_NOTIFICATION_PREFIX    = "Snackbar notification: ";
        private static final int    SNACKBAR_DEFAULT_DURATION       = Snackbar.LENGTH_LONG;
        private static final int    SNACKBAR_MAX_LINES              = 3;

        private int colorId;
        private int actionTextColor;

        SnackbarNotification(int colorId, int actionTextColor) {
            this.colorId = colorId;
            this.actionTextColor = actionTextColor;
        }

        public Snackbar showSnackbar(Context context, View parentView, int messageId, Class clazz, int duration) {
            return showSnackbar(context, parentView, context.getString(messageId), clazz, duration);
        }

        public Snackbar showSnackbar(Context context, View parentView, int messageId, Class clazz) {
            return showSnackbar(context, parentView, context.getString(messageId), clazz);
        }

        public Snackbar showSnackbar(Context context, View parentView, String message, Class clazz) {
            return showSnackbar(context, parentView, message, clazz, SNACKBAR_DEFAULT_DURATION);
        }

        public Snackbar showSnackbar(Context context, View parentView, String message, Class clazz, int duration) {
            if (Utility.isNotStringEmpty(message) && parentView != null) {
                Snackbar snackbar = Snackbar.make(parentView, message, duration);
                snackbar.setActionTextColor(ContextCompat.getColor(context, actionTextColor));

                View snackbarView = snackbar.getView();
                snackbarView.setBackgroundColor(ContextCompat.getColor(context, colorId));
                ((TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text)).setMaxLines(SNACKBAR_MAX_LINES);

                return snackbar;
            }
            return null;
        }
    }

}
