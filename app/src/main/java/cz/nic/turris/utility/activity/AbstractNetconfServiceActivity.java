/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.utility.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import cz.nic.turris.connectors.DataRefreshService;

public abstract class AbstractNetconfServiceActivity extends AbstractTurrisActivity {

    private ServiceConnection netconfServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            netconfServiceBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            netconfServiceBound = false;
        }
    };

    private void bindService() {
        bindService(new Intent(this, DataRefreshService.class), netconfServiceConnection, Context.BIND_AUTO_CREATE);
    }

    private void unbindService() {
        if (netconfServiceBound) {
            unbindService(netconfServiceConnection);
            netconfServiceBound = false;
        }
    }

    private boolean netconfServiceBound = false;


//////////////////////////// ANDROID LIFECYCLE

    @Override
    protected void onStart() {
        super.onStart();
        bindService();
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        /* service does not have to be unbind in onStop */
        unbindService();
    }
}
