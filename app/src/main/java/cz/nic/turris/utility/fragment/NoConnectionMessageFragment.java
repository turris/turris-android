/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.utility.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import org.greenrobot.eventbus.Subscribe;

import cz.nic.turris.R;
import cz.nic.turris.utility.activity.AbstractTurrisActivity;
import cz.nic.turris.connectors.DataRefreshService;

public class NoConnectionMessageFragment extends Fragment {

    private View                layout;
    private Animation           fadeInAnimation;
    private Animation           fadeOutAnimation;


//////////////////////////// SHOW/HIDE

    private void showNoConnectionMessage(boolean isVisible, boolean animation) {
        getActivity().runOnUiThread(() -> {
            if (isVisible && layout.getVisibility() != View.VISIBLE) {
                if (animation) {
                    layout.startAnimation(fadeInAnimation);
                } else {
                    layout.setVisibility(View.VISIBLE);
                }
            } else if (!isVisible && layout.getVisibility() == View.VISIBLE) {
                if (animation) {
                    layout.startAnimation(fadeOutAnimation);
                } else {
                    layout.setVisibility(View.GONE);
                }
            }
        });
    }


//////////////////////////// EVENT HANDLING

    @Subscribe
    public void handleNetconfServiceConnectionEvent(DataRefreshService.NetconfServiceConnectionEvent netconfServiceConnectionEvent) {
        processNetconfServiceConnectionEvent(netconfServiceConnectionEvent, true);
    }

    private void processNetconfServiceConnectionEvent(DataRefreshService.NetconfServiceConnectionEvent netconfServiceConnectionEvent, boolean animation) {
        getActivity().runOnUiThread(() -> showNoConnectionMessage(netconfServiceConnectionEvent == null || !netconfServiceConnectionEvent.isConnected(), animation));
    }


//////////////////////////// ANDROID LIFECYCLE

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return layout = inflater.inflate(R.layout.noconnection_fragment, container, false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fadeInAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
        fadeInAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationRepeat(Animation animation) {}
            @Override
            public void onAnimationEnd(Animation animation) {}

            @Override
            public void onAnimationStart(Animation animation) {
                layout.setVisibility(View.VISIBLE);
            }
        });

        fadeOutAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
        fadeOutAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationRepeat(Animation animation) {}
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
                layout.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        processNetconfServiceConnectionEvent(((AbstractTurrisActivity) getActivity()).getTurrisApplication().getEventBus().getStickyEvent(DataRefreshService.NetconfServiceConnectionEvent.class), false);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AbstractTurrisActivity) getActivity()).getTurrisApplication().getEventBus().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        ((AbstractTurrisActivity) getActivity()).getTurrisApplication().getEventBus().unregister(this);
    }
}
