/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.utility.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cz.nic.turris.R;

public abstract class AbstractInfoFragment extends Fragment {

    protected abstract Fragment prepareTopFragment();

    protected abstract Fragment prepareMiddleFragment();

    protected abstract Fragment prepareBottomFragment();

    private void replaceFragment(FragmentTransaction fragmentTransaction, Fragment fragment, int containerId) {
        if (fragment != null) {
            fragmentTransaction.replace(containerId, fragment);
        }
    }


//////////////////////////// ANDROID LIFECYCLE

    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View layout = inflater.inflate(R.layout.info_fragment, container, false);

        if (savedInstanceState == null) {
            FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
            replaceFragment(fragmentTransaction, prepareTopFragment(),     R.id.info_fragment_top);
            replaceFragment(fragmentTransaction, prepareMiddleFragment(),  R.id.info_fragment_middle);
            replaceFragment(fragmentTransaction, prepareBottomFragment(),  R.id.info_fragment_bottom);
            fragmentTransaction.commit();
        }

		return layout;
	}
}
