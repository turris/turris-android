/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.utility.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.github.mikephil.charting.data.Entry;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.List;

import cz.nic.turris.utility.Utility;

public abstract class AbstractTimeChartFragment extends AbstractChartFragment {

    private static final DateTimeFormatter  DATE_TIME_FORMATTER = DateTimeFormat.forPattern("HH:mm:ss");

    private DateTimeFormatter   dateTimeLocaleFormatter;
    private long                currentTime;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dateTimeLocaleFormatter = DATE_TIME_FORMATTER.withLocale(Utility.getCurrentLocale(getContext()));
    }

    @Override
    protected void updateView(List<ChartData> chartDataList) {
        currentTime = System.currentTimeMillis() / Utility.MILLIS_COUNT;
        super.updateView(chartDataList);
    }

    @Override
    protected Entry prepareChartEntry(Double x, Double y) {
        Double xValue = x - currentTime;
        return new Entry(xValue.floatValue(), y.floatValue());
    }

    @Override
    protected String getXAxisFormattedValues(float value) {
        return new DateTime(((long)value + currentTime) * Utility.MILLIS_COUNT).toString(dateTimeLocaleFormatter);
    }
}
