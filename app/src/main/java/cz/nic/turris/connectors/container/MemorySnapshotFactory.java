/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.connectors.container;

import java.util.HashMap;
import java.util.Map;

import cz.nic.turris.connectors.NetconfTagDefinition;

public class MemorySnapshotFactory extends AbstractSnapshotArrayContainerFactory<MemorySnapshotFactory.MemorySnapshot> {

	@Override
	protected String prepareSnapshotParameterNodeName() {
		return NetconfTagDefinition.SNAPSHOT_MEMORY;
	}

	@Override
	protected void prepareNodeActions(Map<String, SnapshotNodeAction<MemorySnapshotFactory.MemorySnapshot>> nodeActions) {
		nodeActions.put(NetconfTagDefinition.SNAPSHOT_MEMORY_TOTAL, (memorySnapshot, node, time) -> memorySnapshot.total = Double.parseDouble(node.getTextContent()));
		nodeActions.put(NetconfTagDefinition.SNAPSHOT_MEMORY_FREE, (memorySnapshot, node, time) -> memorySnapshot.free.put((double) time, Double.parseDouble(node.getTextContent())));
		nodeActions.put(NetconfTagDefinition.SNAPSHOT_MEMORY_BUFFERS, (memorySnapshot, node, time) -> memorySnapshot.buffered.put((double) time, Double.parseDouble(node.getTextContent())));
		nodeActions.put(NetconfTagDefinition.SNAPSHOT_MEMORY_CACHED, (memorySnapshot, node, time) -> memorySnapshot.cached.put((double) time, Double.parseDouble(node.getTextContent())));
	}

	@Override
	protected MemorySnapshot prepareOutputContainer() {
		return new MemorySnapshot();
	}


//////////////////////////// DATA CONTAINER

	public static class MemorySnapshot extends AbstractContainerFactory.Container {

		private double				total;
		private Map<Double, Double>	free;
		private Map<Double, Double>	buffered;
		private Map<Double, Double>	cached;

		private MemorySnapshot() {
			free 		= new HashMap<>();
			buffered 	= new HashMap<>();
			cached 		= new HashMap<>();
		}

		public Map<Double, Double> getFree() {
			return free;
		}

		public void setFree(Map<Double, Double> free) {
			this.free = free;
		}

		public double getTotal() {
			return total;
		}

		public Map<Double, Double> getBuffered() {
			return buffered;
		}

		public Map<Double, Double> getCached() {
			return cached;
		}
	}
}