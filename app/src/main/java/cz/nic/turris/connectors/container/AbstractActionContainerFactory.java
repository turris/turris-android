/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.connectors.container;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.Map;
import java.util.TreeMap;

import cz.nic.turris.TurrisLog;
import cz.nic.turris.utility.Utility;

public abstract class AbstractActionContainerFactory<T extends AbstractContainerFactory.Container> extends AbstractContainerFactory<T> {

    @FunctionalInterface
    protected interface NodeAction<U> {
        void doNodeAction(U container, Node node);
    }


//////////////////////////// ABSTRACT METHODS

    protected abstract void prepareNodeActions(Map<String, NodeAction<T>> nodeActions);


//////////////////////////// INSTANCE ACCESS

    protected T processTargetNodes(Node startNode) {
        T container = prepareOutputContainer();
        TreeMap<String, NodeAction<T>> nodeActions = new TreeMap<>();
        prepareNodeActions(nodeActions);

        NodeList childNodes = startNode.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node childNode = childNodes.item(i);
            if (childNode != null && Utility.isNotStringEmpty(childNode.getLocalName())) {
                NodeAction<T> nodeAction = nodeActions.get(childNode.getLocalName());
                if (nodeAction != null) {
                    nodeAction.doNodeAction(container, childNode);
                } else {
                    TurrisLog.debug(getClass(), String.format("No action specified for node with name: %s", childNode.getLocalName()));
                }
            }
        }
        return container;
    }
}