/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.connectors.subtreefilters;

import java.util.ArrayList;
import java.util.List;

import cz.nic.netconf.yuma.YANGCapability;

public class SubtreeFilterFactory {
	
	public List<SubtreeFilterSet> getRouterInfoFilters() {
		List<SubtreeFilterSet> sf = new ArrayList<SubtreeFilterSet>();
		
		sf.add(new SubtreeFilterSet(new YANGCapability("http://www.nic.cz/ns/router/stats", 
													   "http://www.nic.cz/ns/router/stats", "stats", "stats"), "stats", new String[0]));
		
		sf.add(new SubtreeFilterSet(new YANGCapability("http://www.nic.cz/ns/router/nethist", 
				   									   "http://www.nic.cz/ns/router/nethist", "nethist", "nethist"), 
				   									   "nethist", new String[0]));
		
		return sf;
	}
	
	public List<SubtreeFilterSet> getUserNotifyFilters() {
		List<SubtreeFilterSet> sf = new ArrayList<SubtreeFilterSet>();
		
		sf.add(new SubtreeFilterSet(new YANGCapability("http://www.nic.cz/ns/router/user-notify", 
														"http://www.nic.cz/ns/router/user-notify", "user-notify", "user-notify"), "messages", new String[0]));
		
		return sf;
	}
	
	public List<SubtreeFilterSet> getInterfaceInfoFilters(String interfaceName) {
		List<SubtreeFilterSet> sf = new ArrayList<SubtreeFilterSet>();
		
		sf.add(new SubtreeFilterSet(new YANGCapability("http://www.nic.cz/ns/router/stats", 
														"http://www.nic.cz/ns/router/stats", "stats", "stats"), 
														"stats|interfaces|interface|name=$1", new String[]{interfaceName}));
		
		sf.add(new SubtreeFilterSet(new YANGCapability("http://www.nic.cz/ns/router/nethist", 
				   									   "http://www.nic.cz/ns/router/nethist", "nethist", "nethist"), 
				   									   "nethist|snapshots|snapshot|time;network|interface|name=$1;rx;tx", new String[]{interfaceName}));
		
		
		
		return sf;
	}
	
	public List<SubtreeFilterSet> getWirelessInterfaceInfoFilters() {
		List<SubtreeFilterSet> sf = new ArrayList<SubtreeFilterSet>();
		
		return sf;
	}
	
	public List<SubtreeFilterSet> getBridgeInterfaceInfoFilters() {
		List<SubtreeFilterSet> sf = new ArrayList<SubtreeFilterSet>();
		
		return sf;
	}
	
	

}
