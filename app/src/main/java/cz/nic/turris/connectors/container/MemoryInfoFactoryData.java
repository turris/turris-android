/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.connectors.container;

import java.util.List;
import java.util.Map;

import cz.nic.turris.connectors.NetconfTagDefinition;

public class MemoryInfoFactoryData extends AbstractSingleContainerFactory<MemoryInfoFactoryData.MemoryInfo> {

    @Override
    protected void prepareNodeNamePath(List<String> nodeNamePath) {
        nodeNamePath.add(NetconfTagDefinition.STATS);
        nodeNamePath.add(NetconfTagDefinition.MEMORY_INFO);
    }

    @Override
    protected void prepareNodeActions(Map<String, NodeAction<MemoryInfoFactoryData.MemoryInfo>> nodeActions) {
        nodeActions.put(NetconfTagDefinition.MEMORY_INFO_TOTAL, (memoryInfo, node) -> memoryInfo.total = Double.parseDouble(node.getTextContent()));
        nodeActions.put(NetconfTagDefinition.MEMORY_INFO_FREE, (memoryInfo, node) -> memoryInfo.free = Double.parseDouble(node.getTextContent()));
        nodeActions.put(NetconfTagDefinition.MEMORY_INFO_BUFFERS, (memoryInfo, node) -> memoryInfo.buffered = Double.parseDouble(node.getTextContent()));
        nodeActions.put(NetconfTagDefinition.MEMORY_INFO_CACHED, (memoryInfo, node) -> memoryInfo.cached = Double.parseDouble(node.getTextContent()));
    }

    @Override
    protected MemoryInfo prepareOutputContainer() {
        return new MemoryInfo();
    }


//////////////////////////// DATA CONTAINER

    public static class MemoryInfo extends AbstractSingleContainerFactory.Container {

        double free;
        double total;
        double buffered;
        double cached;

        private MemoryInfo() {}

        public double getFree() {
            return free;
        }

        public double getTotal() {
            return total;
        }

        public double getBuffered() {
            return buffered;
        }

        public double getCached() {
            return cached;
        }
    }
}