/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.connectors.container;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cz.nic.turris.connectors.NetconfTagDefinition;

public class WifiInfoFactoryData extends AbstractSingleContainerFactory<WifiInfoFactoryData.WifiInfo> {

	@Override
	protected void prepareNodeNamePath(List<String> nodeNamePath) {}

	@Override
	protected void prepareNodeActions(Map<String, NodeAction<WifiInfoFactoryData.WifiInfo>> nodeActions) {
		nodeActions.put(NetconfTagDefinition.WIFI_INFO_MODE, (wifiInfo, node) -> wifiInfo.role = node.getTextContent());
        nodeActions.put(NetconfTagDefinition.WIFI_INFO_CHANNEL, (wifiInfo, node) -> wifiInfo.channel = node.getTextContent());
        nodeActions.put(NetconfTagDefinition.WIFI_INFO_FREQUENCY, (wifiInfo, node) -> wifiInfo.frequency = node.getTextContent());
        nodeActions.put(NetconfTagDefinition.WIFI_INFO_CLIENTS,(wifiInfo, node) -> {
			if(node != null) {
				NodeList clientsList = ((Element) node).getElementsByTagName(NetconfTagDefinition.WIFI_INFO_CLIENT);
				for(int m = 0; m < clientsList.getLength(); m++) {
					wifiInfo.wifiClients.add((new WifiClientFactoryData()).getContainerInstance(clientsList.item(m)));
				}
			}
		});
	}

	@Override
	protected WifiInfo prepareOutputContainer() {
		return new WifiInfo();
	}


//////////////////////////// DATA CONTAINER

	public static class WifiInfo extends AbstractSingleContainerFactory.Container {

		private String									role;
		private String                                  channel;
		private String                                  frequency;
		private List<WifiClientFactoryData.WifiClient>  wifiClients;

		private WifiInfo() {
			wifiClients = new ArrayList<>();
		}

		public List<WifiClientFactoryData.WifiClient> getWifiClients() {
			return wifiClients;
		}

		public String getRole() {
			return role;
		}

		public String getChannel() {
			return channel;
		}

		public String getFrequency() {
			return frequency;
		}
	}
}