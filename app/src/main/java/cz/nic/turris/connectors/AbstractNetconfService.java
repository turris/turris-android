/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.connectors;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import java.io.IOException;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.util.ArrayList;
import java.util.List;

import cz.nic.netconf.Get;
import cz.nic.netconf.Get.GetReply;
import cz.nic.netconf.SubtreeFilter;
import cz.nic.netconf.frame.RpcHandler;
import cz.nic.netconf.frame.RpcReply;
import cz.nic.netconf.frame.RpcReplyError;
import cz.nic.netconf.transport.NetconfCatcherListener;
import cz.nic.netconf.transport.NetconfTransportEvent;
import cz.nic.netconf.transport.NetconfTransportEvent.EventType;
import cz.nic.netconf.transport.ssl.NetconfTlsCatcher;
import cz.nic.netconf.yuma.YANGCapability;
import cz.nic.turris.R;
import cz.nic.turris.TurrisLog;
import cz.nic.turris.TurrisSettings;
import cz.nic.turris.connectors.subtreefilters.SubtreeFilterSet;

public abstract class AbstractNetconfService extends Service {

    protected static final String           NETCONF_BASE_URL            = "http://www.nic.cz/ns/router/";
    protected static final SubtreeFilterSet NETCONF_STATS_FILTER        = new SubtreeFilterSet(new YANGCapability(NETCONF_BASE_URL + "stats", NETCONF_BASE_URL + "stats", "stats", "stats"), "stats", new String[0]);
    protected static final SubtreeFilterSet NETCONF_NETHIST_FILTER      = new SubtreeFilterSet(new YANGCapability(NETCONF_BASE_URL + "nethist", NETCONF_BASE_URL + "nethist", "nethist", "nethist"), "nethist", new String[0]);
    protected static final SubtreeFilterSet NETCONF_USER_NOTIFY_FILTER  = new SubtreeFilterSet(new YANGCapability(NETCONF_BASE_URL + "user-notify", NETCONF_BASE_URL + "user-notify", "user-notify", "user-notify"), "messages", new String[0]);

    public static final	 int	REMOTE_HOST_DEFAULT_PORT	            = TurrisSettings.DEFAULT_PORT;
    public static final  String	TURRIS_CONNECTION_LABEL 	            = "turris";

    private static final int    CONNECTION_CANNOT_BE_OPENED_MESSAGE_ID  = R.string.connection_cannot_be_opened;
    private static final int    CONNECTION_CLOSED_BY_SERVER_MESSAGE_ID  = R.string.connection_closed_by_server;
    private static final int    CONNECTION_CLOSED_BY_CLIENT_MESSAGE_ID  = R.string.connection_closed_by_client;
    private static final int    CONNECTION_UNKNOWN_ERROR_MESSAGE_ID     = R.string.unknown_error;

    private static final int    NETCONF_CONNECTION_TIMEOUT              = TurrisSettings.NETCONF_CONNECTION_TIMEOUT;


    private List<Connector>     connectors      = new ArrayList<>();
    private final IBinder       binder          = new Binder();


//////////////////////////// NETCONF CATCHER LISTENER

    protected static GetReply prepareReply(RpcHandler handler, ArrayList<SubtreeFilterSet> subTreeFilterList) throws IOException {
        try {
            Get g = new Get(handler.getSession());
            SubtreeFilter sf = createSubtreeFilter(g.createSubtreeFilter(), subTreeFilterList);
            g.setSubtreeFilter(sf);

            RpcReply reply = g.executeSync(handler);
            if (reply.containsErrors()) {
                for (RpcReplyError replyError: reply.getErrors()) {
                    TurrisLog.error(AbstractNetconfService.class, String.format("GET reply contains error: %s", replyError.getErrorMessage().getMessage()));
                }
                return null;
            }

            return g.new GetReply(reply);
        } catch (RuntimeException e) {
            TurrisLog.info(AbstractNetconfService.class, "Cannot prepare RPC reply!", e);
            return null;
        }
    }


//////////////////////////// NETCONF TRANSPORT EVENT ACTIONS

    protected enum NetConfTransportEventAction {

        CONNECTION_CANNOT_BE_OPENED (EventType.CONNECTION_CANNOT_BE_OPENED, CONNECTION_CANNOT_BE_OPENED_MESSAGE_ID, false),
        CONNECTION_CLOSED_BY_SERVER (EventType.CONNECTION_CLOSED_BY_SERVER, CONNECTION_CLOSED_BY_SERVER_MESSAGE_ID, false),
        CONNECTION_CLOSED_BY_USER   (EventType.CONNECTION_CLOSED_BY_USER,   CONNECTION_CLOSED_BY_CLIENT_MESSAGE_ID, false),
        UNKNOWN_ERROR               (null,                                  CONNECTION_UNKNOWN_ERROR_MESSAGE_ID,    true);

        private static final NetConfTransportEventAction FALLBACK_EVENT_ACTION = UNKNOWN_ERROR;

        private final EventType   eventType;
        public  final boolean     sendMessage;
        public  final Integer     errorMessageId;

        NetConfTransportEventAction(EventType eventType, Integer errorMessageId, boolean sendMessage) {
            this.eventType      = eventType;
            this.errorMessageId = errorMessageId;
            this.sendMessage    = sendMessage;
        }

        private void doErrorAction(AbstractNetconfService netconf, Connector connector, Exception e, String hostName) {
            String message = name() + " -> host: " + hostName;
            TurrisLog.error(AbstractNetconfService.class, message);
            TurrisLog.debug(AbstractNetconfService.class, message, e);

            netconf.netconfErrorAction(connector, this, e);
        }

        private static NetConfTransportEventAction getNetConfTransportEventActionForEventType(EventType eventType) {
            for (NetConfTransportEventAction netConfTransportEventAction : values()) {
                if (netConfTransportEventAction.eventType != null && netConfTransportEventAction.eventType.equals(eventType)) {
                    return netConfTransportEventAction;
                }
            }
            return FALLBACK_EVENT_ACTION;
        }
    }


//////////////////////////// SERVICE LIFECYCLE

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        netconfServiceStarted(intent);
        return Service.START_REDELIVER_INTENT;
    }

    @Override
    public IBinder onBind(Intent intent) {
        netconfServiceStarted(intent);
        return binder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        stopSelf();
        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopService();
    }


//////////////////////////// SERVICE CONTROL

    private void stopService() {
        destroyAllConnections();
        netconfServiceStopped();
        TurrisLog.info(getClass(), "NETCONF SERVICE DESTROYED");
    }


//////////////////////////// NETCONF SERVICE LIFECYCLE

    protected abstract void netconfServiceStarted(Intent intent);

    protected abstract void readyForRpcRequests(Connector connector, RpcHandler handler);

    protected abstract void endOfRpcRequests(String hostName);

    protected abstract void netconfServiceStopped();

    protected abstract void netconfConnectionDestroyed();

    protected abstract void netconfErrorAction(Connector connector, NetConfTransportEventAction transportEventAction, Exception e);


//////////////////////////// NETCONF CONNECTION

	private static SubtreeFilter createSubtreeFilter(SubtreeFilter createdSubtreeFilter, ArrayList<SubtreeFilterSet> subtreeFilterList) {
		if (subtreeFilterList != null) {
            for (SubtreeFilterSet subtreeFilterSet: subtreeFilterList) {
                createdSubtreeFilter.addFilter(subtreeFilterSet.getCapabilty()).addFilterString(subtreeFilterSet.getFilter(), subtreeFilterSet.getFilterParam());
            }
		}
		return createdSubtreeFilter;
	}

	protected void connect(Long routerId, String hostName, X509Certificate clientCert, RSAPrivateKey clientKey, X509Certificate serverCert) {
        Connector connector = new Connector(this, routerId, hostName, clientCert, clientKey, serverCert);
        connectors.add(connector);
        connector.prepareNetconfCatchListener();
        TurrisLog.info(getClass(), "NETCONF SERVICE CONNECTED FOR HOSTNAME: " + hostName);
	}

    protected void disconnect(Connector connector) {
        connectors.remove(connector);
        connector.destroyConnection();
        TurrisLog.info(getClass(), "NETCONF SERVICE DISCONNECTED FOR HOSTNAME: " + connector.hostName);
    }

    public static class Connector implements NetconfCatcherListener {

        private NetconfTlsCatcher           netconfTlsCatcher;
        private X509Certificate             clientCert;
        private RSAPrivateKey               clientKey;
        private X509Certificate             serverCert;

        private Long                        routerId;
        private String                      hostName;
        private AbstractNetconfService      abstractNetconfService;
        private Thread                      connectionThread;

        private Connector(AbstractNetconfService abstractNetconfService, Long routerId, String hostName, X509Certificate clientCert, RSAPrivateKey clientKey, X509Certificate serverCert) {
            this.abstractNetconfService = abstractNetconfService;
            this.routerId   = routerId;
            this.hostName   = hostName;
            this.clientCert = clientCert;
            this.clientKey  = clientKey;
            this.serverCert = serverCert;
        }

        private void prepareNetconfCatchListener() {
            netconfTlsCatcher = new NetconfTlsCatcher(TURRIS_CONNECTION_LABEL, hostName, REMOTE_HOST_DEFAULT_PORT, serverCert, clientCert, clientKey);
            Runnable runnableConnection = netconfTlsCatcher.getRunnableConnection(NETCONF_CONNECTION_TIMEOUT);
            netconfTlsCatcher.setNetconfCatcherListener(this);
            connectionThread = new Thread(runnableConnection);
            connectionThread.start();
        }

        private void destroyConnection() {
            synchronized (abstractNetconfService) {
                if (netconfTlsCatcher != null) {
                    connectionThread.setUncaughtExceptionHandler((t, e) -> TurrisLog.debug(AbstractNetconfService.class, "Exception in disconnected service", e));
                    netconfTlsCatcher.setNetconfCatcherListener(null);
                    netconfTlsCatcher.disconnect();
                    netconfTlsCatcher = null;
                }
                abstractNetconfService.endOfRpcRequests(hostName);
                abstractNetconfService.netconfConnectionDestroyed();
            }
        }

        public Long getRouterId() {
            return routerId;
        }

        public String getHostName() {
            return hostName;
        }

        @Override
        public void processTransportEvents(NetconfTransportEvent event) {
            synchronized (abstractNetconfService) {
                if (netconfTlsCatcher != null) {
                    //noinspection ThrowableResultOfMethodCallIgnored
                    NetConfTransportEventAction.getNetConfTransportEventActionForEventType(event.getEventType()).doErrorAction(abstractNetconfService, Connector.this, event.getTransportErrorException(), hostName);
                }
            }
        }

        @Override
        public void processReadyForRpcRequests(RpcHandler handler) {
            synchronized (abstractNetconfService) {
                if (netconfTlsCatcher != null) {
                    abstractNetconfService.readyForRpcRequests(this, handler);
                }
            }
        }

        @Override
        public String toString() {
            return routerId + " (" + hostName + ")";
        }
    }

    protected void destroyAllConnections() {
        //noinspection Convert2streamapi
        for (Connector connector: new ArrayList<>(connectors)) {
            disconnect(connector);
        }
    }
}
