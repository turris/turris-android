/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.connectors;

import android.content.Intent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.IOException;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.util.ArrayList;
import java.util.List;

import cz.nic.netconf.Get.GetReply;
import cz.nic.netconf.frame.Leaf;
import cz.nic.netconf.frame.Rpc;
import cz.nic.netconf.frame.RpcHandler;
import cz.nic.netconf.transport.Capability;
import cz.nic.turris.TurrisApplication;
import cz.nic.turris.TurrisLog;
import cz.nic.turris.connectors.event.NetconfCommandEvent;
import cz.nic.turris.connectors.subtreefilters.SubtreeFilterSet;
import cz.nic.turris.database.RouterProfile;
import cz.nic.turris.connectors.container.InterfaceListItemFactoryData;
import cz.nic.turris.connectors.container.LoadSnapshotFactory;
import cz.nic.turris.connectors.container.MemoryInfoFactoryData;
import cz.nic.turris.connectors.container.MemorySnapshotFactory;
import cz.nic.turris.connectors.container.MessageFactoryData;
import cz.nic.turris.connectors.container.NetworkSnapshotFactory;
import cz.nic.turris.connectors.container.RouterExtendedInfoFactoryData;
import cz.nic.turris.connectors.container.TemperatureSnapshotFactory;

public class DataRefreshService extends AbstractNetconfService {

    private static final ArrayList<SubtreeFilterSet>    NETCONF_ALL_DATA_FILTER_SET = new ArrayList<SubtreeFilterSet>() {
        {
            add(NETCONF_STATS_FILTER);
            add(NETCONF_NETHIST_FILTER);
        }
    };

    private static final ArrayList<SubtreeFilterSet>    NETCONF_MESSAGES_FILTER_SET = new ArrayList<SubtreeFilterSet>() {
        {
            add(NETCONF_USER_NOTIFY_FILTER);
        }
    };

    private static final String ROUTER_URI 					            = "http://www.nic.cz/ns/router";
	private static final String ROUTER_PREFIX 				            = "nc";
	private static final String RPC_ACTION_MAINTAIN 		            = "maintain";
	private static final String RPC_ACTION_USER_NOTIFY 		            = "user-notify";
	private static final String RPC_REBOOT 					            = "reboot";
	private static final String RPC_DISPLAY 				            = "display";
	private static final String RPC_MESSAGE_ID 				            = "message-id";

    private static final int    REFRESH_THREAD_PERIOD_TIME              = 3000;

    public static boolean               RUNNING_FLAG = false;

    private EventBus                    eventBus;
    private RpcHandler                  handler;

    private Long                        routerId;
    private String                      hostname;
    public  X509Certificate             clientCertificate;
    public  RSAPrivateKey               clientKey;
    public  X509Certificate             serverCertificate;
    private RPCRefresher                rpcRefresher;


//////////////////////////// SERVICE COMMANDS

    public enum NetConfServiceCommand {
        RECONNECT("RECONNECT", (DataRefreshService dataRefreshService, RpcHandler handler, String data, EventBus eventBus) -> {
            dataRefreshService.connectToActiveRouterProfile();
        }),
		REBOOT("REBOOT ROUTER", (DataRefreshService dataRefreshService, RpcHandler handler, String data, EventBus eventBus) -> {
            handler.sendSyncRpc(prepareRPCMessage(RPC_ACTION_MAINTAIN, RPC_REBOOT));
            RECONNECT.doCommandAction(dataRefreshService, handler, data, eventBus);
        }),
		DISMISS_MESSAGE("DISMISS MESSAGE", (DataRefreshService dataRefreshService, RpcHandler handler, String data, EventBus eventBus) -> {
            TurrisLog.debug(DataRefreshService.class, String.format("MESSAGE ID: %s", data));

            eventBus.removeStickyEvent(MessageFactoryData.Message[].class);

            Rpc dismissMsgRpc = prepareRPCMessage(RPC_ACTION_USER_NOTIFY, RPC_DISPLAY);
            Leaf msgIdLeaf = dismissMsgRpc.getInput().linkLeaf(Rpc.createLeaf(prepareCapability(RPC_ACTION_USER_NOTIFY), RPC_MESSAGE_ID));
            dismissMsgRpc.getInput().assignLeaf(msgIdLeaf, data);

            handler.sendSyncRpc(dismissMsgRpc);
		});

        @FunctionalInterface
		private interface NetConfServiceCommandAction {
			void doAction(DataRefreshService dataRefreshService, RpcHandler handler, String data, EventBus eventBus) throws Exception;
		}

		private final String                      description;
		private final NetConfServiceCommandAction commandAction;

		NetConfServiceCommand(String description, NetConfServiceCommandAction commandAction) {
			this.description = description;
			this.commandAction = commandAction;
		}

		private void doCommandAction(DataRefreshService dataRefreshService, RpcHandler handler, String data, EventBus eventBus) {
			try {
                TurrisLog.info(DataRefreshService.class, description);
                commandAction.doAction(dataRefreshService, handler, data, eventBus);
			} catch (Exception e) {
                TurrisLog.error(DataRefreshService.class, String.format("%s dataRefreshService service command error!", description), e);
                sendErrorEvent(eventBus, e.getLocalizedMessage());
			}
		}

		private void doCommandActionInThread(DataRefreshService dataRefreshService, RpcHandler handler, String data, EventBus eventBus) {
            synchronized (this) {
                new Thread(() -> doCommandAction(dataRefreshService, handler, data, eventBus)).start();
            }
		}

        private static Capability prepareCapability(String rpcAction) {
            String capabilityBaseURI = String.format("%s/%s", ROUTER_URI, rpcAction);
            return new Capability(capabilityBaseURI, capabilityBaseURI, ROUTER_PREFIX);
        }

        private static Rpc prepareRPCMessage(String rpcAction, String name) {
            return new Rpc(prepareCapability(rpcAction), name);
        }
	}


//////////////////////////// SERVICE LIFECYCLE

    @Override
    public void onCreate() {
        super.onCreate();
        eventBus = ((TurrisApplication) getApplication()).getEventBus();
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        eventBus.unregister(this);
    }


//////////////////////////// NETCONF SERVICE LIFECYCLE

    @Override
    protected void netconfServiceStarted(Intent intent) {
        connectToActiveRouterProfile();
    }

    private void connectToActiveRouterProfile() {
        RouterProfile activeRouter = RouterProfile.getActiveRouter(getApplicationContext());
        if (activeRouter != null && !activeRouter.getId().equals(routerId)) {
            destroyAllConnections();
            eventBus.removeAllStickyEvents();

            routerId            = activeRouter.getId();
            hostname            = activeRouter.getHostName();
            clientCertificate   = RouterProfile.getX509CertificateFromBytes(activeRouter.getClientCertificate());
            clientKey           = RouterProfile.getRsaPrivateKeyFromBytes(activeRouter.getClientKey());
            serverCertificate   = RouterProfile.getX509CertificateFromBytes(activeRouter.getServerCertificate());

            TurrisLog.info(getClass(), "NETCONF SERVICE STARTING FOR HOSTNAME: " + hostname);
            connect(routerId, hostname, clientCertificate, clientKey, serverCertificate);
        }
    }

    private static class RPCRefresher {

        private final Thread rpcRefreshThread;

        private boolean refreshThreadActive = true;
        private boolean firstData = true;

        public RPCRefresher(Connector connector, EventBus eventBus, RpcHandler handler) {
            rpcRefreshThread = new Thread(() -> {
                try {
                    TurrisLog.info(getClass(), "REFRESH THREAD [" + connector.getHostName() + "]: STARTED");
                    List<Object> dataToSend = new ArrayList<>();
                    while(refreshThreadActive) {

                        if (dataToSend.size() > 0) {
                            TurrisLog.debug(getClass(), "REFRESH THREAD [" + connector.getHostName() + "]: SENDING DATA");
                            for (Object data: dataToSend) {
                                eventBus.postSticky(data);
                            }
                            if (firstData) {
                                firstData = false;
                                eventBus.postSticky(new NetconfServiceConnectionEvent(true));
                            }
                            dataToSend.clear();
                        }

                        TurrisLog.info(getClass(), "REFRESH THREAD [" + connector.getHostName() + "]: LOADING NEW DATA");
                        GetReply reply = prepareReply(handler, NETCONF_ALL_DATA_FILTER_SET);
                        if (reply != null) {
                            Document data = reply.getData();
                            if (data != null) {
                                Element rootElement = data.getDocumentElement();

                                dataToSend.add(new MemoryInfoFactoryData().getContainerInstance(rootElement));
                                dataToSend.add(new RouterExtendedInfoFactoryData().getContainerInstance(rootElement));
                                dataToSend.add(new InterfaceListItemFactoryData().getContainersArrayInstance(rootElement));

                                dataToSend.add(new TemperatureSnapshotFactory().getContainerInstance(rootElement));
                                dataToSend.add(new LoadSnapshotFactory().getContainerInstance(rootElement));
                                dataToSend.add(new MemorySnapshotFactory().getContainerInstance(rootElement));
                                dataToSend.add(new NetworkSnapshotFactory().getContainersMapInstance(rootElement));
                            }
                        }

                        reply = prepareReply(handler, NETCONF_MESSAGES_FILTER_SET);
                        if (reply != null) {
                            Document data = reply.getData();
                            if (data != null) {
                                dataToSend.add(new MessageFactoryData().getContainersArrayInstance(data.getDocumentElement()));
                            }
                        }

                        Thread.sleep(REFRESH_THREAD_PERIOD_TIME);
                    }
                    TurrisLog.info(getClass(), "REFRESH THREAD [" + connector.getHostName() + "]: FINISHED");
                } catch (InterruptedException e) {
                    TurrisLog.error(getClass(), "REFRESH THREAD [" + connector.getHostName() + "]: INTERRUPTED", e);
                } catch (IOException e) {
                    TurrisLog.error(getClass(), "REFRESH THREAD [" + connector.getHostName() + "]: INTERRUPTED", e);
                }
            });
            rpcRefreshThread.start();
        }

        private void stop() {
            refreshThreadActive = false;
        }
    }

    @Override
    protected void readyForRpcRequests(Connector connector, RpcHandler handler) {
        this.handler = handler;
        stopRpcRefresher();
        rpcRefresher = new RPCRefresher(connector, eventBus, handler);
    }

    @Override
    protected void endOfRpcRequests(String netconf) {
        stopRpcRefresher();
        eventBus.postSticky(new NetconfEndOfRPCRequestsEvent());
    }

    private void stopRpcRefresher() {
        if (rpcRefresher != null) {
            rpcRefresher.stop();
        }
    }

    @Override
    protected void netconfServiceStopped() {
        RUNNING_FLAG = false;
    }

    @Override
    protected void netconfConnectionDestroyed() {
        eventBus.postSticky(new NetconfServiceConnectionEvent(false));
    }

    @Override
    protected void netconfErrorAction(Connector connector, NetConfTransportEventAction eventAction, Exception e) {
        // TODO mates: use separated enum
        String message = getResources().getString(eventAction.errorMessageId);
        if (e != null) {
            message = message + ": " + e.getMessage();
        }
        // TODO mates: use separated enum
        if (eventAction.sendMessage) {
            sendErrorEvent(eventBus, message);
        }

        disconnect(connector);
        connect(routerId, hostname, clientCertificate, clientKey, serverCertificate);
    }


//////////////////////////// EVENTS

    @Subscribe
    public void handleNetconfCommandEvent(NetconfCommandEvent netconfCommandEvent) {
        if (handler != null) {
            netconfCommandEvent.getNetConfServiceCommand().doCommandActionInThread(this, handler, netconfCommandEvent.getData(), eventBus);
        } else {
            TurrisLog.warning(getClass(), "RPC HANDLER NOT READY -> COMMAND: " + netconfCommandEvent.getNetConfServiceCommand() + " NOT PERFORMED");
        }
    }

    private static void sendErrorEvent(EventBus eventBus, String message) {
        eventBus.post(new NetconfErrorEvent(message));
    }

    private static class NetconfEndOfRPCRequestsEvent {
        private NetconfEndOfRPCRequestsEvent() {}
    }

    public static class NetconfServiceConnectionEvent {

        private final boolean connected;

        private NetconfServiceConnectionEvent(boolean connected) {
            this.connected = connected;
        }

        public boolean isConnected() {
            return connected;
        }
    }

    public static class NetconfErrorEvent {

        private String message;

        private NetconfErrorEvent(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }
}
