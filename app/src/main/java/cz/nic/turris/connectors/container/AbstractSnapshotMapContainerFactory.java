/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.connectors.container;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import cz.nic.turris.TurrisLog;
import cz.nic.turris.utility.Utility;
import cz.nic.turris.connectors.NetconfTagDefinition;

public abstract class AbstractSnapshotMapContainerFactory<T extends AbstractContainerFactory.Container> extends AbstractContainerFactory<T> {

    public static abstract class InMapContainer extends Container {

        protected InMapContainer() {}

        public abstract String getName();

    }

    @FunctionalInterface
    protected interface SnapshotMapNodeAction<U> {
        void doNodeAction(U outputContainer, Node node, long time);
    }

    private static final int INITIAL_TIME_VALUE = -1;

    @Override
    protected final void prepareNodeNamePath(List<String> nodeNamePath) {
        nodeNamePath.add(NetconfTagDefinition.NETWORK_HISTORY);
        nodeNamePath.add(NetconfTagDefinition.SNAPSHOTS);
    }


//////////////////////////// METHODS TO OVERRIDE

    protected abstract String prepareSnapshotParameterNodeName();

    protected abstract String prepareParameterDetailNodeName();

    protected abstract void addContainerToMap(Map<String, T> map, T container, long time);

    protected abstract void prepareNodeActions(Map<String, SnapshotMapNodeAction<T>> nodeActions);

    protected Map<String, T> prepareMapContainer() {
        return new HashMap<>();
    }


//////////////////////////// INSTANCE ACCESS

    public Map<String, T> getContainersMapInstance(Node node) {
        if (node == null) {
            return null;
        }

        Map<String, T> containersMap = prepareMapContainer();

        long time = INITIAL_TIME_VALUE;
        Node startNode = findStartNode(node);
        NodeList nodesToProcess = startNode.getChildNodes();
        for(int i = 0; i < nodesToProcess.getLength(); i++) {
            Node snapshotNode = nodesToProcess.item(i);
            if (Utility.isNotStringEmpty(snapshotNode.getLocalName()) && NetconfTagDefinition.SNAPSHOT.equals(snapshotNode.getLocalName())) {
                NodeList parameterNodes = snapshotNode.getChildNodes();
                for (int j = 0; j < parameterNodes.getLength(); j++) {
                    Node parameterNode = parameterNodes.item(j);
                    if (NetconfTagDefinition.TIME.equals(parameterNode.getLocalName())) {
                        String timeValue = parameterNode.getTextContent();
                        time = Long.parseLong(timeValue.substring(0, Math.min(timeValue.length(), String.valueOf(Long.MAX_VALUE).length())));
                    } else if (Utility.isNotStringEmpty(parameterNode.getLocalName()) && parameterNode.getLocalName().equals(prepareSnapshotParameterNodeName())) {
                        processDetailNodes(containersMap, parameterNode, time);
                    }
                }
            }
        }
        return containersMap;
    }

    private void processDetailNodes(Map<String, T> map, Node startNode, long time) {
        TreeMap<String, SnapshotMapNodeAction<T>> nodeActions = new TreeMap<>();
        prepareNodeActions(nodeActions);

        NodeList parameterNodes = startNode.getChildNodes();
        for (int i = 0; i < parameterNodes.getLength(); i++) {
            Node parameterNode = parameterNodes.item(i);
            if (Utility.isNotStringEmpty(parameterNode.getLocalName()) && parameterNode.getLocalName().equals(prepareParameterDetailNodeName())) {
                NodeList parameterValueNodes = parameterNode.getChildNodes();

                T container = prepareOutputContainer();
                for (int j = 0; j < parameterValueNodes.getLength(); j++) {
                    Node parameterValueNode = parameterValueNodes.item(j);

                    if (parameterValueNode != null && Utility.isNotStringEmpty(parameterValueNode.getLocalName())) {
                        SnapshotMapNodeAction<T> nodeAction = nodeActions.get(parameterValueNode.getLocalName());
                        if (nodeAction != null) {
                            nodeAction.doNodeAction(container, parameterValueNode, time);
                        } else {
                            TurrisLog.debug(getClass(), String.format("No action specified for node with name: %s", parameterNode.getLocalName()));
                        }
                    }
                }
                addContainerToMap(map, container, time);
            }
        }
    }
}