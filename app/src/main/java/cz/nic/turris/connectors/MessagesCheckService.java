/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.connectors;

import android.content.Intent;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import cz.nic.netconf.Get;
import cz.nic.netconf.frame.RpcHandler;
import cz.nic.turris.TurrisLog;
import cz.nic.turris.connectors.subtreefilters.SubtreeFilterSet;
import cz.nic.turris.database.RouterProfile;
import cz.nic.turris.connectors.container.MessageFactoryData;
import cz.nic.turris.notification.NotificationUtility;

public class MessagesCheckService extends AbstractNetconfService {

    private static final ArrayList<SubtreeFilterSet>    NETCONF_MESSAGES_FILTER_SET = new ArrayList<SubtreeFilterSet>() {
        {
            add(NETCONF_USER_NOTIFY_FILTER);
        }
    };

    private static boolean RUNNING_FLAG = false;

    private CountDownLatch latch;


//////////////////////////// NETCONF SERVICE LIFECYCLE

    @Override
    protected void netconfServiceStarted(Intent intent) {
        if (!RUNNING_FLAG && intent != null) {
            RUNNING_FLAG = true;

            TurrisLog.info(getClass(), "NETCONF MESSAGE CHECK SERVICE STARTING");
            new Thread(() -> {
                List<RouterProfile> allRouterProfiles = RouterProfile.getAllRegisteredRouters();
                latch = new CountDownLatch(allRouterProfiles.size());
                for (RouterProfile routerProfile : allRouterProfiles) {
                    connect(routerProfile.getId(),
                            routerProfile.getHostName(),
                            RouterProfile.getX509CertificateFromBytes(routerProfile.getClientCertificate()),
                            RouterProfile.getRsaPrivateKeyFromBytes(routerProfile.getClientKey()),
                            RouterProfile.getX509CertificateFromBytes(routerProfile.getServerCertificate()));
                }
                try {
                    latch.await();
                } catch (InterruptedException e) {
                    TurrisLog.error(MessagesCheckService.this.getClass(), "Cannot wait for connection thread!", e);
                } finally {
                    stopSelf();
                }
            }).start();
        }
    }

    @Override
    protected void readyForRpcRequests(Connector connector, RpcHandler handler) {
        try {
            TurrisLog.info(getClass(), "DOWNLOADING MESSAGES FOR HOST: " + connector.getHostName());
            Get.GetReply reply = prepareReply(handler, NETCONF_MESSAGES_FILTER_SET);
            if (reply != null) {
                boolean notifyResult = NotificationUtility.createNotification(getApplicationContext(), connector.getRouterId(), new MessageFactoryData().getContainersArrayInstance(reply.getData().getDocumentElement()));
                if (notifyResult) {
                    TurrisLog.debug(getClass(), "NOTIFICATION CREATED");
                } else {
                    TurrisLog.debug(getClass(), "NO NOTIFICATION CREATED");
                }
            }
        } catch (Exception e) {
            TurrisLog.error(DataRefreshService.class, "Cannot download actual messages for host name: " + connector.getHostName() + "!", e);
        }
        disconnect(connector);
    }

    @Override
    protected void endOfRpcRequests(String hostName) {
        TurrisLog.info(getClass(), "END OF DOWNLOADING MESSAGES FOR HOST: " + hostName);
        latch.countDown();
    }

    @Override
    protected void netconfServiceStopped() {
        TurrisLog.info(getClass(), "NETCONF MESSAGE CHECK SERVICE STOPPED");
        RUNNING_FLAG = false;
    }

    @Override
    protected void netconfConnectionDestroyed() {

    }

    @Override
    protected void netconfErrorAction(Connector connector, NetConfTransportEventAction transportEventAction, Exception e) {
        disconnect(connector);
    }

}
