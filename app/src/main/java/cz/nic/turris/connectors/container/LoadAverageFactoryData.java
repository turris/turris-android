/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.connectors.container;

import java.util.List;
import java.util.Map;

import cz.nic.turris.connectors.NetconfTagDefinition;

public class LoadAverageFactoryData extends AbstractSingleContainerFactory<LoadAverageFactoryData.LoadAverage> {

	@Override
	protected void prepareNodeNamePath(List<String> nodeNamePath) {}

	@Override
	protected void prepareNodeActions(Map<String, NodeAction<LoadAverageFactoryData.LoadAverage>> nodeActions) {
        nodeActions.put(NetconfTagDefinition.LOAD_AVERAGE_AVG1, (loadAverage, node) -> loadAverage.loadAverage1 = node.getTextContent());
        nodeActions.put(NetconfTagDefinition.LOAD_AVERAGE_AVG5, (loadAverage, node) -> loadAverage.loadAverage5 = node.getTextContent());
        nodeActions.put(NetconfTagDefinition.LOAD_AVERAGE_AVG15, (loadAverage, node) -> loadAverage.loadAverage15 = node.getTextContent());
	}

	@Override
	protected LoadAverage prepareOutputContainer() {
		return new LoadAverage();
	}


//////////////////////////// DATA CONTAINER

	public static class LoadAverage extends AbstractSingleContainerFactory.Container {

		private String loadAverage1 = null;
		private String loadAverage5 = null;
		private String loadAverage15 = null;

		private LoadAverage() {}

		public String getLoadAverage1() {
			return loadAverage1;
		}

		public String getLoadAverage5() {
			return loadAverage5;
		}

		public String getLoadAverage15() {
			return loadAverage15;
		}
	}
}