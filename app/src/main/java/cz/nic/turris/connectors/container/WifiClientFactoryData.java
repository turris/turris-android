/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.connectors.container;

import java.util.List;
import java.util.Map;

import cz.nic.turris.connectors.NetconfTagDefinition;

public class WifiClientFactoryData extends AbstractSingleContainerFactory<WifiClientFactoryData.WifiClient> {

	@Override
	protected void prepareNodeNamePath(List<String> nodeNamePath) {}

	@Override
	protected void prepareNodeActions(Map<String, NodeAction<WifiClientFactoryData.WifiClient>> nodeActions) {
		nodeActions.put(NetconfTagDefinition.WIFI_CLIENT_MAC, (wifiClient, node) -> wifiClient.mac = node.getTextContent());
		nodeActions.put(NetconfTagDefinition.WIFI_CLIENT_SIGNAL, (wifiClient, node) -> wifiClient.signal = node.getTextContent());
		nodeActions.put(NetconfTagDefinition.WIFI_CLIENT_TX_BITRATE, (wifiClient, node) -> wifiClient.txBitrate = node.getTextContent());
		nodeActions.put(NetconfTagDefinition.WIFI_CLIENT_RX_BITRATE, (wifiClient, node) -> wifiClient.rxBitrate = node.getTextContent());
	}

	@Override
	protected WifiClient prepareOutputContainer() {
		return new WifiClient();
	}


//////////////////////////// DATA CONTAINER

	public static class WifiClient extends AbstractSingleContainerFactory.Container {

		private String mac;
		private String signal;
		private String txBitrate;
		private String rxBitrate;

		private WifiClient() {}

		public String getMac() {
			return mac;
		}

		public String getSignal() {
			return signal;
		}

		public String getTxBitrate() {
			return txBitrate;
		}

		public String getRxBitrate() {
			return rxBitrate;
		}
	}
}
