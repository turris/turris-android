/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.connectors.container;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cz.nic.turris.connectors.NetconfTagDefinition;

public class BridgeInfoFactoryData extends AbstractSingleContainerFactory<BridgeInfoFactoryData.BridgeInfo> {

    private static final String STP_TRUE_VALUE = "1";

    @Override
    protected void prepareNodeNamePath(List<String> nodeNamePath) {}

    @Override
    protected void prepareNodeActions(Map<String, NodeAction<BridgeInfoFactoryData.BridgeInfo>> nodeActions) {
        nodeActions.put(NetconfTagDefinition.BRIDGE_ID, (bridgeInfo, node) -> bridgeInfo.bridgeId = node.getTextContent());
        nodeActions.put(NetconfTagDefinition.BRIDGE_STP_STATE, (bridgeInfo, node) -> bridgeInfo.stp = STP_TRUE_VALUE.equals(node.getTextContent()));
        nodeActions.put(NetconfTagDefinition.BRIDGE_ASSOCIATED_INTERFACES, (bridgeInfo, node) -> {
            if(node != null) {
                NodeList assocInterfacesList = ((Element) node).getElementsByTagName(NetconfTagDefinition.BRIDGE_INTERFACE);
                for(int i = 0; i < assocInterfacesList.getLength(); i++) {
                    bridgeInfo.bondedInterfaces.add(assocInterfacesList.item(i).getTextContent());
                }
            }
        });
    }

    @Override
    protected BridgeInfo prepareOutputContainer() {
        return new BridgeInfo();
    }


//////////////////////////// DATA CONTAINER

    public static class BridgeInfo extends AbstractSingleContainerFactory.Container {

        private List<String>    bondedInterfaces;
        private String          bridgeId;
        private boolean         stp;

        private BridgeInfo(){
            bondedInterfaces = new ArrayList<>();
        }

        public List<String> getBondedInterfaces() {
            return bondedInterfaces;
        }

        public String getBridgeId() {
            return bridgeId;
        }

        public boolean isStp() {
            return stp;
        }
    }
}