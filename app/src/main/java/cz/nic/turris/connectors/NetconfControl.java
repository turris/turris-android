/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.connectors;

import android.support.v7.app.AlertDialog;

import cz.nic.turris.R;
import cz.nic.turris.utility.activity.AbstractTurrisActivity;
import cz.nic.turris.connectors.event.NetconfCommandEvent;

public class NetconfControl {

    public static void reboot(AbstractTurrisActivity activity, Runnable restartHandler) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(R.string.restart_confirm_message)
                .setPositiveButton(R.string.restart_button, (dialog, id) -> {
                    if (restartHandler != null) {
                        restartHandler.run();
                    }
                    activity.getTurrisApplication().getEventBus().post(new NetconfCommandEvent(DataRefreshService.NetConfServiceCommand.REBOOT));
                })
                .setNegativeButton(R.string.cancel_button, (dialog, id) -> {});
        builder.show();
    }

}
