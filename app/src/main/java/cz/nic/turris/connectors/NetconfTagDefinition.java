/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.connectors;

public final class NetconfTagDefinition {

    private NetconfTagDefinition() {}

    public static final String NETWORK_HISTORY                          = "nethist";
    public static final String TIME                                     = "time";
    public static final String STATS                                    = "stats";

    public static final String BOARD_NAME                               = "board-name";
    public static final String HOST_NAME                                = "hostname";
    public static final String KERNEL_VERSION                           = "kernel-version";
    public static final String TURRIS_OS_VERSION                        = "turris-os-version";
    public static final String FIRMWARE_VERSION_CONTENT_BORDER          = "&apos;";
    public static final String LOCAL_TIME                               = "local-time";
    public static final String UP_TIME                                  = "uptime";
    public static final String LOAD_AVERAGE                             = "load-average";

    public static final String WIFI_CLIENT_MAC                          = "mac";
    public static final String WIFI_CLIENT_SIGNAL                       = "signal";
    public static final String WIFI_CLIENT_TX_BITRATE                   = "tx-bitrate";
    public static final String WIFI_CLIENT_RX_BITRATE                   = "rx-bitrate";

    public static final String WIFI_INFO_MODE                           = "mode";
    public static final String WIFI_INFO_CHANNEL                        = "channel";
    public static final String WIFI_INFO_FREQUENCY                      = "frequency";
    public static final String WIFI_INFO_CLIENTS                        = "clients";
    public static final String WIFI_INFO_CLIENT                         = "client";

    public static final String BRIDGE_ID                                = "id";
    public static final String BRIDGE_STP_STATE                         = "stp-state";
    public static final String BRIDGE_ASSOCIATED_INTERFACES             = "associated-interfaces";
    public static final String BRIDGE_INTERFACE                         = "interface";

    public static final String MESSAGES                                 = "messages";
    public static final String MESSAGE                                  = "message";
    public static final String MESSAGE_ID                               = "id";
    public static final String MESSAGE_BODY                             = "body";
    public static final String MESSAGE_SEVERITY                         = "severity";
    public static final String MESSAGE_TIMESTAMP                        = "timestamp";
    public static final String MESSAGE_DISPLAYED                        = "displayed";

    public static final String MEMORY_INFO                              = "meminfo";
    public static final String MEMORY_INFO_TOTAL                        = "MemTotal";
    public static final String MEMORY_INFO_FREE                         = "MemFree";
    public static final String MEMORY_INFO_BUFFERS                      = "Buffers";
    public static final String MEMORY_INFO_CACHED                       = "Cached";

    public static final String INTERFACES                               = "interfaces";
    public static final String INTERFACE                                = "interface";
    public static final String INTERFACE_NAME                           = "name";
    public static final String INTERFACE_USE                            = "use";
    public static final String INTERFACE_UP                             = "up";
    public static final String INTERFACE_WIRELESS                       = "wireless";
    public static final String INTERFACE_BRIDGE                         = "bridge";
    public static final String INTERFACE_ADDRESS                        = "address";

    public static final String INTERFACE_ADDRESS_ADDRESS                = "address";
    public static final String INTERFACE_ADDRESS_TYPE                   = "type";

    public static final String LOAD_AVERAGE_AVG1                        = "avg-1";
    public static final String LOAD_AVERAGE_AVG5                        = "avg-5";
    public static final String LOAD_AVERAGE_AVG15                       = "avg-15";

    public static final String SNAPSHOTS                                = "snapshots";
    public static final String SNAPSHOT                                 = "snapshot";

    public static final String SNAPSHOT_CPU                             = "cpu";
    public static final String SNAPSHOT_CPU_LOAD                        = "load";

    public static final String SNAPSHOT_MEMORY                          = "memory";
    public static final String SNAPSHOT_MEMORY_TOTAL                    = "memtotal";
    public static final String SNAPSHOT_MEMORY_FREE                     = "memfree";
    public static final String SNAPSHOT_MEMORY_BUFFERS                  = "buffers";
    public static final String SNAPSHOT_MEMORY_CACHED                   = "cached";

    public static final String SNAPSHOT_NETWORK                         = "network";
    public static final String SNAPSHOT_NETWORK_INTERFACE               = "interface";
    public static final String SNAPSHOT_NETWORK_INTERFACE_NAME          = "name";
    public static final String SNAPSHOT_NETWORK_INTERFACE_RX            = "rx";
    public static final String SNAPSHOT_NETWORK_INTERFACE_TX            = "tx";

    public static final String SNAPSHOT_TEMPERATURE                     = "temperature";
    public static final String SNAPSHOT_TEMPERATURE_CPU                 = "cpu";
    public static final String SNAPSHOT_TEMPERATURE_BOARD               = "board";
}
