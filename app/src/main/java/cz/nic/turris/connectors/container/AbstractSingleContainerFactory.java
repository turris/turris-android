/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.connectors.container;

import org.w3c.dom.Node;

public abstract class AbstractSingleContainerFactory<T extends AbstractContainerFactory.Container> extends AbstractActionContainerFactory<T> {


//////////////////////////// INSTANCE ACCESS

    public T getContainerInstance(Node node) {
        if (node == null) {
            return null;
        }

        Node startNode = findStartNode(node);
        return processTargetNodes(startNode);
    }
}