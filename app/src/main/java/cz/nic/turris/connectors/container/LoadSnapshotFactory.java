/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.connectors.container;

import java.util.HashMap;
import java.util.Map;

import cz.nic.turris.utility.Utility;
import cz.nic.turris.connectors.NetconfTagDefinition;

public class LoadSnapshotFactory extends AbstractSnapshotArrayContainerFactory<LoadSnapshotFactory.LoadSnapshot> {

	private static final float LOAD_RATIO = Utility.LOAD_MAXIMAL_VALUE * 1.0f;

	@Override
	protected String prepareSnapshotParameterNodeName() {
		return NetconfTagDefinition.SNAPSHOT_CPU;
	}

	@Override
	protected void prepareNodeActions(Map<String, SnapshotNodeAction<LoadSnapshotFactory.LoadSnapshot>> nodeActions) {
		nodeActions.put(NetconfTagDefinition.SNAPSHOT_CPU_LOAD, (loadSnapshot, node, time) -> {
            float load = Float.parseFloat(node.getTextContent()) * LOAD_RATIO;
            if (load > Utility.LOAD_MAXIMAL_VALUE) {
                load = Utility.LOAD_MAXIMAL_VALUE;
            }
            loadSnapshot.load.put((double) time, (double) load);
        });
	}

	@Override
	protected LoadSnapshot prepareOutputContainer() {
		return new LoadSnapshot();
	}


//////////////////////////// DATA CONTAINER

	public static class LoadSnapshot extends AbstractContainerFactory.Container {

		private Map<Double, Double> load;

		private LoadSnapshot() {
            load = new HashMap<>();
		}

		public Map<Double, Double> getLoad() {
			return load;
		}
	}
}