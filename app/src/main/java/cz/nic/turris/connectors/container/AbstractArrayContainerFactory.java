/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.connectors.container;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

import cz.nic.turris.utility.Utility;

public abstract class AbstractArrayContainerFactory<T extends AbstractContainerFactory.Container> extends AbstractActionContainerFactory<T> {


//////////////////////////// ABSTRACT METHODS

    protected abstract String prepareArrayNodeName();

    protected abstract T[] prepareOutputContainerArray();


//////////////////////////// INSTANCE ACCESS

    public T[] getContainersArrayInstance(Node node) {
        if (node == null) {
            return null;
        }

        List<T> containersList = new ArrayList<>();

        Node startNode = findStartNode(node);
        NodeList nodesToProcess = startNode.getChildNodes();
        for(int i = 0; i < nodesToProcess.getLength(); i++) {
            Node nodeToProcess = nodesToProcess.item(i);
            if (Utility.isNotStringEmpty(nodeToProcess.getLocalName()) && nodeToProcess.getLocalName().equals(prepareArrayNodeName())) {
                containersList.add(processTargetNodes(nodeToProcess));
            }
        }

        return containersList.toArray(prepareOutputContainerArray());
    }
}