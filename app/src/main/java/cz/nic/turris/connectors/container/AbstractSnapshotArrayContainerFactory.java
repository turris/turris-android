/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.connectors.container;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import cz.nic.turris.TurrisLog;
import cz.nic.turris.utility.Utility;
import cz.nic.turris.connectors.NetconfTagDefinition;

public abstract class AbstractSnapshotArrayContainerFactory<T extends AbstractContainerFactory.Container> extends AbstractContainerFactory<T> {

    @FunctionalInterface
    protected interface SnapshotNodeAction<U> {
        void doNodeAction(U outputContainer, Node node, long time);
    }

    private static final int INITIAL_TIME_VALUE = -1;

    @Override
    protected final void prepareNodeNamePath(List<String> nodeNamePath) {
        nodeNamePath.add(NetconfTagDefinition.NETWORK_HISTORY);
        nodeNamePath.add(NetconfTagDefinition.SNAPSHOTS);
    }

//////////////////////////// ABSTRACT METHODS

    protected abstract String prepareSnapshotParameterNodeName();

    protected abstract void prepareNodeActions(Map<String, SnapshotNodeAction<T>> nodeActions);


//////////////////////////// INSTANCE ACCESS

    public T getContainerInstance(Node node) {
        if (node == null) {
            return null;
        }

        T container = prepareOutputContainer();
        long time = INITIAL_TIME_VALUE;
        Node startNode = findStartNode(node);
        NodeList nodesToProcess = startNode.getChildNodes();
        for(int i = 0; i < nodesToProcess.getLength(); i++) {
            Node snapshotNode = nodesToProcess.item(i);
            if (Utility.isNotStringEmpty(snapshotNode.getLocalName()) && NetconfTagDefinition.SNAPSHOT.equals(snapshotNode.getLocalName())) {
                NodeList parameterNodes = snapshotNode.getChildNodes();
                for (int j = 0; j < parameterNodes.getLength(); j++) {
                    Node parameterNode = parameterNodes.item(j);
                    if (NetconfTagDefinition.TIME.equals(parameterNode.getLocalName())) {
                        String timeValue = parameterNode.getTextContent();
                        time = Long.parseLong(timeValue.substring(0, Math.min(timeValue.length(), String.valueOf(Long.MAX_VALUE).length())));
                    } else if (Utility.isNotStringEmpty(parameterNode.getLocalName()) && parameterNode.getLocalName().equals(prepareSnapshotParameterNodeName())) {
                        processTargetNodes(container, parameterNode, time);
                    }
                }
            }
        }
        return container;
    }

    protected void processTargetNodes(T container, Node startNode, long time) {
        TreeMap<String, SnapshotNodeAction<T>> nodeActions = new TreeMap<>();
        prepareNodeActions(nodeActions);

        NodeList childNodes = startNode.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node childNode = childNodes.item(i);
            if (childNode != null && Utility.isNotStringEmpty(childNode.getLocalName())) {
                SnapshotNodeAction<T> nodeAction = nodeActions.get(childNode.getLocalName());
                if (nodeAction != null) {
                    nodeAction.doNodeAction(container, childNode, time);
                } else {
                    TurrisLog.debug(getClass(), String.format("No action specified for node with name: %s", childNode.getLocalName()));
                }
            }
        }
    }
}