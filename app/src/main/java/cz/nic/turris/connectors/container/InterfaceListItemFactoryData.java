/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.connectors.container;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cz.nic.turris.connectors.NetconfTagDefinition;

public class InterfaceListItemFactoryData extends AbstractArrayContainerFactory<InterfaceListItemFactoryData.InterfaceListItem> {

	@Override
	protected void prepareNodeNamePath(List<String> nodeNamePath) {
		nodeNamePath.add(NetconfTagDefinition.STATS);
		nodeNamePath.add(NetconfTagDefinition.INTERFACES);
	}

	@Override
	protected String prepareArrayNodeName() {
		return NetconfTagDefinition.INTERFACE;
	}

	@Override
	protected void prepareNodeActions(Map<String, NodeAction<InterfaceListItemFactoryData.InterfaceListItem>> nodeActions) {
		nodeActions.put(NetconfTagDefinition.INTERFACE_NAME, (interfaceListItem, node) -> interfaceListItem.name = node.getTextContent());
		nodeActions.put(NetconfTagDefinition.INTERFACE_USE, (interfaceListItem, node) -> interfaceListItem.use = node.getTextContent());
		nodeActions.put(NetconfTagDefinition.INTERFACE_UP, (interfaceListItem, node) -> interfaceListItem.up = true);
		nodeActions.put(NetconfTagDefinition.INTERFACE_WIRELESS, (interfaceListItem, node) -> {
            interfaceListItem.type = InterfaceListItem.InterfaceType.WIRELESS;
            interfaceListItem.wifiInfo = ((new WifiInfoFactoryData()).getContainerInstance(node));
        });
		nodeActions.put(NetconfTagDefinition.INTERFACE_BRIDGE, (interfaceListItem, node) -> {
            interfaceListItem.type = InterfaceListItem.InterfaceType.BRIDGE;
            interfaceListItem.bridgeInfo = (new BridgeInfoFactoryData()).getContainerInstance(node);
        });
		nodeActions.put(NetconfTagDefinition.INTERFACE_ADDRESS, (interfaceListItem, node) -> interfaceListItem.addresses.add((new InterfaceAddressFactoryData()).getContainerInstance(node)));
	}

	@Override
	protected InterfaceListItem[] prepareOutputContainerArray() {
		return new InterfaceListItem[]{};
	}

	@Override
	protected InterfaceListItem prepareOutputContainer() {
		return new InterfaceListItem();
	}


//////////////////////////// DATA CONTAINER

	public static class InterfaceListItem extends AbstractContainerFactory.Container {

		private String                                          	name;
		private String                                          	use;
		private boolean                                         	up;
		private InterfaceType                                   	type;
		private List<InterfaceAddressFactoryData.InterfaceAddress>	addresses;
		private BridgeInfoFactoryData.BridgeInfo                    bridgeInfo;
		private WifiInfoFactoryData.WifiInfo                        wifiInfo;

        public enum InterfaceType {

			GENERIC		(0),
            BRIDGE		(1),
            WIRELESS	(2);

			private int number;

			InterfaceType(int number) {
				this.number = number;
			}

			public int getNumber() {
				return number;
			}

			public static InterfaceType getInterfaceTypeForNumber(int number) {
				for (InterfaceType interfaceType: values()) {
					if (interfaceType.number == number) {
						return interfaceType;
					}
				}
				return GENERIC;
			}
		}

		private InterfaceListItem(){
			addresses   = new ArrayList<>();
			type        = InterfaceType.GENERIC;
		}

		public String getName() {
			return name;
		}

		public String getUse() {
			return use;
		}

		public InterfaceType getType() {
			return type;
		}

		public boolean isUp() {
			return up;
		}

		public List<InterfaceAddressFactoryData.InterfaceAddress> getAddressList() {
			return addresses;
		}

		public BridgeInfoFactoryData.BridgeInfo getBridgeInfo() {
			return bridgeInfo;
		}

		public WifiInfoFactoryData.WifiInfo getWifiInfo() {
			return wifiInfo;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj == null) {
				return false;
			}
			if (!(obj instanceof InterfaceListItem)) {
				return false;
			}

			InterfaceListItem interfaceListItem = (InterfaceListItem) obj;
			return name != null && name.equals(interfaceListItem.name);
		}
	}
}

