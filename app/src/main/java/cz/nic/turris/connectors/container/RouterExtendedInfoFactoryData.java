/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.connectors.container;

import java.util.List;
import java.util.Map;

import cz.nic.turris.utility.Utility;
import cz.nic.turris.connectors.NetconfTagDefinition;

public class RouterExtendedInfoFactoryData extends AbstractSingleContainerFactory<RouterExtendedInfoFactoryData.RouterExtendedInfo> {

    @Override
    protected void prepareNodeNamePath(List<String> nodeNamePath) {
        nodeNamePath.add(NetconfTagDefinition.STATS);
    }

    @Override
    protected void prepareNodeActions(Map<String, NodeAction<RouterExtendedInfo>> nodeActions) {
        nodeActions.put(NetconfTagDefinition.BOARD_NAME, (routerMoreInfo, node) -> routerMoreInfo.boardName = node.getTextContent());
        nodeActions.put(NetconfTagDefinition.HOST_NAME, (routerMoreInfo, node) -> routerMoreInfo.hostName = node.getTextContent());
        nodeActions.put(NetconfTagDefinition.KERNEL_VERSION, (routerMoreInfo, node) -> routerMoreInfo.kernelVersion = node.getTextContent());
        nodeActions.put(NetconfTagDefinition.TURRIS_OS_VERSION, (routerMoreInfo, node) -> routerMoreInfo.turrisOsVersion = removeNonRequiredContent(node.getTextContent(), NetconfTagDefinition.FIRMWARE_VERSION_CONTENT_BORDER));
        nodeActions.put(NetconfTagDefinition.LOCAL_TIME, (routerMoreInfo, node) -> routerMoreInfo.localTime = Long.parseLong(node.getTextContent()) * Utility.MILLIS_COUNT);
        nodeActions.put(NetconfTagDefinition.UP_TIME, (routerMoreInfo, node) -> routerMoreInfo.uptime = node.getTextContent());
        nodeActions.put(NetconfTagDefinition.LOAD_AVERAGE, (routerMoreInfo, node) -> routerMoreInfo.loadAvg = (new LoadAverageFactoryData()).getContainerInstance(node));
    }

    private String removeNonRequiredContent(String text, String contentBorders) {
        String textContent = text;
        if (textContent.contains(contentBorders)) {
            textContent = textContent.substring(textContent.indexOf(contentBorders) + contentBorders.length(), textContent.lastIndexOf(contentBorders));
        }
        return textContent;
    }

    @Override
    protected RouterExtendedInfo prepareOutputContainer() {
        return new RouterExtendedInfo();
    }


//////////////////////////// DATA CONTAINER

    public static class RouterExtendedInfo extends AbstractSingleContainerFactory.Container {

        private String                              hostName;
        private String                              boardName;
        private String                              kernelVersion;
        private String                              turrisOsVersion;
        private long                                localTime;
        private String                              uptime;
        private LoadAverageFactoryData.LoadAverage  loadAvg;

        private RouterExtendedInfo() {}

        public String getHostName() {
            return hostName;
        }

        public String getBoardName() {
            return boardName;
        }

        public String getKernelVersion() {
            return kernelVersion;
        }

        public String getTurrisOsVersion() {
            return turrisOsVersion;
        }

        public long getLocalTime() {
            return localTime;
        }

        public String getUptime() {
            return uptime;
        }

        public LoadAverageFactoryData.LoadAverage getLoadAvg() {
            return loadAvg;
        }
    }
}