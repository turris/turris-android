/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.connectors.container;

import java.util.List;
import java.util.Map;

import cz.nic.turris.connectors.NetconfTagDefinition;

public class MessageFactoryData extends AbstractArrayContainerFactory<MessageFactoryData.Message> {

    @Override
	protected void prepareNodeNamePath(List<String> nodeNamePath) {
		nodeNamePath.add(NetconfTagDefinition.STATS);
		nodeNamePath.add(NetconfTagDefinition.MESSAGES);
	}

	@Override
	protected String prepareArrayNodeName() {
		return NetconfTagDefinition.MESSAGE;
	}

	@Override
	protected void prepareNodeActions(Map<String, NodeAction<MessageFactoryData.Message>> nodeActions) {
        nodeActions.put(NetconfTagDefinition.MESSAGE_ID, (message, node) -> message.id = node.getTextContent());
        nodeActions.put(NetconfTagDefinition.MESSAGE_BODY, (message, node) -> message.body = node.getTextContent());
        nodeActions.put(NetconfTagDefinition.MESSAGE_SEVERITY, (message, node) -> message.severity = node.getTextContent());
		nodeActions.put(NetconfTagDefinition.MESSAGE_TIMESTAMP, (message, node) -> message.timestamp = Long.valueOf(node.getTextContent()));
        nodeActions.put(NetconfTagDefinition.MESSAGE_DISPLAYED, (message, node) -> message.displayed = true);
	}

	@Override
	protected Message prepareOutputContainer() {
		return new Message();
	}

	@Override
	protected Message[] prepareOutputContainerArray() {
		return new Message[]{};
	}


//////////////////////////// DATA CONTAINER

	public static class Message extends AbstractContainerFactory.Container {

		public static final String MESSAGE_SEVERITY_RESTART = "restart";
		public static final String MESSAGE_SEVERITY_ERROR	= "error";
		public static final String MESSAGE_SEVERITY_UPDATE	= "update";
		public static final String MESSAGE_SEVERITY_NEWS	= "news";

        private String  id;
		private String  body;
		private String  severity;
		private Long	timestamp;
		private boolean displayed;

		private Message() {
			displayed = false;
		}

		public String getId() {
			return id;
		}

		public String getBody() {
			return body;
		}

		public String getSeverity() {
			return severity;
		}

		public Long getTimestamp() {
			return timestamp;
		}

		public boolean isDisplayed() {
			return displayed;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj == null) {
				return false;
			}
			if (!(obj instanceof Message)) {
				return false;
			}

			Message message = (Message) obj;
			if (message.id == null) {
				return false;
			}

			return message.id.equals(id);
		}
	}
}