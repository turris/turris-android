/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.connectors.container;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

import cz.nic.turris.utility.Utility;

public abstract class AbstractContainerFactory<T extends AbstractContainerFactory.Container> {

    public static abstract class Container {

        private final long containerUpdateTimestamp;

        protected Container() {
            this.containerUpdateTimestamp = System.currentTimeMillis();
        }

        public long getContainerUpdateTimestamp() {
            return containerUpdateTimestamp;
        }
    }


//////////////////////////// ABSTRACT METHODS

    protected abstract void prepareNodeNamePath(List<String> nodeNamePath);

    protected abstract T prepareOutputContainer();


//////////////////////////// INSTANCE ACCESS

    protected Node findStartNode(Node node) {
        ArrayList<String> namePath = new ArrayList<>();
        prepareNodeNamePath(namePath);

        Node currentNode = node;
        for (String nodeName: namePath) {
            NodeList currentChildNodes = currentNode.getChildNodes();
            for (int i = 0; i < currentChildNodes.getLength(); i++) {
                Node nodeToCheck = currentChildNodes.item(i);
                if (Utility.isNotStringEmpty(nodeName) && nodeName.equals(nodeToCheck.getNodeName())) {
                    currentNode = nodeToCheck;
                    break;
                }
            }
        }
        return currentNode;
    }
}