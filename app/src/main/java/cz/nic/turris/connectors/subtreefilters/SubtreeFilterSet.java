/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.connectors.subtreefilters;

import android.os.Parcel;
import android.os.Parcelable;
import cz.nic.netconf.yuma.YANGCapability;

public class SubtreeFilterSet implements Parcelable {

	private String[] capability;
	private String filter;
	private String[] filterParam;

	public SubtreeFilterSet(YANGCapability capability, String filter,
			String[] filterParam) {
		setCapabilty(capability);
		this.filter = filter;
		this.filterParam = filterParam;
	}

	protected SubtreeFilterSet(Parcel in) {
		capability = new String[4];
		in.readStringArray(capability);
		filter = in.readString();
		filterParam = new String[in.readInt()];
		in.readStringArray(filterParam);
	}

	public YANGCapability getCapabilty() {
		return new YANGCapability(this.capability[0], capability[1], capability[2], capability[3]);
	}

	public void setCapabilty(YANGCapability capability) {
		this.capability = new String[4];
		this.capability[0] = capability.getCapabilityBaseURI();
		this.capability[1] = capability.getNamespaceURI();
		this.capability[2] = capability.getPrefix();
		this.capability[3] = capability.getModuleName();
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}

	public String[] getFilterParam() {
		return filterParam;
	}

	public void setFilterParam(String[] filterParam) {
		this.filterParam = filterParam;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeStringArray(capability);
		dest.writeString(filter);
		dest.writeInt(filterParam.length);
		dest.writeStringArray(filterParam);

	}

	public static final Parcelable.Creator<SubtreeFilterSet> CREATOR = new Parcelable.Creator<SubtreeFilterSet>() {
		@Override
		public SubtreeFilterSet createFromParcel(Parcel in) {
			return new SubtreeFilterSet(in);
		}

		@Override
		public SubtreeFilterSet[] newArray(int size) {
			return new SubtreeFilterSet[size];
		}
	};
}
