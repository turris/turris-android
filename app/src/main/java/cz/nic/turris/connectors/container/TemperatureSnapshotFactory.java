/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.connectors.container;

import java.util.HashMap;
import java.util.Map;

import cz.nic.turris.connectors.NetconfTagDefinition;

public class TemperatureSnapshotFactory extends AbstractSnapshotArrayContainerFactory<TemperatureSnapshotFactory.TemperatureSnapshot> {

    @Override
    protected String prepareSnapshotParameterNodeName() {
        return NetconfTagDefinition.SNAPSHOT_TEMPERATURE;
    }

    @Override
    protected void prepareNodeActions(Map<String, SnapshotNodeAction<TemperatureSnapshotFactory.TemperatureSnapshot>> nodeActions) {
        nodeActions.put(NetconfTagDefinition.SNAPSHOT_TEMPERATURE_CPU, (temperatureSnapshot, node, time) -> temperatureSnapshot.cpu.put((double) time, Double.parseDouble(node.getTextContent())));
        nodeActions.put(NetconfTagDefinition.SNAPSHOT_TEMPERATURE_BOARD, (temperatureSnapshot, node, time) -> temperatureSnapshot.board.put((double) time, Double.parseDouble(node.getTextContent())));
    }

    @Override
    protected TemperatureSnapshot prepareOutputContainer() {
        return new TemperatureSnapshot();
    }


//////////////////////////// DATA CONTAINER

    public static class TemperatureSnapshot extends AbstractContainerFactory.Container {

        private Map<Double, Double> cpu;
        private Map<Double, Double> board;

        public Map<Double, Double> getCpu() {
            return cpu;
        }

        public Map<Double, Double> getBoard() {
            return board;
        }

        private TemperatureSnapshot(){
            cpu = new HashMap<>();
            board = new HashMap<>();
        }
    }
}