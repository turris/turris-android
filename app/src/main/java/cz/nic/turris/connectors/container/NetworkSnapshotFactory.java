/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.connectors.container;

import java.util.HashMap;
import java.util.Map;

import cz.nic.turris.connectors.NetconfTagDefinition;

public class NetworkSnapshotFactory extends AbstractSnapshotMapContainerFactory<NetworkSnapshotFactory.NetworkSnapshot> {

    private static final int    INITIAL_LAST_VALUE  = -1;
    //TODO mates: check if values are in bytes or bites
    private static final double KILO_BYTES_DIVIDER  = 1024.0;

    @Override
	protected String prepareSnapshotParameterNodeName() {
		return NetconfTagDefinition.SNAPSHOT_NETWORK;
	}

    @Override
    protected String prepareParameterDetailNodeName() {
        return NetconfTagDefinition.SNAPSHOT_NETWORK_INTERFACE;
    }

    @Override
	protected void prepareNodeActions(Map<String, SnapshotMapNodeAction<NetworkSnapshotFactory.NetworkSnapshot>> nodeActions) {
		nodeActions.put(NetconfTagDefinition.SNAPSHOT_NETWORK_INTERFACE_NAME, (networkSnapshot, node, time) -> networkSnapshot.name = node.getTextContent());
		nodeActions.put(NetconfTagDefinition.SNAPSHOT_NETWORK_INTERFACE_RX, (networkSnapshot, node, time) -> networkSnapshot.addRx(time, Double.parseDouble(node.getTextContent())));
		nodeActions.put(NetconfTagDefinition.SNAPSHOT_NETWORK_INTERFACE_TX, (networkSnapshot, node, time) -> networkSnapshot.addTx(time, Double.parseDouble(node.getTextContent())));
	}

	@Override
	protected void addContainerToMap(Map<String, NetworkSnapshot> map, NetworkSnapshot container, long time) {
		NetworkSnapshot networkSnapshot = map.get(container.getName());
		if (networkSnapshot != null) {
			networkSnapshot.addRx(time, container.getLastRx());
			networkSnapshot.addTx(time, container.getLastTx());
		} else {
			map.put(container.getName(), container);
		}
	}

	@Override
	protected Map<String, NetworkSnapshot> prepareMapContainer() {
		return new NetworkSnapshotsContainer();
	}

	@Override
	protected NetworkSnapshot prepareOutputContainer() {
		return new NetworkSnapshot();
	}


//////////////////////////// DATA CONTAINER

	public static class NetworkSnapshot extends AbstractSnapshotMapContainerFactory.InMapContainer {

		private String      		name;
		private Map<Double, Double>	rx;
		private Map<Double, Double>	tx;
		private double      		lastRx = INITIAL_LAST_VALUE;
		private double      		lastTx = INITIAL_LAST_VALUE;

		private NetworkSnapshot() {
			this.rx = new HashMap<>();
			this.tx = new HashMap<>();
		}

		public Map<Double, Double> getRx() {
			return rx;
		}

		public Map<Double, Double> getTx() {
			return tx;
		}

		private void addRx(long time, double value) {
            addValueToChart(rx, value, lastRx, time);
			lastRx = value;
		}

		private void addTx(long time, double value) {
            addValueToChart(tx, value, lastTx, time);
			lastTx = value;
		}

        private void addValueToChart(Map<Double, Double> series, double value, double lastValue, long time) {
            if (lastValue != INITIAL_LAST_VALUE) {
                series.put((double) time, (value - lastValue) / KILO_BYTES_DIVIDER);
            }
        }

        public double getLastRx() {
            return lastRx;
        }

        public double getLastTx() {
            return lastTx;
        }

		@Override
        public String getName() {
			return name;
		}

        @Override
        public String toString() {
            return name;
        }
    }

    public static class NetworkSnapshotsContainer extends HashMap<String, NetworkSnapshot> {}
}