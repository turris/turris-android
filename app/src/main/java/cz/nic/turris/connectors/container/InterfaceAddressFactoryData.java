/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.connectors.container;

import java.util.List;
import java.util.Map;

import cz.nic.turris.TurrisLog;
import cz.nic.turris.connectors.NetconfTagDefinition;

public class InterfaceAddressFactoryData extends AbstractSingleContainerFactory<InterfaceAddressFactoryData.InterfaceAddress> {

	@Override
	protected void prepareNodeNamePath(List<String> nodeNamePath) {}

	@Override
	protected void prepareNodeActions(Map<String, NodeAction<InterfaceAddressFactoryData.InterfaceAddress>> nodeActions) {
        nodeActions.put(NetconfTagDefinition.INTERFACE_ADDRESS_ADDRESS, (interfaceAddress, node) -> interfaceAddress.address = node.getTextContent());
        nodeActions.put(NetconfTagDefinition.INTERFACE_ADDRESS_TYPE, (interfaceAddress, node) -> interfaceAddress.type = InterfaceAddress.AddressType.getAddressTypeForTypeCode(node.getTextContent()));
	}

	@Override
	protected InterfaceAddress prepareOutputContainer() {
		return new InterfaceAddress();
	}


//////////////////////////// DATA CONTAINER

	public static class InterfaceAddress extends AbstractSingleContainerFactory.Container {

		private String      address;
		private AddressType type;

        public enum AddressType {

            TYPE_ETHER  ("link/ether"),
            TYPE_IPV4   ("inet"),
            TYPE_IPV6   ("inet6"),
            TYPE_UNKNOWN("unknown");

            private static final AddressType FALLBACK_TYPE = TYPE_UNKNOWN;

            private String typeCode;

            AddressType(String typeCode) {
                this.typeCode = typeCode;
            }

            public static AddressType getAddressTypeForTypeCode(String typeCode) {
                for (AddressType addressType : values()) {
                    if (addressType.typeCode.equals(typeCode)) {
                        return addressType;
                    }
                }
                TurrisLog.debug(AddressType.class, String.format("Cannot find WIFI role for role name: %s -> using: %s", typeCode, FALLBACK_TYPE.typeCode));
                return FALLBACK_TYPE;
            }
        }

		private InterfaceAddress() {}

		public String getAddress() {
			return address;
		}

		public AddressType getType() {
			return type;
		}
	}
}