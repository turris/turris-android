/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.token;

import android.annotation.SuppressLint;
import android.support.annotation.Nullable;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import cz.nic.turris.TurrisLog;
import cz.nic.turris.TurrisSettings;

public class TokenAccess {

    private static final String BEGIN_CERTIFICATE_STRING    = "-----BEGIN CERTIFICATE-----";
    private static final String END_CERTIFICATE_STRING      = "-----END CERTIFICATE-----";
    private static final String BEGIN_PRIVATE_KEY_STRING    = "-----BEGIN PRIVATE KEY-----";
    private static final String END_PRIVATE_KEY_STRING      = "-----END PRIVATE KEY-----";
    private static final String REQUEST_METHOD              = "GET";
    private static final int    RESPONSE_CODE_OK            = 200;
    private static final int    CONNECTION_TIMEOUT          = TurrisSettings.TOKEN_DOWNLOAD_CONNECTION_TIMEOUT;
    private static final int    READ_TIMEOUT                = TurrisSettings.TOKEN_DOWNLOAD_READ_TIMEOUT;


    private TokenResponseHandler tokenResponseHandler;

    public static class TokenResponse {

        public final String     url;
        public final boolean    validSSLCertificate;
        public final Integer    responseCode;
        public final byte[]     clientCertificate;
        public final byte[]     clientKey;
        public final byte[]     serverCertificate;

        private TokenResponse(String url, boolean validSSLCertificate, Integer responseCode, byte[] clientCertificate, byte[] clientKey, byte[] serverCertificate) {
            this.url = url;
            this.validSSLCertificate = validSSLCertificate;
            this.responseCode = responseCode;
            this.clientCertificate = clientCertificate;
            this.clientKey = clientKey;
            this.serverCertificate = serverCertificate;
        }

        private static TokenResponse createInvalidSSLCertificateTokenResponse(String url) {
            return new TokenResponse(url, false, null, null, null, null);
        }

        private static TokenResponse createValidSSLCertificateTokenResponse(String url, Integer responseCode, byte[] clientCertificate, byte[] clientKey, byte[] serverCertificate) {
            return new TokenResponse(url, false, responseCode, clientCertificate, clientKey, serverCertificate);
        }
    }

    private static class TurrisHttpsTrustManager implements X509TrustManager {

        private static TrustManager[]           trustManagers;
        private static final X509Certificate[]  acceptedIssuers = new X509Certificate[]{};

        @Override
        @SuppressLint("TrustAllX509TrustManager")
        public void checkClientTrusted(java.security.cert.X509Certificate[] x509Certificates, String s) throws java.security.cert.CertificateException {}

        @Override
        @SuppressLint("TrustAllX509TrustManager")
        public void checkServerTrusted(java.security.cert.X509Certificate[] x509Certificates, String s) throws java.security.cert.CertificateException {}

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return acceptedIssuers;
        }

        private static void allowAllSSL(boolean allowAll) {
            if (allowAll) {
                HttpsURLConnection.setDefaultHostnameVerifier((arg0, arg1) -> true);

                if (trustManagers == null) {
                    trustManagers = new TrustManager[]{new TurrisHttpsTrustManager()};
                }

                try {
                    SSLContext context = SSLContext.getInstance("TLS");
                    context.init(null, trustManagers, new SecureRandom());
                    HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
                } catch (NoSuchAlgorithmException | KeyManagementException e) {
                    TurrisLog.error(TurrisHttpsTrustManager.class, "Cannot skip SSL certificate check!", e);
                }
            }
        }

    }

    @FunctionalInterface
    public interface TokenResponseHandler {
        void tokenResponseAction(@Nullable TokenResponse tokenResponse);
    }

    public TokenAccess(TokenResponseHandler tokenResponseHandler) {
        this.tokenResponseHandler = tokenResponseHandler;
    }

    public void downloadFile(String tokenUrl, boolean allowAllSSL) {
        new Thread(() -> {
            InputStream is = null;
            try {
                TurrisHttpsTrustManager.allowAllSSL(allowAllSSL);
                URL url = new URL(tokenUrl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod(REQUEST_METHOD);
                conn.setDoInput(true);

                TurrisLog.info(TokenAccess.class, "Requested new token from URL: " + tokenUrl);
                conn.connect();

                int responseCode = conn.getResponseCode();
                byte[] clientCertificate    = null;
                byte[] clientKey            = null;
                byte[] serverCertificate    = null;

                TurrisLog.info(TokenAccess.class, "New token response code: " + responseCode);
                if (responseCode == RESPONSE_CODE_OK) {
                    byte[] responseData = getBytesFromInputStream(is = conn.getInputStream());

                    clientCertificate   = parseDERFromPEM(responseData, BEGIN_CERTIFICATE_STRING, END_CERTIFICATE_STRING, 0);
                    clientKey           = parseDERFromPEM(responseData, BEGIN_PRIVATE_KEY_STRING, END_PRIVATE_KEY_STRING, 0);
                    serverCertificate   = parseDERFromPEM(responseData, BEGIN_CERTIFICATE_STRING, END_CERTIFICATE_STRING, 1);
                }

                if (tokenResponseHandler != null) {
                    tokenResponseHandler.tokenResponseAction(TokenResponse.createValidSSLCertificateTokenResponse(tokenUrl, responseCode, clientCertificate, clientKey, serverCertificate));
                }
            } catch (IOException e) {
                if (e instanceof SSLHandshakeException) {
                    TurrisLog.debug(getClass(), "SSL handshake problem!", e);
                    tokenResponseHandler.tokenResponseAction(TokenResponse.createInvalidSSLCertificateTokenResponse(tokenUrl));
                } else {
                    TurrisLog.error(getClass(), "Error while downloading token!", e);
                    tokenResponseHandler.tokenResponseAction(null);
                }
            } finally {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e) {
                        TurrisLog.error(getClass(), "Error while closing input stream!", e);
                    }
                }
            }
        }).start();
    }

    private static byte[] getBytesFromInputStream(InputStream is) {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        try {
            byte[] buffer = new byte[0xFFFF];
            for (int len; (len = is.read(buffer)) != -1;) {
                os.write(buffer, 0, len);
            }
            os.flush();
            return os.toByteArray();
        } catch (IOException e) {
            TurrisLog.error(TokenAccess.class, "Cannot convert input stream to byte array!", e);
        } finally {
            try {
                os.close();
            } catch (IOException e) {
                TurrisLog.error(TokenAccess.class, "Cannot close output stream!", e);
            }
        }
        return null;
    }

    private static byte[] parseDERFromPEM(byte[] pem, String beginDelimiter, String endDelimiter, int tokenNum) {
        String[] tokens = new String(pem).split(beginDelimiter);
        tokens = tokens[1 + tokenNum].split(endDelimiter);
        return Base64.decode(tokens[0], Base64.DEFAULT);
    }
}
