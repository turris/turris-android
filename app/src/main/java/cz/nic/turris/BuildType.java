/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris;

import cz.nic.sentryclient.SentryLogger;
import cz.nic.turris.utility.Utility;

public enum BuildType {

    DEV     ("dev",     false,  SentryLogger.SentryLogEntryLevel.DEBUG),
    DEBUG   ("debug",   true,   SentryLogger.SentryLogEntryLevel.DEBUG),
    RELEASE ("release", true,   SentryLogger.SentryLogEntryLevel.INFO);

    private final static BuildType FALLBACK_VARIANT = BuildType.DEV;

    private final String                            key;
    private final boolean                           bugReport;
    private final SentryLogger.SentryLogEntryLevel  logEntryLevel;

    BuildType(String key, boolean bugReport, SentryLogger.SentryLogEntryLevel logEntryLevel) {
        this.key                    = key;
        this.bugReport              = bugReport;
        this.logEntryLevel          = logEntryLevel;
    }

    public boolean hasBugReport() {
        return bugReport;
    }

    public SentryLogger.SentryLogEntryLevel getLogEntryLevel() {
        return logEntryLevel;
    }

    @Override
    public String toString() {
        return name();
    }

    public static BuildType getBuildTypeForKey(String key) {
        if (Utility.isNotStringEmpty(key)) {
            for (BuildType buildType : BuildType.values()) {
                if (buildType.key.equals(key)) {
                    return buildType;
                }
            }
        }
        return FALLBACK_VARIANT;
    }
}
