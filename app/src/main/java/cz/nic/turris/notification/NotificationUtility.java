/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.service.notification.StatusBarNotification;
import android.support.v4.app.NotificationCompat;

import java.util.Arrays;

import cz.nic.turris.R;
import cz.nic.turris.TurrisLog;
import cz.nic.turris.TurrisSettings;
import cz.nic.turris.main.MainActivity;
import cz.nic.turris.connectors.MessagesCheckService;
import cz.nic.turris.database.RouterProfile;
import cz.nic.turris.database.UserNotification;
import cz.nic.turris.connectors.container.MessageFactoryData;

public class NotificationUtility {

    public static final String  MESSAGE_MAX_TIMESTAMP_KEY           = "message_max_timestamp";
    private static final String NOTIFICATION_CONTENT_BUNDLE_KEY     = "notification_content";
    private static final String NOTIFICATION_CONTENT_BODY_KEY       = "body";
    private static final String NOTIFICATION_CONTENT_TIMESTAMP_KEY  = "timestamp";

    public static boolean createNotification(Context context, Long routerId, MessageFactoryData.Message[] allMessages) {
        return createNotification(context, routerId, toBundleArray(allMessages));
    }

    private static boolean createNotification(Context context, Long routerId, Bundle[] allMessages) {
        if (allMessages == null || allMessages.length <= 0) {
            return false;
        }

        String routerName = RouterProfile.getRegisteredRouterWithId(routerId).getName();
        if (routerName == null) {
            TurrisLog.error(NotificationUtility.class, "Notification is not shown -> NO ROUTER WITH ID: " + routerId);
            return false;
        }

        /* Workaround for easy router name change in already displayed notification.
         * All notification data are stored in extra for easy notification update. */
        Bundle bundle = new Bundle();
        bundle.putParcelableArray(NOTIFICATION_CONTENT_BUNDLE_KEY, allMessages);

        Long alreadyNotifiedMessageTimestamp = UserNotification.getLastCheckedMessageTimestampForRouterId(routerId);
        long maxMessageTimestamp = 0;

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context);
        notificationBuilder.setSmallIcon(R.drawable.notify);
        notificationBuilder.setContentText(routerName);
        notificationBuilder.setExtras(bundle);

        if (allMessages.length == 1) {
            notificationBuilder.setContentTitle(allMessages[0].getString(NOTIFICATION_CONTENT_BODY_KEY));
            maxMessageTimestamp = allMessages[0].getLong(NOTIFICATION_CONTENT_TIMESTAMP_KEY);
        } else {
            notificationBuilder.setContentTitle(context.getResources().getQuantityString(R.plurals.notify_messages_content_title, allMessages.length, allMessages.length));
            NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
            inboxStyle.setSummaryText(routerName);
            for (Bundle message: allMessages) {
                long timestamp = message.getLong(NOTIFICATION_CONTENT_TIMESTAMP_KEY);
                inboxStyle.addLine(message.getString(NOTIFICATION_CONTENT_BODY_KEY));
                maxMessageTimestamp = timestamp > maxMessageTimestamp ? timestamp : maxMessageTimestamp;
            }
            notificationBuilder.setStyle(inboxStyle);
        }

        if (TurrisSettings.NOTIFY_ONLY_NEW_MESSAGES && alreadyNotifiedMessageTimestamp != null && maxMessageTimestamp <= alreadyNotifiedMessageTimestamp) {
            return false;
        }

        Intent resultIntent = new Intent(context, MainActivity.class);
        resultIntent.putExtra(MainActivity.TurrisMenuDefinition.MENU_DEFINITION_KEY, MainActivity.TurrisMenuDefinition.MESSAGES.getId());
        resultIntent.putExtra(MainActivity.ACTIVE_ROUTER_ID_KEY, routerId);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        Intent deleteIntent = new Intent(context, NotificationDeletedBroadcastReceiver.class);
        deleteIntent.putExtra(MainActivity.ACTIVE_ROUTER_ID_KEY, routerId);
        deleteIntent.putExtra(NotificationUtility.MESSAGE_MAX_TIMESTAMP_KEY, maxMessageTimestamp);

        notificationBuilder.setContentIntent(PendingIntent.getActivity(context, (int)System.currentTimeMillis(), resultIntent, PendingIntent.FLAG_UPDATE_CURRENT));
        notificationBuilder.setDeleteIntent(PendingIntent.getBroadcast(context, (int)System.currentTimeMillis(), deleteIntent, PendingIntent.FLAG_UPDATE_CURRENT));
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(routerId.intValue(), notificationBuilder.build());
        return true;
    }

    public static void updateNotification(Context context, Long routerId) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            /* Notification router name update with notification data stored in extra */
            for (StatusBarNotification statusBarNotification: notificationManager.getActiveNotifications()) {
                if (statusBarNotification.getId() == routerId) {
                    Parcelable[] notificationContent = statusBarNotification.getNotification().extras.getParcelableArray(NOTIFICATION_CONTENT_BUNDLE_KEY);
                    if (notificationContent != null) {
                        createNotification(context, routerId, Arrays.copyOf(notificationContent, notificationContent.length, Bundle[].class));
                    }
                }
            }
        } else {
            /* Under SDK version 23 must be notification canceled and created again */
            notificationManager.cancel(routerId.intValue());
            context.startService(new Intent(context, MessagesCheckService.class));
        }
    }

    public static void removeNotification(Context context, Long routerId, MessageFactoryData.Message[] messages) {
        saveMaxMessageTimestamp(routerId, messages);
        ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).cancel(routerId.intValue());
    }

    public static boolean saveMaxMessageTimestamp(Long routerId, MessageFactoryData.Message[] messages) {
        if (messages != null) {
            long maxMessageTimestamp = 0;
            for (MessageFactoryData.Message message: messages) {
                Long timestamp = message.getTimestamp();
                if (timestamp > maxMessageTimestamp) {
                    maxMessageTimestamp = timestamp;
                }
            }
            return saveMaxMessageTimestamp(routerId, maxMessageTimestamp);
        }
        return false;
    }

    public static boolean saveMaxMessageTimestamp(Long routerId, Long maxMessageTimestamp) {
        if (routerId == null) {
            TurrisLog.info(NotificationUtility.class, "Cannot save max message timestamp -> routerId is null!");
            return false;
        }
        return saveMaxMessageTimestamp(RouterProfile.getRegisteredRouterWithId(routerId), maxMessageTimestamp);
    }

    public static boolean saveMaxMessageTimestamp(RouterProfile routerProfile, Long maxMessageTimestamp) {
        return UserNotification.saveLastCheckedMessageTimestampForRouterProfile(routerProfile, maxMessageTimestamp);
    }


//////////////////////////// UTILITY

    private static Bundle[] toBundleArray(MessageFactoryData.Message[] messages) {
        if (messages == null) {
            return null;
        }
        Bundle[] result = new Bundle[messages.length];
        for (int i = 0; i < messages.length; i++) {
            MessageFactoryData.Message message = messages[i];
            Bundle bundle = new Bundle();
            bundle.putString(NOTIFICATION_CONTENT_BODY_KEY, message.getBody());
            bundle.putLong(NOTIFICATION_CONTENT_TIMESTAMP_KEY, message.getTimestamp());
            result[i] = bundle;
        }
        return result;
    }
}
