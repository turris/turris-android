/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.scanner;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.util.Size;
import android.view.Surface;
import android.view.TextureView;

import org.greenrobot.eventbus.Subscribe;

import java.nio.ByteBuffer;
import java.util.Arrays;

import cz.nic.turris.TurrisApplication;
import cz.nic.turris.TurrisLog;

import static android.support.v4.content.PermissionChecker.PERMISSION_GRANTED;

@SuppressLint("ViewConstructor")
@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
public class Camera2Preview extends TextureView {

    private static final int            IMAGE_FORMAT                = ImageFormat.YUV_420_888;
    private static final int            IMAGE_READER_BUFFER_LIMIT   = 4;
    private static final int            REQUIRED_CAMERA_LENS_FACING = CameraCharacteristics.LENS_FACING_BACK;
    private static final String         BACKGROUND_THREAD_NAME      = "BACKGROUND_THREAD";

    private CameraQRScanner         cameraQRScanner;
    private CameraManager           cameraManager;
    private String                  cameraId;
    private CameraDevice            cameraDevice;
    private CaptureRequest.Builder  captureRequestBuilder;
    private ImageReader             imageReader;
    private CameraCaptureSession    cameraCaptureSessions;
    private Size                    previewSize;
    private Handler                 backgroundHandler;
    private HandlerThread           backgroundThread;

    Camera2Preview(CameraQRScanner cameraQRScanner) {
        super(cameraQRScanner.getContext());
        this.cameraQRScanner = cameraQRScanner;

        setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {
            @Override
            public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
                ((TurrisApplication) getContext().getApplicationContext()).getEventBus().register(Camera2Preview.this);
                startBackgroundThread();
                prepareCamera(width, height);
                openCamera();
            }

            @Override
            public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {}

            @Override
            public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                ((TurrisApplication) getContext().getApplicationContext()).getEventBus().unregister(Camera2Preview.this);
                closeCamera();
                stopBackgroundThread();
                return false;
            }

            @Override
            public void onSurfaceTextureUpdated(SurfaceTexture surface) {}
        });
    }


//////////////////////////// BACKGROUND THREAD

    protected void startBackgroundThread() {
        backgroundThread = new HandlerThread(BACKGROUND_THREAD_NAME);
        backgroundThread.start();
        backgroundHandler = new Handler(backgroundThread.getLooper());
    }

    protected void stopBackgroundThread() {
        if (backgroundHandler != null) {
            backgroundThread.quitSafely();
            try {
                backgroundThread.join();
                backgroundThread = null;
                backgroundHandler = null;
            } catch (InterruptedException e) {
                TurrisLog.error(getClass(), "Error while stopping background thread", e);
            }
        }
    }


//////////////////////////// CAMERA CONTROL

    @Subscribe
    public void handleOpenCameraActionEvent(ScannerActivity.OpenCameraActionEvent openCameraActionEvent) {
        openCamera();
    }

    private void prepareCamera(int width, int height) {
        cameraManager = (CameraManager) getContext().getSystemService(Context.CAMERA_SERVICE);
        try {
            String[] cameraIdList = cameraManager.getCameraIdList();
            if (cameraIdList.length > 0) {
                this.cameraId = cameraIdList[0];
                for (String cameraId : cameraIdList) {
                    Integer lensFacing = cameraManager.getCameraCharacteristics(cameraId).get(CameraCharacteristics.LENS_FACING);
                    if (lensFacing != null && lensFacing == REQUIRED_CAMERA_LENS_FACING) {
                        this.cameraId = cameraId;
                    }
                }

                StreamConfigurationMap map = cameraManager.getCameraCharacteristics(cameraId).get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                if (map != null) {

                    Size[] outputSizes = map.getOutputSizes(SurfaceTexture.class);
                    CameraQRScanner.Size[] sizes = new CameraQRScanner.Size[outputSizes.length];
                    for(int i = 0; i < outputSizes.length; i++) {
                        Size outputSize = outputSizes[i];
                        sizes[i] = new CameraQRScanner.Size(outputSize.getWidth(), outputSize.getHeight());
                    }

                    CameraQRScanner.Size bestPreviewSize = CameraQRScanner.getBestPreviewSize(sizes, width, height);
                    previewSize = new Size(bestPreviewSize.width, bestPreviewSize.height);
                    imageReader = ImageReader.newInstance(previewSize.getWidth(), previewSize.getHeight(), IMAGE_FORMAT, IMAGE_READER_BUFFER_LIMIT);
                    imageReader.setOnImageAvailableListener(mOnImageAvailableListener, backgroundHandler);
                }
            } else {
                //TODO show no camera message
            }
        } catch (CameraAccessException e) {
            TurrisLog.error(getClass(), "Error while opening the camera", e);
        }
    }

    private void openCamera() {
        TurrisLog.debug(getClass(), "Opening the camera");
        try {
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) == PERMISSION_GRANTED) {
                cameraManager.openCamera(cameraId, new CameraDevice.StateCallback() {

                            @Override
                            public void onOpened(@NonNull CameraDevice cameraDevice) {
                                Camera2Preview.this.cameraDevice = cameraDevice;
                                createCameraPreview();
                            }

                            @Override
                            public void onDisconnected(@NonNull CameraDevice cameraDevice) {
                                cameraDevice.close();
                            }

                            @Override
                            public void onError(@NonNull CameraDevice cameraDevice, int i) {
                                cameraDevice.close();
                                Camera2Preview.this.cameraDevice = null;
                            }
                        },
                        backgroundHandler);
            } else {
                TurrisLog.info(getClass(), "No CAMERA permissions!");
                ((TurrisApplication) getContext().getApplicationContext()).getEventBus().post(new CameraQRScanner.CameraPermissionRequestEvent());
            }
        } catch (CameraAccessException e) {
            TurrisLog.error(getClass(), "Error while opening the camera", e);
        }
        TurrisLog.debug(getClass(), "Camera is opened");
    }

    private void closeCamera() {
        TurrisLog.debug(getClass(), "Closing the camera");
        if (cameraCaptureSessions != null) {
            cameraCaptureSessions.close();
            cameraCaptureSessions = null;
        }
        if (cameraDevice != null) {
            cameraDevice.close();
            cameraDevice = null;
        }
        if (imageReader != null) {
            imageReader.close();
            imageReader = null;
        }
        TurrisLog.debug(getClass(), "Camera is closed");
    }


//////////////////////////// CAMERA PREVIEW

    private void createCameraPreview() {
        try {
            if (previewSize != null) {
                SurfaceTexture texture = getSurfaceTexture();
                texture.setDefaultBufferSize(previewSize.getWidth(), previewSize.getHeight());
                Surface surface = new Surface(texture);
                Surface imageSurface = imageReader.getSurface();
                captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
                captureRequestBuilder.addTarget(imageSurface);
                captureRequestBuilder.addTarget(surface);
                cameraDevice.createCaptureSession(Arrays.asList(imageSurface, surface), new CameraCaptureSession.StateCallback() {

                            @Override
                            public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                                if (cameraDevice == null) {
                                    return;
                                }
                                cameraCaptureSessions = cameraCaptureSession;
                                updatePreview();
                            }

                            @Override
                            public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
                                TurrisLog.error(getClass(), "Camera device configuration failed!");
                            }

                        },
                        backgroundHandler);
            }
        } catch (CameraAccessException e) {
            TurrisLog.error(getClass(), "Can't access the camera", e);
        }
    }

    private void updatePreview() {
        new Thread(() -> {
            if (cameraDevice == null) {
                return;
            }
            captureRequestBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
            try {
                if (cameraCaptureSessions != null) {
                    cameraCaptureSessions.setRepeatingRequest(captureRequestBuilder.build(), null, backgroundHandler);
                }
            } catch (CameraAccessException e) {
                TurrisLog.error(getClass(), "Can't access the camera", e);
            }
        }).start();
    }


//////////////////////////// IMAGE PROCESSING

    private final ImageReader.OnImageAvailableListener mOnImageAvailableListener = new ImageReader.OnImageAvailableListener() {
        @Override
        public void onImageAvailable(ImageReader reader) {

            Image img = reader.acquireLatestImage();
            if(img == null) return;

            try {
                int width = img.getWidth();
                int height = img.getHeight();

                ByteBuffer yBuffer = img.getPlanes()[0].getBuffer();
                byte[] tempImageData = new byte[yBuffer.capacity()];
                yBuffer.get(tempImageData);

                cameraQRScanner.newCameraData(tempImageData, width, height);
            } catch (Exception e) {
                TurrisLog.error(Camera2Preview.class, "Cannot get image data!", e);
            } finally {
                img.close();
            }
        }
    };
}
