/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.scanner;

import android.Manifest;
import android.hardware.Camera;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import cz.nic.turris.TurrisApplication;
import cz.nic.turris.TurrisLog;

import static android.support.v4.content.PermissionChecker.PERMISSION_GRANTED;

@SuppressWarnings("deprecation")
public class Camera1Preview extends SurfaceView implements SurfaceHolder.Callback {

    private static final int    CAMERA_ORIENTATION_DEGREE   = 90;

    private Camera camera;
    private SurfaceHolder holder;
    private CameraQRScanner.Size bestPreviewSize;
    private List<Camera.Size> supportedPreviewSizes;

    private CameraQRScanner cameraQRScanner;

    public Camera1Preview(CameraQRScanner cameraQRScanner) {
        super(cameraQRScanner.getContext());
        this.cameraQRScanner = cameraQRScanner;

        holder = getHolder();
        holder.addCallback(this);
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }


//////////////////////////// CAMERA CONTROL

    private void openCamera() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PERMISSION_GRANTED) {
            TurrisLog.info(getClass(), "No CAMERA permissions!");
            ((TurrisApplication) getContext().getApplicationContext()).getEventBus().post(new CameraQRScanner.CameraPermissionRequestEvent());
        } else {
            closeCamera();
            camera = Camera.open();
            supportedPreviewSizes = camera.getParameters().getSupportedPreviewSizes();
            startPreview();
        }
    }

    private void closeCamera() {
        if (camera != null) {
            camera.stopPreview();
            camera.setPreviewCallback(null);
            camera.release();
            camera = null;
        }
    }

    private void startPreview() {
        if (camera != null && holder != null && holder.getSurface() != null) {
            try {
                camera.setPreviewDisplay(holder);

                Camera.Parameters parameters = camera.getParameters();
                if (bestPreviewSize != null) {
                    parameters.setPreviewSize(bestPreviewSize.width, bestPreviewSize.height);
                }

                if (checkAndEnableFocusMode(parameters, Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO) ||
                    checkAndEnableFocusMode(parameters, Camera.Parameters.FOCUS_MODE_AUTO) ||
                    checkAndEnableFocusMode(parameters, Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {

                    TurrisLog.info(getClass(), "Auto focus enabled");
                }

                camera.setParameters(parameters);

                camera.setDisplayOrientation(CAMERA_ORIENTATION_DEGREE);
                camera.setPreviewCallback((data, camera1) -> cameraQRScanner.newCameraData( data,
                                                                                            camera1.getParameters().getPreviewSize().width,
                                                                                            camera1.getParameters().getPreviewSize().height));
                camera.startPreview();
            } catch (Exception e) {
                TurrisLog.error(getClass(), "Error starting camera preview!", e);
            }
        }
    }

    private boolean checkAndEnableFocusMode(Camera.Parameters parameters, String focusMode) {
        if (parameters.getSupportedFocusModes().contains(focusMode)) {
            parameters.setFocusMode(focusMode);
            return true;
        }
        return false;
    }

    private void stopPreview() {
        if (camera != null) {
            try {
                camera.setPreviewCallback(null);
                camera.stopPreview();
            } catch (Exception e) {
                TurrisLog.debug(getClass(), "Tried to stop a non-existent camera preview");
            }
        }
    }


//////////////////////////// SURFACE HOLDER

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        ((TurrisApplication) getContext().getApplicationContext()).getEventBus().register(this);
        new Thread(this::openCamera).start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        stopPreview();
        startPreview();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        ((TurrisApplication) getContext().getApplicationContext()).getEventBus().unregister(this);
        closeCamera();

        if (holder != null) {
            holder.removeCallback(this);
            holder.getSurface().release();
        }
    }


//////////////////////////// ANDROID LIFECYCLE

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int width = resolveSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        final int height = resolveSize(getSuggestedMinimumHeight(), heightMeasureSpec);

        setMeasuredDimension(width, height);

        if (supportedPreviewSizes != null) {
            CameraQRScanner.Size[] sizes = new CameraQRScanner.Size[supportedPreviewSizes.size()];
            for (int i = 0; i < supportedPreviewSizes.size(); i++) {
                Camera.Size supportedSize = supportedPreviewSizes.get(i);
                sizes[i] = new CameraQRScanner.Size(supportedSize.width, supportedSize.height);
            }
            this.bestPreviewSize = CameraQRScanner.getBestPreviewSize(sizes, width, height);
        }
    }


//////////////////////////// EVENTS

    @Subscribe
    public void handleOpenCameraActionEvent(ScannerActivity.OpenCameraActionEvent openCameraActionEvent) {
        openCamera();
    }
}
