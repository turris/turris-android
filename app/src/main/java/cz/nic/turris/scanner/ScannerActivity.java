/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.scanner;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;

import org.greenrobot.eventbus.Subscribe;

import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.nic.turris.R;
import cz.nic.turris.TurrisLog;
import cz.nic.turris.TurrisSettings;
import cz.nic.turris.utility.Utility;
import cz.nic.turris.intro.IntroActivity;
import cz.nic.turris.main.MainActivity;
import cz.nic.turris.utility.activity.AbstractTurrisActivity;
import cz.nic.turris.database.RouterProfile;
import cz.nic.turris.token.TokenAccess;

import static android.support.v4.content.PermissionChecker.PERMISSION_GRANTED;

public class ScannerActivity extends AbstractTurrisActivity {

    private static final String     URL_SCHEME_SEPARATOR                    = "://";
    private static final Pattern    ROUTER_NAME_NUMBERING_PATTERN           = Pattern.compile("\\((\\d+)\\)(?!.*\\d)");
    private static final String     DEVICE_NEW_NAME_SUFFIX                  = "(%s)";
    private static final int        DEVICE_NEW_NAME_START_NUMBER            = 1;

    private static final int        CAMERA_FEEDBACK_VIBRATOR_DURATION       = TurrisSettings.CAMERA_FEEDBACK_VIBRATOR_DURATION;
    private static final int        CAMERA_PERMISSION_REQUEST_CODE          = 0;
    private static final int        ERROR_SNACKBAR_DURATION                 = 4000;
    private static final String     HELP_DIALOG_TAG                         = "dialog";
    private static final String     APP_SETTING_PACKAGE_KEY                 = "package:";

    private QRCodeContent   qrCodeContent;
    private CameraQRScanner cameraQRScanner;
    private Vibrator        vibrator;
    private Snackbar        noCameraPermissionsSnackbar;
    private FrameLayout     cameraPreviewPlaceholder;


//////////////////////////// DEVICE NAMING

    private static String checkAndUpdateDeviceName(String deviceName) {
        String newDeviceName = deviceName;
        while (RouterProfile.existsRouterWithName(newDeviceName)) {
            newDeviceName = createNewDeviceName(newDeviceName);
        }
        return newDeviceName;
    }

    private static String createNewDeviceName(String name) {
        Matcher matcher = ROUTER_NAME_NUMBERING_PATTERN.matcher(name);
        if (matcher.find()) {
            String groupWithNumber = matcher.group(matcher.groupCount());
            try {
                int deviceNumber = Integer.valueOf(groupWithNumber);
                return matcher.replaceAll(String.format(DEVICE_NEW_NAME_SUFFIX, ++deviceNumber));
            } catch (Exception e) {
                TurrisLog.debug(ScannerActivity.class, "Cannot convert value in brackets to integer: " + groupWithNumber);
                return null;
            }
        }
        return name + String.format(DEVICE_NEW_NAME_SUFFIX, DEVICE_NEW_NAME_START_NUMBER);
    }


//////////////////////////// TOKEN DOWNLOAD

    private enum TokenResponseCodeReaction {
        CODE_200(200, true, "200 - Token successfully downloaded!") {
            @Override
            protected void processAction(ScannerActivity scannerActivity, TokenAccess.TokenResponse tokenResponse) {
                if (RouterProfile.existsRouterWithName(scannerActivity.qrCodeContent.name)) {
                    TurrisLog.info(getClass(), "Router name in QR code already exists!");

                    scannerActivity.runOnUiThread(() -> {
                        String originalName = scannerActivity.qrCodeContent.name;
                        String newName = checkAndUpdateDeviceName(originalName);
                        if (newName == null) {
                            TurrisLog.error(getClass(), "Error while renaming existing router!");
                            return;
                        }

                        AlertDialog.Builder builder = new AlertDialog.Builder(scannerActivity);
                        builder.setMessage(scannerActivity.getString(R.string.device_name_already_exists, originalName, newName))
                                .setPositiveButton(R.string.register_button, (dialog, id) ->
                                    registerNewRouterProfileAndConnect(
                                            scannerActivity,
                                            newName,
                                            scannerActivity.qrCodeContent.host,
                                            scannerActivity.qrCodeContent.boardName,
                                            tokenResponse.clientCertificate,
                                            tokenResponse.clientKey,
                                            tokenResponse.serverCertificate
                                    ))
                                .setNegativeButton(R.string.cancel_button, (dialog, id) -> scannerActivity.enableQRReadingFast());
                        builder.show();
                    });

                    return;
                }

                registerNewRouterProfileAndConnect(
                        scannerActivity,
                        scannerActivity.qrCodeContent.name,
                        scannerActivity.qrCodeContent.host,
                        scannerActivity.qrCodeContent.boardName,
                        tokenResponse.clientCertificate,
                        tokenResponse.clientKey,
                        tokenResponse.serverCertificate
                );
            }
        },
        CODE_404(404, true, "404 - Token not found!") {
            @Override
            protected void processAction(ScannerActivity scannerActivity, TokenAccess.TokenResponse tokenResponse) {
                showErrorAndEnableQRScanner(scannerActivity, R.string.token_download_404, R.string.token_download_404_help_title, R.string.token_download_404_help_text);
            }
        },
        CODE_410(410, true, "410 - Expired link!") {
            @Override
            protected void processAction(ScannerActivity scannerActivity, TokenAccess.TokenResponse tokenResponse) {
                showErrorAndEnableQRScanner(scannerActivity, R.string.token_download_410, R.string.token_download_410_help_title, R.string.token_download_410_help_text);
            }
        },
        INVALID_SSL_CERTIFICATE (null, false, "Invalid SSL certificate!") {
            @Override
            protected void processAction(ScannerActivity scannerActivity, TokenAccess.TokenResponse tokenResponse) {
                scannerActivity.runOnUiThread(() -> {
                    AlertDialog.Builder builder = new AlertDialog.Builder(scannerActivity);
                    builder.setMessage(R.string.token_download_cert_invalid)
                            .setPositiveButton(R.string.token_download_cert_invalid_confirm, (dialog, id) -> scannerActivity.downloadToken(tokenResponse.url, true))
                            .setNegativeButton(R.string.cancel_button, (dialog, id) -> scannerActivity.enableQRReadingImmediately());
                    builder.show();
                });
            }
        },
        OTHER   (null, null, "Error while downloading!") {
            @Override
            protected void processAction(ScannerActivity scannerActivity, TokenAccess.TokenResponse tokenResponse) {
                showErrorAndEnableQRScanner(scannerActivity, R.string.token_download_error, R.string.token_download_error_help_title, R.string.token_download_error_help_text);
            }
        };

        private static TokenResponseCodeReaction FALLBACK_VARIANT = OTHER;

        private final Integer   code;
        private final Boolean   validSSLCertificate;
        private final String    logMessage;

        TokenResponseCodeReaction(Integer code, Boolean validSSLCertificate, String logMessage) {
            this.code = code;
            this.validSSLCertificate = validSSLCertificate;
            this.logMessage = logMessage;
        }

        private void processActionOnUIThread(ScannerActivity scannerActivity, TokenAccess.TokenResponse tokenResponse) {
            TurrisLog.info(getClass(), logMessage);
            scannerActivity.runOnUiThread(() -> processAction(scannerActivity, tokenResponse));
        }

        protected abstract void processAction(ScannerActivity scannerActivity, TokenAccess.TokenResponse tokenResponse);

        public static void processTokenResponse(ScannerActivity scannerActivity, TokenAccess.TokenResponse tokenResponse) {
            if (tokenResponse != null) {
                for (TokenResponseCodeReaction responseCodeReaction: values()) {
                    if (responseCodeReaction.validSSLCertificate == null || responseCodeReaction.validSSLCertificate.equals(tokenResponse.validSSLCertificate)
                            && responseCodeReaction.code == null || responseCodeReaction.code.equals(tokenResponse.responseCode)) {
                        responseCodeReaction.processActionOnUIThread(scannerActivity, tokenResponse);
                        return;
                    }
                }
            }
            FALLBACK_VARIANT.processActionOnUIThread(scannerActivity, tokenResponse);
        }
    }


//////////////////////////// UTILITY

    private static void showErrorAndEnableQRScanner(ScannerActivity scannerActivity, int messageId, Integer titleTextId, Integer helpTextId) {
        Timer snackbarTimer = new Timer();
        Snackbar snackbar = Utility.SnackbarNotification.ERROR.showSnackbar(scannerActivity, scannerActivity.findViewById(R.id.qr_scanner_camera_window), messageId, scannerActivity.getClass(), Snackbar.LENGTH_INDEFINITE);

        if (titleTextId != null && helpTextId != null) {
            snackbar.setAction(R.string.token_download_help, v -> {
                snackbarTimer.cancel();
                FragmentTransaction fragmentTransaction = scannerActivity.getSupportFragmentManager().beginTransaction();
                Fragment lastDialog = scannerActivity.getSupportFragmentManager().findFragmentByTag(HELP_DIALOG_TAG);
                if (lastDialog != null) {
                    fragmentTransaction.remove(lastDialog);
                }
                fragmentTransaction.addToBackStack(null);

                ScannerHelpDialog scannerHelpDialog = ScannerHelpDialog.newInstance(titleTextId, helpTextId);
                scannerHelpDialog.setOnDismissListener(scannerActivity::enableQRReadingImmediately);
                scannerHelpDialog.show(fragmentTransaction, HELP_DIALOG_TAG);
            });
        }

        snackbarTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                snackbar.dismiss();
                scannerActivity.enableQRReadingImmediately();
            }
        }, ERROR_SNACKBAR_DURATION);

        snackbar.show();
    }

    private void goToMainActivity() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    private void downloadToken(String url, boolean allowAllSSL) {
        new TokenAccess(tokenResponse -> TokenResponseCodeReaction.processTokenResponse(this, tokenResponse)).downloadFile(url, allowAllSSL);
    }

    private static void registerNewRouterProfileAndConnect(ScannerActivity scannerActivity, String name, String host, String boardName, byte[] clientCertificate, byte[] clientKey, byte[] serverCertificate) {
        RouterProfile.createNewRegisteredRouter(name, host, boardName, clientCertificate, clientKey, serverCertificate).setAsActive(scannerActivity.getTurrisApplication(), true);
        scannerActivity.goToMainActivity();
    }

    private void enableQRReadingFast() {
        runOnUiThread(() -> {
            initCameraQRScanner();
            cameraQRScanner.enableQRReadingFast();
        });
    }

    private void enableQRReadingImmediately() {
        runOnUiThread(() -> {
            initCameraQRScanner();
            cameraQRScanner.enableQRReadingImmediately();
        });
    }


//////////////////////////// QR SCANNER

    private static class QRCodeContent {

        private final String name;
        private final String host;
        private final String url;
        private final String boardName;

        private QRCodeContent(String name, String host, String url, String boardName) {
            this.name = name;
            this.host = host;
            this.url = url;
            this.boardName = boardName;
        }
    }

    private void processDataString(String dataUri) {
        qrCodeContent = prepareQRCodeContent(dataUri);

        if (qrCodeContent == null) {
            TurrisLog.info(getClass(), "Invalid QR code!");
            showErrorAndEnableQRScanner(this, R.string.qr_code_invalid, null, null);
            return;
        }

        downloadToken(qrCodeContent.url, false);
    }

    //TODO mates: create unit test
    @Nullable
    private QRCodeContent prepareQRCodeContent(String qrCodeData) {
        TurrisLog.info(getClass(), "Decoding QR code content: " + qrCodeData);

        if (Utility.isNotStringEmpty(qrCodeData)) {
            try {
                Uri qrUri = Uri.parse(qrCodeData);
                String scheme = qrUri.getScheme();
                String urlScheme = qrUri.getQueryParameter(getString(R.string.token_access_uri_download_uri_scheme_key));
                String path = qrUri.getEncodedPath();
                String name = qrUri.getQueryParameter(getString(R.string.token_access_uri_hostname_key));
                String host = qrUri.getHost();
                String boardName = qrUri.getQueryParameter(getString(R.string.token_access_uri_board_name_key));

                TurrisLog.info(getClass(), "QR code scheme: " + scheme);
                TurrisLog.info(getClass(), "QR code url scheme: " + urlScheme);
                TurrisLog.info(getClass(), "QR code path: " + path);
                TurrisLog.info(getClass(), "QR code host name: " + name);
                TurrisLog.info(getClass(), "QR code host: " + host);
                TurrisLog.info(getClass(), "QR code board name: " + boardName);

                if (Utility.isNotStringEmpty(scheme) && getString(R.string.token_access_uri_scheme).equals(scheme)
                        && Utility.isNotStringEmpty(urlScheme)
                        && Utility.isNotStringEmpty(path)
                        && Utility.isNotStringEmpty(name)
                        && Utility.isNotStringEmpty(host)
                        && Utility.isNotStringEmpty(boardName)) {

                    String url = urlScheme + URL_SCHEME_SEPARATOR + host + path;
                    TurrisLog.info(getClass(), "Token URL: " + url);
                    TurrisLog.info(getClass(), "VALID QR code content!");
                    return new QRCodeContent(name, host, url, boardName);
                }
            } catch (Throwable t) {
                TurrisLog.info(getClass(), "ERROR while parsing QR code content!", t);
            }
        }
        TurrisLog.info(getClass(), "INVALID QR code content!");
        return null;
    }

    private void initCameraQRScanner() {
        if (cameraQRScanner == null) {
            cameraQRScanner = new CameraQRScanner(this,
                    Utility.getFloatFromDimenItem(getResources(), R.dimen.qr_scanner_area_left),
                    Utility.getFloatFromDimenItem(getResources(), R.dimen.qr_scanner_area_top),
                    Utility.getFloatFromDimenItem(getResources(), R.dimen.qr_scanner_area_width),
                    Utility.getFloatFromDimenItem(getResources(), R.dimen.qr_scanner_area_height),
                    Utility.getFloatFromDimenItem(getResources(), R.dimen.qr_scanner_area_alpha),
                    R.color.qr_scanner_dimmer);

            cameraPreviewPlaceholder = ((FrameLayout) findViewById(R.id.qr_scanner_camera_preview_placeholder));
            cameraPreviewPlaceholder.addView(cameraQRScanner);
            cameraQRScanner.setCodeResultReader(dataString -> {
                vibrator.vibrate(CAMERA_FEEDBACK_VIBRATOR_DURATION);
                processDataString(dataString);
            });
        }
    }

    private void disposeCameraQRScanner() {
        if (cameraQRScanner != null) {
            cameraQRScanner.setCodeResultReader(null);
            cameraPreviewPlaceholder.removeAllViews();
            cameraQRScanner = null;
        }
    }


//////////////////////////// ACTIVITY LIFECYCLE

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getTurrisApplication().getEventBus().register(this);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_qr_scanner);

        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        (findViewById(R.id.qr_scanner_help_button)).setOnClickListener(v -> startActivity(new Intent(this, IntroActivity.class)));

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    protected void onStart() {
        super.onStart();
        /* process data string from intent -> third party app */
        String dataString = getIntent().getDataString();
        if (Utility.isNotStringEmpty(dataString)) {
            processDataString(dataString);
        } else {
            initCameraQRScanner();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        disposeCameraQRScanner();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkCameraPermissions();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getTurrisApplication().getEventBus().unregister(this);
    }

    @Override
    public void onBackPressed() {
        if (RouterProfile.existsRouter()) {
            goToMainActivity();
        } else {
            super.onBackPressed();
        }
    }


//////////////////////////// CAMERA PERMISSIONS

    private void checkCameraPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PERMISSION_GRANTED) {
            hideCameraPermissionErrorSnackbar();
        } else {
            showCameraPermissionErrorSnackbar();
        }
    }

    private void showCameraPermissionErrorSnackbar() {
        if (noCameraPermissionsSnackbar == null || !noCameraPermissionsSnackbar.isShown()) {
            noCameraPermissionsSnackbar = Utility.SnackbarNotification.ERROR.showSnackbar(this, findViewById(R.id.qr_scanner_camera_window), R.string.qr_scanner_no_camera_permission, getClass(), Snackbar.LENGTH_INDEFINITE);
            noCameraPermissionsSnackbar.setAction(R.string.qr_scanner_no_camera_permission_action, v -> {
                Intent appSettingsIntent = new Intent();
                appSettingsIntent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                appSettingsIntent.addCategory(Intent.CATEGORY_DEFAULT);
                appSettingsIntent.setData(Uri.parse(APP_SETTING_PACKAGE_KEY + getPackageName()));
                appSettingsIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                appSettingsIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                appSettingsIntent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                startActivity(appSettingsIntent);
            });
            noCameraPermissionsSnackbar.show();
        }
    }

    private void hideCameraPermissionErrorSnackbar() {
        if (noCameraPermissionsSnackbar != null) {
            noCameraPermissionsSnackbar.dismiss();
        }
        noCameraPermissionsSnackbar = null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (CAMERA_PERMISSION_REQUEST_CODE == requestCode && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            getTurrisApplication().getEventBus().post(new OpenCameraActionEvent());
            hideCameraPermissionErrorSnackbar();
        }
    }

    @Subscribe
    public void handleCameraPermissionRequestEvent(CameraQRScanner.CameraPermissionRequestEvent cameraPermissionRequestEvent) {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_REQUEST_CODE);
    }

    public static class OpenCameraActionEvent {
        private OpenCameraActionEvent() {}
    }
}
