/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.scanner;

import android.os.Build;
import android.support.percent.PercentLayoutHelper;
import android.support.percent.PercentRelativeLayout;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.PlanarYUVLuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;

import cz.nic.turris.R;
import cz.nic.turris.TurrisLog;

public class CameraQRScanner extends PercentRelativeLayout {

    private static final double         CAMERA_ASPECT_TOLERANCE                     = 0.1;
    private static final int            INVALID_QR_CODE_DIMMER_DELAY_IMMEDIATELY    = 1;
    private static final int            INVALID_QR_CODE_DIMMER_DELAY_FAST           = 1000;
    private static final int            INVALID_QR_CODE_DIMMER_DELAY_SLOW           = 3000;
    private static final int            INVALID_QR_CODE_DIMMER_FADEOUT_WAIT_TIME    = 500;
    private static final String         TARGET_DIMMER_BOTTOM_TAG                    = "targetDimmerBottom";
    private static final String         TARGET_DIMMER_LEFT_TAG                      = "targetDimmerLeft";
    private static final String         TARGET_DIMMER_TOP_TAG                       = "targetDimmerTop";
    private static final String         TARGET_DIMMER_RIGHT_TAG                     = "targetDimmerRight";

    private Dimmer                      dimmer;
    private MultiFormatReader           qrReader;
    private CameraQRScannerHandler      codeResultReader;
    private boolean                     qrReadEnabled   = true;
    private boolean                     dimmerAreaReady = false;

    private final float                 targetDimmerHeightPercentage;
    private final float                 targetDimmerLeftPercentage;
    private final float                 targetDimmerTopPercentage;
    private final float                 targetDimmerWidthPercentage;
    private final float                 targetDimmerAlpha;
    private final int                   targetDimmerColor;

    public CameraQRScanner(ScannerActivity scannerActivity,
                           float targetDimmerLeftPercentage,
                           float targetDimmerTopPercentage,
                           float targetDimmerWidthPercentage,
                           float targetDimmerHeightPercentage,
                           float targetDimmerAlpha,
                           int targetDimmerColor) {

        super(scannerActivity);

        this.targetDimmerLeftPercentage = targetDimmerLeftPercentage;
        this.targetDimmerTopPercentage = targetDimmerTopPercentage;
        this.targetDimmerWidthPercentage = targetDimmerWidthPercentage;
        this.targetDimmerHeightPercentage = targetDimmerHeightPercentage;
        this.targetDimmerAlpha = targetDimmerAlpha;
        this.targetDimmerColor = targetDimmerColor;

        init(scannerActivity);
    }

    private void init(ScannerActivity scannerActivity) {
        qrReader = new MultiFormatReader();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            TurrisLog.debug(getClass(), "Using Camera 2 for QR scanner");
            addView(new Camera2Preview(this));
        } else {
            TurrisLog.debug(getClass(), "Using Camera 1 for QR scanner");
            addView(new Camera1Preview(this));
        }

        addView(dimmer = new Dimmer(scannerActivity));
    }


//////////////////////////// DIMMER

    private static class Dimmer extends View {

        private final Animation fadeInAnimation;
        private final Animation fadeOutAnimation;

        private boolean         showing = false;
        private boolean         hiding  = false;

        public Dimmer(ScannerActivity scannerActivity) {
            super(scannerActivity);
            setBackgroundColor(ContextCompat.getColor(scannerActivity, android.R.color.black));
            setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

            fadeInAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in);
            fadeInAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationRepeat(Animation animation) {}

                @Override
                public void onAnimationEnd(Animation animation) {
                    hiding = false;
                }

                @Override
                public void onAnimationStart(Animation animation) {
                    scannerActivity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                    setVisibility(View.VISIBLE);
                }
            });

            fadeOutAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.fade_out);
            fadeOutAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationRepeat(Animation animation) {}
                @Override
                public void onAnimationStart(Animation animation) {}

                @Override
                public void onAnimationEnd(Animation animation) {
                    setVisibility(View.GONE);
                    scannerActivity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                    showing = false;
                }
            });
        }

        private void show() {
            if (!showing) {
                showing = true;
                startAnimation(fadeInAnimation);
            }
        }

        private void hide() {
            if (!hiding) {
                hiding = true;
                startAnimation(fadeOutAnimation);
            }
        }
    }

    private void showDimmer() {
        if (dimmer.getVisibility() != VISIBLE) {
            dimmer.post(() -> dimmer.show());
        }
    }

    private void hideDimmer() {
        if (dimmer.getVisibility() == VISIBLE) {
            dimmer.post(() -> dimmer.hide());
        }
    }

    public void setCodeResultReader(CameraQRScannerHandler codeResultReader) {
        this.codeResultReader = codeResultReader;
    }

    public void enableQRReadingSlow() {
        enableQRReading(INVALID_QR_CODE_DIMMER_DELAY_SLOW);
    }

    public void enableQRReadingFast() {
        enableQRReading(INVALID_QR_CODE_DIMMER_DELAY_FAST);
    }

    public void enableQRReadingImmediately() {
        enableQRReading(INVALID_QR_CODE_DIMMER_DELAY_IMMEDIATELY);
    }

    private void enableQRReading(int delay) {
        new Thread(() -> {
            synchronized (this) {
                try {
                    wait(delay);
                    hideDimmer();
                    while (dimmer.getVisibility() == VISIBLE) {
                        wait(INVALID_QR_CODE_DIMMER_FADEOUT_WAIT_TIME);
                    }
                } catch (InterruptedException e) {
                    TurrisLog.error(getClass(), "Cannot wait after QR code decode!", e);
                    dimmer.setVisibility(GONE);
                } finally {
                    qrReader.reset();
                    qrReadEnabled = true;
                }
            }
        }).start();
    }


//////////////////////////// ANDROID LIFECYCLE

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        float scanAreaLeftOffsetPercentage = targetDimmerLeftPercentage;
        float scanAreaTopOffsetPercentage = targetDimmerTopPercentage;
        float scanAreaWidthPercentage = targetDimmerWidthPercentage;
        float scanAreaHeightPercentage = targetDimmerHeightPercentage;
        float bottomAreaHeight = 1f - scanAreaTopOffsetPercentage - scanAreaHeightPercentage;
        float rightAreaWidth = 1 - scanAreaLeftOffsetPercentage - scanAreaWidthPercentage;
        float verticalAreaHeight = 1 - bottomAreaHeight - scanAreaTopOffsetPercentage;
        prepareCameraDimmerArea(
                TARGET_DIMMER_BOTTOM_TAG,
                1f,
                bottomAreaHeight,
                1 - bottomAreaHeight,
                0f);
        prepareCameraDimmerArea(
                TARGET_DIMMER_LEFT_TAG,
                scanAreaLeftOffsetPercentage,
                verticalAreaHeight,
                scanAreaTopOffsetPercentage,
                0f);
        prepareCameraDimmerArea(
                TARGET_DIMMER_TOP_TAG,
                1f,
                scanAreaTopOffsetPercentage,
                0f,
                0f);
        prepareCameraDimmerArea(
                TARGET_DIMMER_RIGHT_TAG,
                rightAreaWidth,
                verticalAreaHeight,
                scanAreaTopOffsetPercentage,
                scanAreaLeftOffsetPercentage + scanAreaWidthPercentage);
        dimmerAreaReady = true;
    }


//////////////////////////// QR SCANNER

    @FunctionalInterface
    public interface CameraQRScannerHandler {
        void qrCodeScanned(String qrCodeData);
    }

    public void newCameraData(byte[] bytes, int width, int height) {
        if (dimmerAreaReady && qrReadEnabled) {
            qrReadEnabled = false;
            hideDimmer();
            if (codeResultReader != null) {
                new Thread(() -> {
                    LuminanceSource source = new PlanarYUVLuminanceSource(
                            bytes,
                            width, height,
                            (int)(targetDimmerTopPercentage * width), (int)(targetDimmerLeftPercentage * height),
                            (int)(targetDimmerHeightPercentage * width), (int)(targetDimmerWidthPercentage * height), false);
                    try {
                        Result result = qrReader.decode(new BinaryBitmap(new HybridBinarizer(source)));
                        //qr code successfully loaded
                        showDimmer();
                        codeResultReader.qrCodeScanned(result.getText());
                    } catch (NotFoundException e) {
                        //no qr code
                        TurrisLog.debug(getClass(), "Cannot decode bitmap!", e);
                        qrReadEnabled = true;
                    } catch (Throwable e) {
                        TurrisLog.error(getClass(), "Error while decoding bitmap!", e);
                        qrReadEnabled = true;
                    }
                }).start();
            } else {
                qrReadEnabled = true;
            }
        }
    }


//////////////////////////// UTILITY

    private void prepareCameraDimmerArea(String tag, float widthPercent, float heightPercent, float topMarginPercent, float leftMarginPercent) {
        View targetDimmer = findViewWithTag(tag);
        if (targetDimmer == null) {
            targetDimmer = new View(getContext());
            targetDimmer.setTag(tag);
            targetDimmer.setBackgroundColor(ContextCompat.getColor(getContext(), targetDimmerColor));
            targetDimmer.setAlpha(targetDimmerAlpha);
            LayoutParams params = new LayoutParams(getContext(), null);
            targetDimmer.setLayoutParams(params);
            addView(targetDimmer);
        }
        PercentLayoutHelper.PercentLayoutInfo percentLayoutInfo = ((PercentRelativeLayout.LayoutParams) targetDimmer.getLayoutParams()).getPercentLayoutInfo();
        percentLayoutInfo.widthPercent = widthPercent;
        percentLayoutInfo.heightPercent = heightPercent;
        percentLayoutInfo.topMarginPercent = topMarginPercent;
        percentLayoutInfo.leftMarginPercent = leftMarginPercent;
    }

    static class Size {

        final int width;
        final int height;

        Size(int width, int height) {
            this.width = width;
            this.height = height;
        }
    }

    public static Size getBestPreviewSize(Size[] previewSizes, int width, int height) {
        if (previewSizes == null) {
            return null;
        }

        Size bestSize = null;
        double targetRatio = (double) height / width;
        double minDiff = Double.MAX_VALUE;
        for (Size previewSize : previewSizes) {
            double ratio = (double) previewSize.width / previewSize.height;
            if (Math.abs(ratio - targetRatio) > CAMERA_ASPECT_TOLERANCE) {
                continue;
            }

            if (Math.abs(previewSize.height - height) < minDiff) {
                bestSize = previewSize;
                minDiff = Math.abs(previewSize.height - height);
            }
        }

        if (bestSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Size size : previewSizes) {
                if (Math.abs(size.height - height) < minDiff) {
                    bestSize = size;
                    minDiff = Math.abs(size.height - height);
                }
            }
        }
        return bestSize;
    }


//////////////////////////// EVENTS

    public static class CameraPermissionRequestEvent {
        CameraPermissionRequestEvent() {}
    }
}
