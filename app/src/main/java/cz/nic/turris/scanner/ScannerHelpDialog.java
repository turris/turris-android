/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.scanner;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import cz.nic.turris.R;

public class ScannerHelpDialog extends DialogFragment {

    private static final String HELP_TEXT_RESOURCE_ID_BUNDLE_KEY = "helpTextResourceId";
    private static final String TITLE_TEXT_RESOURCE_ID_BUNDLE_KEY = "titleTextResourceId";

    private int helpTextResourceId;
    private int titleTextResourceId;
    private Runnable onDismissListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        titleTextResourceId = getArguments().getInt(TITLE_TEXT_RESOURCE_ID_BUNDLE_KEY);
        helpTextResourceId = getArguments().getInt(HELP_TEXT_RESOURCE_ID_BUNDLE_KEY);
        setCancelable(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.help_dialog_fragment, container, false);
        ((TextView) view.findViewById(R.id.help_dialog_title)).setText(getString(titleTextResourceId));
        ((TextView) view.findViewById(R.id.help_dialog_text)).setText(getString(helpTextResourceId));
        (view.findViewById(R.id.help_dialog_button_close)).setOnClickListener(v -> dismiss());

        return view;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (onDismissListener != null) {
            onDismissListener.run();
        }
    }

    public void setOnDismissListener(Runnable onDismissListener) {
        this.onDismissListener = onDismissListener;
    }

    public static ScannerHelpDialog newInstance(int titleTextResourceId, int helpTextResourceId) {
        ScannerHelpDialog scannerHelpDialog = new ScannerHelpDialog();

        Bundle bundle = new Bundle();
        bundle.putInt(TITLE_TEXT_RESOURCE_ID_BUNDLE_KEY, titleTextResourceId);
        bundle.putInt(HELP_TEXT_RESOURCE_ID_BUNDLE_KEY, helpTextResourceId);
        scannerHelpDialog.setArguments(bundle);

        return scannerHelpDialog;
    }
}
