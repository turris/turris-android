/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatDelegate;

import com.activeandroid.ActiveAndroid;

import org.greenrobot.eventbus.EventBus;

import cz.nic.sentryclient.SentryClient;
import cz.nic.sentryclient.SentryLogger;
import cz.nic.turris.receivers.TurrisAlarmReceiver;
import cz.nic.turris.utility.Utility;

public class TurrisApplication extends Application {

    private BuildType   buildType;
    private EventBus    eventBus;

    @Override
    public void onCreate() {
        TurrisLog.info(getClass(), String.format("--------------------- %s ---------------------", BuildConfig.APPLICATION_FULL_NAME));
        super.onCreate();
        prepareBuildType();
        initBugReporting();
        prepareEventBus();
        prepareORM();
        registerMessageCheckerAlarm(this);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    private void prepareBuildType() {
        buildType = BuildType.getBuildTypeForKey(BuildConfig.BUILD_TYPE);
        TurrisLog.info(getClass(), String.format("APPLICATION BUILD TYPE: %s", buildType));
    }

    private void prepareEventBus() {
        eventBus = EventBus.builder().eventInheritance(true).build();
    }

    private void prepareORM() {
        ActiveAndroid.initialize(this);
    }


    public BuildType getBuildType() {
        return buildType;
    }

    public EventBus getEventBus() {
        return eventBus;
    }

    public static void registerMessageCheckerAlarm(Context context) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setInexactRepeating(
                AlarmManager.ELAPSED_REALTIME,
                TurrisSettings.NOTIFY_ALARM_INTERVAL,
                TurrisSettings.NOTIFY_ALARM_INTERVAL,
                PendingIntent.getBroadcast(context, 0, new Intent(context, TurrisAlarmReceiver.class), 0));

    }

//////////////////////////// BUG REPORTING

    private void initBugReporting() {
        if (getBuildType().hasBugReport()) {
            //noinspection ConstantConditions
            if (Utility.isNotStringEmpty(BuildConfig.BUG_REPORT_KEY)) {
                TurrisLog.info(getClass(), "BUG REPORT ENABLED");
                SentryClient.initialize();
                SentryClient.getInstance().start(BuildConfig.BUG_REPORT_KEY);
                SentryClient.getInstance().setApplicationVersion(BuildConfig.VERSION_NAME);
                SentryClient.getInstance().setDefaultExceptionHandler();
                SentryClient.getInstance().setSentryLogger(new SentryLogger(TurrisSettings.MAX_SENTRY_LOGS, buildType.getLogEntryLevel()));
                return;
            }
            TurrisLog.warning(getClass(), "NO BUG REPORT ID PROVIDED");
        }
        TurrisLog.info(getClass(), "BUG REPORT DISABLED");
    }
}
