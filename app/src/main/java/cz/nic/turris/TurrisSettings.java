/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris;

import android.app.AlarmManager;

import cz.nic.turris.main.MainActivity;

public class TurrisSettings {

    private TurrisSettings() {}

    public static final int                                 DEFAULT_PORT                        = 6513;
    public static final int                                 NETCONF_CONNECTION_TIMEOUT          = 10000;
    public static final int                                 TOKEN_DOWNLOAD_CONNECTION_TIMEOUT   = 3000;
    public static final int                                 TOKEN_DOWNLOAD_READ_TIMEOUT         = 2000;
    public static final int                                 CAMERA_FEEDBACK_VIBRATOR_DURATION   = 100;

    public static final boolean                             SHOW_ONLY_UP_INTERFACES     = true;

    public static final boolean                             NOTIFY_ONLY_NEW_MESSAGES    = true;
    public static final long                                NOTIFY_ALARM_INTERVAL       = AlarmManager.INTERVAL_FIFTEEN_MINUTES;

    public static final MainActivity.TurrisMenuDefinition   INITIAL_MENU_ITEM           = MainActivity.TurrisMenuDefinition.EXTENDED_INFO;

    public static final int                                 MAX_SENTRY_LOGS             = 50;

    public static final LicenseDefinition                   APP_LICENCE                 = LicenseDefinition.GPLv3;
    public static final String                              URL_SOURCE_CODE             = "https://gitlab.labs.nic.cz/turris/turris-android";
    public static final String                              URL_TURRIS                  = "https://www.turris.cz/";
    public static final String                              URL_CZ_NIC                  = "https://www.nic.cz/";

    public enum LicenseDefinition {
        APACHE_2_0  ("Apache License Version 2.0",  "https://www.apache.org/licenses/LICENSE-2.0"),
        BSD         ("BSD License",                 "http://www.linfo.org/bsdlicense.html"),
        GPLv3       ("GPLv3",                       "https://www.gnu.org/licenses/gpl-3.0.txt"),
        MIT         ("MIT License",                 "https://opensource.org/licenses/MIT");

        public final String name;
        public final String link;

        LicenseDefinition(String name, String link) {
            this.name = name;
            this.link = link;
        }
    }

}
