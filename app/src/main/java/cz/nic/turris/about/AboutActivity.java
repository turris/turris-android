/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.about;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import cz.nic.turris.R;
import cz.nic.turris.TurrisSettings;
import cz.nic.turris.utility.activity.AbstractBackToolbarActivity;

public class AboutActivity extends AbstractBackToolbarActivity {

    private class DependencyAdapter extends RecyclerView.Adapter<DependencyAdapter.DependencyViewDetailHolder> {

        class DependencyViewDetailHolder extends RecyclerView.ViewHolder {

            private final TextView  dependencyTitle;
            private final TextView  dependencyLicence;
            private final View      dependencyNameGroup;
            private final View      dependencyLicenceGroup;

            private DependencyViewDetailHolder(View itemView) {
                super(itemView);
                this.dependencyTitle = (TextView) itemView.findViewById(R.id.about_dependency_title);
                this.dependencyLicence = (TextView) itemView.findViewById(R.id.about_dependency_licence);
                this.dependencyNameGroup = itemView.findViewById(R.id.about_dependency_name_group);
                this.dependencyLicenceGroup = itemView.findViewById(R.id.about_dependency_licence_group);
            }
        }


        private DependencyDefinition[] dependencies;

        private DependencyAdapter(DependencyDefinition[] dependencies) {
            this.dependencies = dependencies;
        }

        @Override
        public DependencyViewDetailHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new DependencyViewDetailHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.dependency_item, parent, false));
        }

        @Override
        public void onBindViewHolder(DependencyViewDetailHolder dependencyViewDetailHolder, int position) {
            if (dependencies != null) {
                DependencyDefinition dependency = dependencies[position];
                if (dependency != null) {
                    dependencyViewDetailHolder.dependencyTitle.setText(dependency.name);
                    dependencyViewDetailHolder.dependencyLicence.setText("(" + dependency.licenceDependency.name + ")");
                    dependencyViewDetailHolder.dependencyNameGroup.setOnClickListener(v -> openURL(dependency.link));
                    dependencyViewDetailHolder.dependencyLicenceGroup.setOnClickListener(v -> openURL(dependency.licenceDependency.link));
                }
            }
        }

        @Override
        public int getItemCount() {
            return dependencies != null ? dependencies.length : 0;
        }
    }

    private enum DependencyDefinition {
        EVENT_BUS                   ("EventBus",                "http://greenrobot.org/eventbus/",                      TurrisSettings.LicenseDefinition.APACHE_2_0),
        ACTIVE_ANDROID              ("ActiveAndroid",           "http://www.activeandroid.com/",                        TurrisSettings.LicenseDefinition.APACHE_2_0),
        MP_ANDROID_CHART            ("MPAndroidChart",          "https://github.com/PhilJay/MPAndroidChart",            TurrisSettings.LicenseDefinition.APACHE_2_0),
        JODA_TIME                   ("Joda-Time",               "http://www.joda.org/joda-time/",                       TurrisSettings.LicenseDefinition.APACHE_2_0),
        AV_LOADING_INDICATOR_VIEW   ("AVLoadingIndicatorView",  "https://github.com/81813780/AVLoadingIndicatorView",   TurrisSettings.LicenseDefinition.APACHE_2_0),
        ZXING                       ("ZXing",                   "https://github.com/zxing/zxing",                       TurrisSettings.LicenseDefinition.APACHE_2_0),
        MATERIAL_DRAWER             ("MaterialDrawer",          "http://mikepenz.github.io/MaterialDrawer/",            TurrisSettings.LicenseDefinition.APACHE_2_0),
        MATERIAL_INTRO_SCREEN       ("Material Intro Screen",   "https://github.com/TangoAgency/material-intro-screen", TurrisSettings.LicenseDefinition.MIT);

        private final String                                    name;
        private final String                                    link;
        private final TurrisSettings.LicenseDefinition          licenceDependency;

        DependencyDefinition(String name, String link, TurrisSettings.LicenseDefinition licenceDependency) {
            this.name = name;
            this.link = link;
            this.licenceDependency = licenceDependency;
        }
    }


//////////////////////////// CONSTRUCTOR

    public AboutActivity() {
        super(R.string.about_screen);
    }


//////////////////////////// ANDROID LIFECYCLE

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_activity);

        ((RecyclerView) findViewById(R.id.about_dependency_list)).setAdapter(new DependencyAdapter(DependencyDefinition.values()));
        (findViewById(R.id.about_turris_logo)).setOnClickListener(v -> openURL(TurrisSettings.URL_TURRIS));
        (findViewById(R.id.about_cznic_logo)).setOnClickListener(v -> openURL(TurrisSettings.URL_CZ_NIC));
        (findViewById(R.id.about_app_licence_title)).setOnClickListener(v -> openURL(TurrisSettings.APP_LICENCE.link));
        (findViewById(R.id.about_app_licence_description)).setOnClickListener(v -> openURL(TurrisSettings.APP_LICENCE.link));
        (findViewById(R.id.about_source_code_title)).setOnClickListener(v -> openURL(TurrisSettings.URL_SOURCE_CODE));
        TextView sourceCodeLink = (TextView) findViewById(R.id.about_source_code_link);
        sourceCodeLink.setText(TurrisSettings.URL_SOURCE_CODE);
        sourceCodeLink.setOnClickListener(v -> openURL(TurrisSettings.URL_SOURCE_CODE));
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.stay, R.anim.slide_out_right);
    }


//////////////////////////// UTILITY

    private void openURL(String url) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
    }
}
