/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.intro;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.CustomViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import agency.tango.materialintroscreen.MaterialIntroActivity;
import agency.tango.materialintroscreen.MessageButtonBehaviour;
import agency.tango.materialintroscreen.SlideFragment;
import agency.tango.materialintroscreen.widgets.OverScrollViewPager;
import agency.tango.materialintroscreen.widgets.SwipeableViewPager;
import cz.nic.turris.R;
import cz.nic.turris.utility.Utility;
import cz.nic.turris.scanner.ScannerActivity;

public class IntroActivity extends MaterialIntroActivity {

    public enum IntroPages {

        INSTALL_ACCESS_TOKENS   (R.drawable.intro_page_install_access_tokens,   R.string.intro_page_install_access_tokens_title,    R.string.intro_page_install_access_tokens_description),
        GENERATE_ACCESS_TOKEN   (R.drawable.intro_page_generate_access_token,   R.string.intro_page_generate_access_token_title,    R.string.intro_page_generate_access_token_description),
        OPEN_TOKEN_QR_CODE      (R.drawable.intro_page_open_token_qr_code,      R.string.intro_page_open_token_qr_code_title,       R.string.intro_page_open_token_qr_code_description, R.string.intro_page_open_token_qr_code_scanner_action, v -> {
            goToQRScanner(v.getContext());
        });

        private final int                   image;
        private final int                   title;
        private final int                   description;
        private final View.OnClickListener  buttonAction;
        private final int                   buttonTitle;

        IntroPages(int image, int title, int description) {
            this(image, title, description, -1, null);
        }

        IntroPages(int image, int title, int description, int buttonTitle, View.OnClickListener buttonAction) {
            this.image          = image;
            this.title          = title;
            this.description    = description;
            this.buttonAction   = buttonAction;
            this.buttonTitle    = buttonTitle;
        }

        protected String getPositionNumberString() {
            return (ordinal() + 1) + ". ";
        }
    }

    public static class TurrisFirstSlideFragment extends SlideFragment {

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            return inflater.inflate(R.layout.intro_first_slide_fragment, container, false);
        }

        @Override
        public int backgroundColor() {
            return R.color.intro_slide_background_color;
        }

        @Override
        public int buttonsColor() {
            return R.color.intro_slide_components_color;
        }

    }

    public static class TurrisHelpSlideFragment extends SlideFragment {

        private static final String TITLE                   = "title";
        private static final String DESCRIPTION             = "description";
        private static final String IMAGE                   = "image";

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            Bundle bundle = getArguments();
            View view = inflater.inflate(R.layout.intro_help_slide_fragment, container, false);
            ((ImageView) view.findViewById(R.id.intro_slide_top_image)).setImageResource(bundle.getInt(IMAGE));
            ((TextView) view.findViewById(R.id.intro_slide_title)).setText(Utility.fromHtml(bundle.getString(TITLE)));
            ((TextView) view.findViewById(R.id.intro_slide_description)).setText(Utility.fromHtml(bundle.getString(DESCRIPTION)));
            return view;
        }

        @Override
        public int backgroundColor() {
            return R.color.intro_slide_background_color;
        }

        @Override
        public int buttonsColor() {
            return R.color.intro_slide_components_color;
        }

        public static TurrisHelpSlideFragment createInstance(int image, String titleText, String descriptionText) {
            TurrisHelpSlideFragment slideFragment = new TurrisHelpSlideFragment();

            Bundle bundle = new Bundle();
            bundle.putInt(IMAGE, image);
            bundle.putString(TITLE, titleText);
            bundle.putString(DESCRIPTION, descriptionText);

            slideFragment.setArguments(bundle);
            return slideFragment;
        }

    }


//////////////////////////// ANDROID LIFECYCLE

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addSlide(new TurrisFirstSlideFragment());
        for (IntroPages introPage: IntroPages.values()) {
            addSlide(
                    TurrisHelpSlideFragment.createInstance(
                            introPage.image,
                            introPage.getPositionNumberString() + getText(introPage.title).toString(),
                            getText(introPage.description).toString()),
                    introPage.buttonAction != null ? new MessageButtonBehaviour(introPage.buttonAction, getString(introPage.buttonTitle)) : null);
        }

        ImageButton nextButton = (ImageButton) findViewById(agency.tango.materialintroscreen.R.id.button_next);
        SwipeableViewPager viewPager = ((OverScrollViewPager) findViewById(agency.tango.materialintroscreen.R.id.view_pager_slides)).getOverScrollView();
        viewPager.addOnPageChangeListener(new CustomViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
            @Override
            public void onPageScrollStateChanged(int state) {}

            @Override
            public void onPageSelected(int position) {
                if (position == IntroPages.values().length) {
                    if (nextButton.getVisibility() == View.VISIBLE) {
                        nextButton.startAnimation(AnimationUtils.loadAnimation(IntroActivity.this, agency.tango.materialintroscreen.R.anim.fade_out));
                        nextButton.setVisibility(View.INVISIBLE);
                    }
                } else {
                    if (nextButton.getVisibility() != View.VISIBLE) {
                        nextButton.startAnimation(AnimationUtils.loadAnimation(IntroActivity.this, agency.tango.materialintroscreen.R.anim.fade_in));
                        nextButton.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        hideBackButton();
    }

    @Override
    public void finish() {
        goToQRScanner(this);
        super.finish();
    }


//////////////////////////// UTILITY

    private static void goToQRScanner(Context context) {
        context.startActivity(new Intent(context, ScannerActivity.class));
    }
}
