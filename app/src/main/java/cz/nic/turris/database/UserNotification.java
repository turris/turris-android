/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.database;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import java.util.List;

import cz.nic.turris.TurrisLog;
import cz.nic.turris.notification.NotificationUtility;

public class UserNotification {

    private static final String UNIQUE_GROUP_ROUTER_LAST_MESSAGE    = "router_last_message_unique_group";

    private static final String COLUMN_ID                           = Table.DEFAULT_ID_NAME;
    private static final String COLUMN_ROUTER_PROFILE_MODEL         = "router_profile_model";
    private static final String COLUMN_LAST_MESSAGE_TIMESTAMP       = "last_checked_message_timestamp";

    final UserNotificationModel userNotificationModel;

    private UserNotification(UserNotificationModel userNotificationModel) {
        this.userNotificationModel = userNotificationModel;
    }


//////////////////////////// MODEL

    @SuppressWarnings("WeakerAccess") //cannot be private -> ActiveAndroid restriction
    @Table(name = "user_notification_model")
    public static class UserNotificationModel extends Model {
        @Column(name = COLUMN_ROUTER_PROFILE_MODEL, notNull = true, uniqueGroups = {UNIQUE_GROUP_ROUTER_LAST_MESSAGE})
        private RouterProfile.RouterProfileModel routerProfileModel;
        @Column(name = COLUMN_LAST_MESSAGE_TIMESTAMP, notNull = true, uniqueGroups = {UNIQUE_GROUP_ROUTER_LAST_MESSAGE})
        private Long lastCheckedMessageTimestamp;
    }


//////////////////////////// DATA ACCESS

    public Long getLastCheckedMessageTimestamp() {
        return userNotificationModel.lastCheckedMessageTimestamp;
    }


//////////////////////////// USER NOTIFICATION QUERIES

    public static Long getLastCheckedMessageTimestampForRouterId(Long routerId) {
        return getLastCheckedMessageTimestampForRouterProfile(RouterProfile.getRegisteredRouterWithId(routerId));
    }

    public static Long getLastCheckedMessageTimestampForRouterProfile(RouterProfile routerProfile) {
        UserNotification userNotification = getLastUserNotificationForRouterProfile(routerProfile);
        if (userNotification == null) {
            TurrisLog.debug(UserNotification.class, "No user notification saved for router profile: " + routerProfile);
            return null;
        }
        return userNotification.getLastCheckedMessageTimestamp();
    }

    public static UserNotification getLastUserNotificationForRouterProfile(RouterProfile routerProfile) {
        if (routerProfile == null) {
            TurrisLog.info(NotificationUtility.class, "Cannot get user notification for registered router -> registered router is null!");
            return null;
        }
        return toUserNotification(new Select().from(UserNotificationModel.class).where(COLUMN_ROUTER_PROFILE_MODEL + " = ?", routerProfile.getId()).orderBy(COLUMN_ID + " DESC").limit(1).executeSingle());
    }

    public static boolean saveLastCheckedMessageTimestampForRouterProfile(RouterProfile routerProfile, Long lastCheckedMessageTimestamp) {
        if (lastCheckedMessageTimestamp == null) {
            TurrisLog.info(NotificationUtility.class, "Cannot save max message timestamp -> last checked message timestamp is null!");
            return false;
        }
        UserNotification lastUserNotification = getLastUserNotificationForRouterProfile(routerProfile);
        if (lastUserNotification == null) {
            lastUserNotification = toUserNotification(new UserNotificationModel());
            lastUserNotification.userNotificationModel.routerProfileModel = routerProfile.routerProfileModel;
        } else if (lastCheckedMessageTimestamp <= lastUserNotification.getLastCheckedMessageTimestamp()) {
            return false;
        }
        lastUserNotification.userNotificationModel.lastCheckedMessageTimestamp = lastCheckedMessageTimestamp;
        lastUserNotification.userNotificationModel.save();
        return true;
    }

    public static boolean deleteAllUserNotificationsForRouterProfile(RouterProfile routerProfile) {
        if (routerProfile == null) {
            TurrisLog.info(NotificationUtility.class, "Cannot delete user notification for registered router -> registered router is null!");
            return false;
        }
        List<Model> execute = new Delete().from(UserNotificationModel.class).where(COLUMN_ROUTER_PROFILE_MODEL + " = ?", routerProfile.getId()).execute();
        return execute != null && execute.size() > 0;
    }


//////////////////////////// UTILITY

    @Override
    public String toString() {
        return getClass().getSimpleName() + ": [" + userNotificationModel.routerProfileModel + "] -> " + userNotificationModel.lastCheckedMessageTimestamp;
    }

    private static UserNotification toUserNotification(UserNotificationModel userNotificationModel) {
        return userNotificationModel != null ? new UserNotification(userNotificationModel) : null;
    }
}
