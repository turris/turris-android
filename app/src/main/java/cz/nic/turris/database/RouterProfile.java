/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.ByteArrayInputStream;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.ArrayList;
import java.util.List;

import cz.nic.turris.TurrisApplication;
import cz.nic.turris.TurrisLog;
import cz.nic.turris.notification.NotificationUtility;

public class RouterProfile {

    private static final String PREFERENCES_NAME            = "lastUsedRouterPref";
    private static final String LAST_USED_ROUTER_KEY        = "lastUsedRouter";

    private static final String RSA_KEY                     = "RSA";
    private static final String X509_KEY                    = "X.509";

    private static final String COLUMN_ID                   = Table.DEFAULT_ID_NAME;
    private static final String COLUMN_NAME                 = "name";
    private static final String COLUMN_HOST_NAME            = "host_name";
    private static final String COLUMN_BOARD_NAME           = "board_name";
    private static final String COLUMN_CLIENT_CERTIFICATE   = "client_certificate";
    private static final String COLUMN_CLIENT_KEY           = "client_key";
    private static final String COLUMN_SERVER_CERTIFICATE   = "server_certificate";

    final RouterProfileModel routerProfileModel;

    private RouterProfile(RouterProfileModel routerProfileModel) {
        this.routerProfileModel = routerProfileModel;
    }


//////////////////////////// MODEL

    @SuppressWarnings("WeakerAccess") //cannot be private -> ActiveAndroid restriction
    @Table(name = "router_profile_model")
    public static class RouterProfileModel extends Model {
        @Column(name = COLUMN_NAME, notNull = true, unique = true)
        private String name;
        @Column(name = COLUMN_HOST_NAME, notNull = true)
        private String hostName;
        @Column(name = COLUMN_BOARD_NAME, notNull = true)
        private String boardName;
        @Column(name = COLUMN_CLIENT_CERTIFICATE, notNull = true)
        private byte[] clientCertificate;
        @Column(name = COLUMN_CLIENT_KEY, notNull = true)
        private byte[] clientKey;
        @Column(name = COLUMN_SERVER_CERTIFICATE, notNull = true)
        private byte[] serverCertificate;
    }


//////////////////////////// EVENTS

    public static class RouterProfileActiveEvent {

        private final RouterProfile activeRouterProfile;
        private final boolean       goToInitialScreen;

        private RouterProfileActiveEvent(RouterProfile activeRouterProfile, boolean goToInitialScreen) {
            this.activeRouterProfile    = activeRouterProfile;
            this.goToInitialScreen      = goToInitialScreen;
        }

        public RouterProfile getActiveRouterProfile() {
            return activeRouterProfile;
        }

        public boolean isGoToInitialScreen() {
            return goToInitialScreen;
        }
    }

    public static class RouterProfileRenamedEvent {

        final RouterProfile routerProfile;

        private RouterProfileRenamedEvent(RouterProfile routerProfile) {
            this.routerProfile = routerProfile;
        }

        public RouterProfile getRouterProfile() {
            return routerProfile;
        }
    }

    public static class RouterProfileDeletedEvent {
        private RouterProfileDeletedEvent() {
        }
    }


//////////////////////////// DATA ACCESS

    public Long getId() {
        return routerProfileModel.getId();
    }

    public String getName() {
        return routerProfileModel.name;
    }

    public String getHostName() {
        return routerProfileModel.hostName;
    }

    public String getBoardName() {
        return routerProfileModel.boardName;
    }

    public byte[] getClientCertificate() {
        return routerProfileModel.clientCertificate;
    }

    public byte[] getClientKey() {
        return routerProfileModel.clientKey;
    }

    public byte[] getServerCertificate() {
        return routerProfileModel.serverCertificate;
    }


//////////////////////////// ROUTER PROFILE QUERIES

    public static RouterProfile createNewRegisteredRouter(String name, String host, String boardName, byte[] clientCertificate, byte[] clientKey, byte[] serverCertificate) {
        RouterProfileModel registeredRouterModel      = new RouterProfileModel();
        registeredRouterModel.name                    = name;
        registeredRouterModel.hostName                = host;
        registeredRouterModel.boardName               = boardName;
        registeredRouterModel.clientCertificate       = clientCertificate;
        registeredRouterModel.clientKey               = clientKey;
        registeredRouterModel.serverCertificate       = serverCertificate;
        registeredRouterModel.save();
        return toRegisteredRouter(registeredRouterModel);
    }

    public void setAsActive(TurrisApplication turrisApplication, boolean goToInitialScreen) {
        SharedPreferences settings = turrisApplication.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putLong(LAST_USED_ROUTER_KEY, routerProfileModel.getId());
        editor.apply();
        turrisApplication.getEventBus().post(new RouterProfileActiveEvent(this, goToInitialScreen));
    }

    public synchronized static RouterProfile getActiveRouter(Context context) {
        if (context != null) {
            SharedPreferences settings = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
            if (settings != null) {
                long lastUsedRouterId = settings.getLong(LAST_USED_ROUTER_KEY, -1);
                return toRegisteredRouter(RouterProfileModel.load(RouterProfileModel.class, lastUsedRouterId));
            }
        }
        return null;
    }

    public void rename(TurrisApplication turrisApplication, String name) {
        routerProfileModel.name = name;
        routerProfileModel.save();
        turrisApplication.getEventBus().post(new RouterProfileRenamedEvent(this));
    }

    public void delete(TurrisApplication turrisApplication) {
        ActiveAndroid.beginTransaction();
        try {
            if (this.equals(getActiveRouter(turrisApplication))) {
                SharedPreferences settings = turrisApplication.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.remove(LAST_USED_ROUTER_KEY);
                editor.apply();
            }

            UserNotification.deleteAllUserNotificationsForRouterProfile(this);
            routerProfileModel.delete();
            NotificationUtility.removeNotification(turrisApplication, routerProfileModel.getId(), null);
            turrisApplication.getEventBus().post(new RouterProfileDeletedEvent());
            ActiveAndroid.setTransactionSuccessful();
        } finally {
            ActiveAndroid.endTransaction();
        }
    }

    public synchronized static boolean existsRouter() {
        return new Select().from(RouterProfileModel.class).limit(1).exists();
    }

    public synchronized static boolean existsRouterWithName(String name) {
        return new Select().from(RouterProfileModel.class).where(COLUMN_NAME + " = ?", name).exists();
    }

    public static RouterProfile getLastRegisteredRouter() {
        return toRegisteredRouter(new Select().from(RouterProfileModel.class).orderBy(COLUMN_ID + " DESC").limit(1).executeSingle());
    }

    public static List<RouterProfile> getAllRegisteredRouters() {
        return toRegisteredRouterList(new Select().from(RouterProfileModel.class).orderBy(COLUMN_ID + " DESC").execute());
    }

    public synchronized static RouterProfile getRegisteredRouterWithId(Long id) {
        if (id == null) {
            return null;
        }
        return toRegisteredRouter(new Select().from(RouterProfileModel.class).where(COLUMN_ID + " = ?", id).executeSingle());
    }


//////////////////////////// UTILITY

    @Nullable
    public static X509Certificate getX509CertificateFromBytes(@NonNull byte[] data) {
        try {
            CertificateFactory factory = CertificateFactory.getInstance(X509_KEY);
            return (X509Certificate) factory.generateCertificate(new ByteArrayInputStream(data));
        } catch (CertificateException e) {
            TurrisLog.error(RouterProfile.class, "Cannot get X509 certificate from bytes data!", e);
        }
        return null;
    }

    @Nullable
    public static RSAPrivateKey getRsaPrivateKeyFromBytes(@NonNull byte[] data) {
        try {
            PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(data);
            KeyFactory factory = KeyFactory.getInstance(RSA_KEY);
            return (RSAPrivateKey) factory.generatePrivate(spec);
        } catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
            TurrisLog.error(RouterProfile.class, "Cannot get RSA private key from bytes data!", e);
        }
        return null;
    }

    @Override
    public String toString() {
        return "RouterProfile: " + getName() + " (" + getHostName() + ")";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof RouterProfile)) {
            return false;
        }
        return routerProfileModel.equals(((RouterProfile) obj).routerProfileModel);
    }

    private static RouterProfile toRegisteredRouter(RouterProfileModel routerProfileModel) {
        return routerProfileModel != null ? new RouterProfile(routerProfileModel) : null;
    }

    private static List<RouterProfile> toRegisteredRouterList(List<RouterProfileModel> routerProfileModels) {
        if (routerProfileModels == null) {
            return null;
        }
        List<RouterProfile> routerProfiles = new ArrayList<>();
        for (RouterProfileModel routerProfileModel : routerProfileModels) {
            routerProfiles.add(toRegisteredRouter(routerProfileModel));
        }
        return routerProfiles;
    }
}
