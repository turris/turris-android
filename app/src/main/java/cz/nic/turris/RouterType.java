/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris;

import cz.nic.turris.utility.Utility;

public enum RouterType {

    TURRIS_10               ("rtrs01"),
    TURRIS_11               ("rtrs02"),
    TURRIS_OMNIA_01         ("rtrom01"),
    TURRIS_OMNIA_01_1G      ("rtrom-01-1g"),
    TURRIS_OMNIA_01_2G      ("rtrom-01-2g"),
    TURRIS_OMNIA_01_1G_NW   ("rtrom-01-1g-nw"),
    TURRIS_UNKNOWN          ("unknown");

    private static final RouterType FALLBACK_VARIANT = TURRIS_UNKNOWN;

    public String modelId;

    RouterType(String modelId) {
        this.modelId = modelId;
    }

    public static RouterType getRouterTypeForModelId(String modelId) {
        if (Utility.isNotStringEmpty(modelId)) {
            String lowerCaseModelId = modelId.toLowerCase();
            for (RouterType routerType: RouterType.values()) {
                if (routerType.modelId.toLowerCase().equals(lowerCaseModelId)) {
                    return routerType;
                }
            }
        }
        return FALLBACK_VARIANT;
    }
}
