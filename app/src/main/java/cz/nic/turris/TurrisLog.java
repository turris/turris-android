/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris;

import android.util.Log;

import cz.nic.sentryclient.SentryClient;

public final class TurrisLog {

    private TurrisLog() {}

    @SuppressWarnings("unused")
    public static void error(Class clazz, String message) {
        error(clazz.getSimpleName(), message);
    }

    @SuppressWarnings("unused")
    public static void error(String tag, String message) {
        error(tag, message, null);
    }

    @SuppressWarnings("unused")
    public static void error(Class clazz, String message, Throwable throwable) {
        error(clazz.getSimpleName(), message, throwable);
    }

    @SuppressWarnings("unused")
    public static void error(String tag, String message, Throwable throwable) {
        if (throwable != null) {
            Log.e(tag, message, throwable);
        } else {
            Log.e(tag, message);
        }
        if(SentryClient.isRunning() && SentryClient.getInstance().hasSentryLogger()) {
            SentryClient.getInstance().getSentryLogger().onErrorLogged(tag, message);
        }
    }

    @SuppressWarnings("unused")
    public static void warning(Class clazz, String message) {
        warning(clazz.getSimpleName(), message);
    }

    @SuppressWarnings("unused")
    public static void warning(String tag, String message) {
        warning(tag, message, null);
    }

    @SuppressWarnings("unused")
    public static void warning(Class clazz, String message, Throwable throwable) {
        warning(clazz.getSimpleName(), message, throwable);
    }

    @SuppressWarnings("unused")
    public static void warning(String tag, String message, Throwable throwable) {
        if (throwable != null) {
            Log.w(tag, message, throwable);
        } else {
            Log.w(tag, message);
        }
        if(SentryClient.isRunning() && SentryClient.getInstance().hasSentryLogger()) {
            SentryClient.getInstance().getSentryLogger().onWarningLogged(tag, message);
        }
    }

    @SuppressWarnings("unused")
    public static void info(Class clazz, String message) {
        info(clazz.getSimpleName(), message);
    }

    @SuppressWarnings("unused")
    public static void info(String tag, String message) {
        info(tag, message, null);
    }

    @SuppressWarnings("unused")
    public static void info(Class clazz, String message, Throwable throwable) {
        info(clazz.getSimpleName(), message, throwable);
    }

    @SuppressWarnings("unused")
    public static void info(String tag, String message, Throwable throwable) {
        if (throwable != null) {
            Log.i(tag, message, throwable);
        } else {
            Log.i(tag, message);
        }
        if(SentryClient.isRunning() && SentryClient.getInstance().hasSentryLogger()) {
            SentryClient.getInstance().getSentryLogger().onInfoLogged(tag, message);
        }
    }

    @SuppressWarnings("unused")
    public static void debug(Class clazz, String message) {
        debug(clazz.getSimpleName(), message);
    }

    @SuppressWarnings("unused")
    public static void debug(String tag, String message) {
        debug(tag, message, null);
    }

    @SuppressWarnings("unused")
    public static void debug(Class clazz, String message, Throwable throwable) {
        debug(clazz.getSimpleName(), message, throwable);
    }

    @SuppressWarnings("unused")
    public static void debug(String tag, String message, Throwable throwable) {
        if (throwable != null) {
            Log.d(tag, message, throwable);
        } else {
            Log.d(tag, message);
        }
        if(SentryClient.isRunning() && SentryClient.getInstance().hasSentryLogger()) {
            SentryClient.getInstance().getSentryLogger().onDebugLogged(tag, message);
        }
    }
}
