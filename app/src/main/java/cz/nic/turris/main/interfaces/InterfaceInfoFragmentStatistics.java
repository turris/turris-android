/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.main.interfaces;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import cz.nic.turris.R;
import cz.nic.turris.utility.Utility;
import cz.nic.turris.connectors.container.InterfaceAddressFactoryData;
import cz.nic.turris.connectors.container.InterfaceListItemFactoryData;

public class InterfaceInfoFragmentStatistics extends AbstractInterfaceFragment {

    public enum MenuInterfaceType {

        GENERIC     (InterfaceListItemFactoryData.InterfaceListItem.InterfaceType.GENERIC,  R.drawable.network_ethernet),
        BRIDGE      (InterfaceListItemFactoryData.InterfaceListItem.InterfaceType.BRIDGE,   R.drawable.network_ethernet),
        WIRELESS    (InterfaceListItemFactoryData.InterfaceListItem.InterfaceType.WIRELESS, R.drawable.network_wireless);

        private final InterfaceListItemFactoryData.InterfaceListItem.InterfaceType interfaceType;
        private final Integer interfaceIconId;

        MenuInterfaceType(InterfaceListItemFactoryData.InterfaceListItem.InterfaceType interfaceType, Integer interfaceIconId) {
            this.interfaceType = interfaceType;
            this.interfaceIconId = interfaceIconId;
        }

        public Integer getInterfaceIconId() {
            return interfaceIconId;
        }

        public static MenuInterfaceType getMenuInterfaceTypeForInterfaceType(InterfaceListItemFactoryData.InterfaceListItem.InterfaceType interfaceType) {
            for (MenuInterfaceType menuInterfaceType : values()) {
                if (menuInterfaceType.interfaceType.equals(interfaceType)) {
                    return menuInterfaceType;
                }
            }
            return GENERIC;
        }
    }

	private TextView    ipAddressText;
    private TextView    macAddressText;
    private ImageView   iconImage;


//////////////////////////// ANDROID LIFECYCLE

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.interface_info_fragment_statistics, container, false);

        ipAddressText   = (TextView) view.findViewById(R.id.interface_info_statistics_ip_address);
        macAddressText  = (TextView) view.findViewById(R.id.interface_info_statistics_mac_address);
        iconImage       = (ImageView) view.findViewById(R.id.interface_info_statistics_icon);

        ((TextView) view.findViewById(R.id.interface_info_name)).setText(getInterfaceName());
		
		return view;
	}


//////////////////////////// CONTENT REFRESH

    @Override
    protected void updateView(InterfaceListItemFactoryData.InterfaceListItem interfaceListItem) {
        if (isAdded()) {
            ipAddressText.setText(R.string.unkown);
            macAddressText.setText(R.string.unkown);

            Integer interfaceIconId = InterfaceInfoFragmentStatistics.MenuInterfaceType.getMenuInterfaceTypeForInterfaceType(interfaceListItem.getType()).getInterfaceIconId();
            if (interfaceIconId != null) {
                iconImage.setImageResource(interfaceIconId);
            }

            for (InterfaceAddressFactoryData.InterfaceAddress interfaceAddress: interfaceListItem.getAddressList()) {
                AddressTypeAction.getAddressTypeActionForAddressType(interfaceAddress.getType()).action(this, interfaceAddress);
            }
        }
    }

    private enum AddressTypeAction {
        ETHERNET_ACTION (InterfaceAddressFactoryData.InterfaceAddress.AddressType.TYPE_ETHER,
                        (fragment, address) -> {addNewLineToNotEmptyTextView(fragment.macAddressText);
                                                fragment.macAddressText.append(address);}),
        IPV4_ACTION     (InterfaceAddressFactoryData.InterfaceAddress.AddressType.TYPE_IPV4,
                        (fragment, address) -> {addNewLineToNotEmptyTextView(fragment.ipAddressText);
                                                fragment.ipAddressText.append(address);}),
        IPV6_ACTION     (InterfaceAddressFactoryData.InterfaceAddress.AddressType.TYPE_IPV6,
                        (fragment, address) -> {addNewLineToNotEmptyTextView(fragment.ipAddressText);
                                                fragment.ipAddressText.append(address);}),
        NO_ACTION(null, (fragment, address) -> {});

        @FunctionalInterface
        private interface AddressAction {
            void doAddressAction(InterfaceInfoFragmentStatistics fragment, String address);
        }

        private InterfaceAddressFactoryData.InterfaceAddress.AddressType addressType;
        private AddressAction addressAction;

        AddressTypeAction(InterfaceAddressFactoryData.InterfaceAddress.AddressType addressType, AddressAction addressAction) {
            this.addressType = addressType;
            this.addressAction = addressAction;
        }

        public void action(InterfaceInfoFragmentStatistics fragment, InterfaceAddressFactoryData.InterfaceAddress interfaceAddress) {
            addressAction.doAddressAction(fragment, interfaceAddress.getAddress());
        }

        protected static void addNewLineToNotEmptyTextView(TextView textView) {
            if(Utility.isNotStringEmpty(textView.getText().toString())) textView.append(Utility.LINE_SEPARATOR);
        }

        public static AddressTypeAction getAddressTypeActionForAddressType(InterfaceAddressFactoryData.InterfaceAddress.AddressType addressType) {
            for (AddressTypeAction addressTypeAction: values()) {
                if (addressTypeAction.addressType != null && addressTypeAction.addressType.equals(addressType)) {
                    return addressTypeAction;
                }
            }
            return NO_ACTION;
        }
    }


//////////////////////////// INSTANCE ACCESS

	public static InterfaceInfoFragmentStatistics newInstance(String interfaceName) {
		InterfaceInfoFragmentStatistics fragment = new InterfaceInfoFragmentStatistics();
		Bundle bundle = new Bundle();
		bundle.putString(INTERFACE_NAME_BUNDLE_KEY, interfaceName);
		fragment.setArguments(bundle);
		return fragment;
	}
}
