/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.main.routerInfo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.greenrobot.eventbus.Subscribe;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;

import java.util.Arrays;

import cz.nic.turris.R;
import cz.nic.turris.RouterType;
import cz.nic.turris.utility.Utility;
import cz.nic.turris.utility.activity.AbstractTurrisActivity;
import cz.nic.turris.connectors.container.RouterExtendedInfoFactoryData;

public class RouterInfoFragmentGeneral extends Fragment {

    private TextView    hostNameTextView;
    private ImageView   typeIconImageView;
	private TextView    typeNameTextView;
    private TextView    boardNameTextView;
    private TextView    kernelVersionTextView;
    private TextView    turrisOSVersionTextView;
    private TextView    localTimeTextView;


//////////////////////////// ANDROID LIFECYCLE

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.router_info_fragment_general, container, false);

        hostNameTextView        = (TextView)    layout.findViewById(R.id.router_info_hostname);
        typeNameTextView        = (TextView)    layout.findViewById(R.id.router_info_type_name);
        boardNameTextView       = (TextView)    layout.findViewById(R.id.router_info_board_name);
        typeIconImageView       = (ImageView)   layout.findViewById(R.id.router_info_type_icon);
        kernelVersionTextView   = (TextView)    layout.findViewById(R.id.router_info_kernel);
        turrisOSVersionTextView = (TextView)    layout.findViewById(R.id.router_info_turris_os_version);
        localTimeTextView       = (TextView)    layout.findViewById(R.id.router_info_localtime);

        return layout;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        handleRouterExtendedInfoData(((AbstractTurrisActivity) getActivity()).getTurrisApplication().getEventBus().getStickyEvent(RouterExtendedInfoFactoryData.RouterExtendedInfo.class));
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AbstractTurrisActivity) getActivity()).getTurrisApplication().getEventBus().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        ((AbstractTurrisActivity) getActivity()).getTurrisApplication().getEventBus().unregister(this);
    }


//////////////////////////// DATA HANDLERS

    @Subscribe
    public void handleRouterExtendedInfoData(RouterExtendedInfoFactoryData.RouterExtendedInfo routerExtendedInfo) {
        if (routerExtendedInfo != null) {
            updateExtendedInfoInUIThread(routerExtendedInfo);
        }
    }


//////////////////////////// CONTENT REFRESH

    private void updateExtendedInfoInUIThread(@NonNull RouterExtendedInfoFactoryData.RouterExtendedInfo routerExtendedInfo) {
        getActivity().runOnUiThread(() -> updateExtendedInfo(routerExtendedInfo));
    }

	private void updateExtendedInfo(@NonNull RouterExtendedInfoFactoryData.RouterExtendedInfo routerExtendedInfo) {
        if (isAdded()) {
            String boardName = routerExtendedInfo.getBoardName();
            RouterTypeResources routerTypeResources = RouterTypeResources.getRouterTypeResourceForRouterType(RouterType.getRouterTypeForModelId(boardName));

            hostNameTextView.setText(routerExtendedInfo.getHostName());
            typeNameTextView.setText(getString(routerTypeResources.modelNameResource));
            boardNameTextView.setText(getString(R.string.router_board_name_brackets, boardName));
            typeIconImageView.setImageResource(routerTypeResources.modelLogoResource);
            kernelVersionTextView.setText(routerExtendedInfo.getKernelVersion());
            turrisOSVersionTextView.setText(routerExtendedInfo.getTurrisOsVersion());
            localTimeTextView.setText(DateTimeFormat.longDateTime().withLocale(Utility.getCurrentLocale(getContext())).withZone(DateTimeZone.getDefault()).print(routerExtendedInfo.getLocalTime()));
        }
	}


//////////////////////////// ROUTER TYPE RESOURCES

    public enum RouterTypeResources {

        TURRIS_10               (R.string.router_type_turris_10,        R.drawable.router_type_logo_turris,  R.drawable.router_type_icon_turris,  RouterType.TURRIS_10),
        TURRIS_11               (R.string.router_type_turris_11,        R.drawable.router_type_logo_turris,  R.drawable.router_type_icon_turris,  RouterType.TURRIS_11),
        TURRIS_OMNIA_01         (R.string.router_type_turris_omnia_10,  R.drawable.router_type_logo_omnia,   R.drawable.router_type_icon_omnia,   RouterType.TURRIS_OMNIA_01, RouterType.TURRIS_OMNIA_01_1G, RouterType.TURRIS_OMNIA_01_2G, RouterType.TURRIS_OMNIA_01_1G_NW),
        TURRIS_UNKNOWN          (R.string.router_type_turris_unknown,   R.drawable.router_type_logo_unknown, R.drawable.router_type_icon_unknown, RouterType.TURRIS_UNKNOWN);

        private static final RouterTypeResources FALLBACK_VARIANT = TURRIS_UNKNOWN;

        private RouterType[]    routerTypes;
        private int             modelNameResource;
        private int             modelLogoResource;
        private int             modelIconResource;

        RouterTypeResources(int modelNameResource, int modelLogoResource, int modelIconResource, RouterType... routerType) {
            this.modelNameResource = modelNameResource;
            this.modelLogoResource = modelLogoResource;
            this.modelIconResource = modelIconResource;
            this.routerTypes = routerType;
        }

        public int getModelIconResource() {
            return modelIconResource;
        }

        public static RouterTypeResources getRouterTypeResourceForRouterType(RouterType routerType) {
            if (routerType != null) {
                for (RouterTypeResources routerTypeResources: RouterTypeResources.values()) {
                    if (Arrays.asList(routerTypeResources.routerTypes).contains(routerType)) {
                        return routerTypeResources;
                    }
                }
            }
            return FALLBACK_VARIANT;
        }
    }
}
