/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.main.messages;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;

import cz.nic.turris.R;
import cz.nic.turris.utility.activity.AbstractBackToolbarActivity;

public class MessageDetailActivity extends AbstractBackToolbarActivity {

    public MessageDetailActivity() {
        super(R.string.message_detail);
    }


//////////////////////////// ANDROID LIFECYCLE

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.message_detail_activity);

        Intent intent = getIntent();
        if (intent == null || intent.getExtras() == null) {
            finish();
            return;
        }

        Bundle extras = intent.getExtras();
        String messageId = extras.getString(MessageDetailFragment.MESSAGE_ID_BUNDLE_KEY);
        String messageSeverity = extras.getString(MessageDetailFragment.MESSAGE_SEVERITY_BUNDLE_KEY);
        String messageBody = extras.getString(MessageDetailFragment.MESSAGE_BODY_BUNDLE_KEY);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.message_detail_fragment_placeholder, MessageDetailFragment.newInstance(messageId, messageSeverity, messageBody), messageId);
        fragmentTransaction.commit();
    }

    @Override
    protected void onStop() {
        super.onPause();
        if (!isChangingConfigurations()) {
            // do not show this activity after resume from background -> netconf service has no connection
            finish();
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
