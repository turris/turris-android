/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.main.messages;

import org.greenrobot.eventbus.EventBus;

import cz.nic.turris.connectors.DataRefreshService;
import cz.nic.turris.connectors.event.NetconfCommandEvent;

public final class MessageAccess {

    public static class MessageRemoveEvent {

        private final String    messageId;
        private final boolean   forceRefresh;

        private MessageRemoveEvent(String messageId, boolean forceRefresh) {
            this.messageId = messageId;
            this.forceRefresh = forceRefresh;
        }

        public String getMessageId() {
            return messageId;
        }

        public boolean isForceRefresh() {
            return forceRefresh;
        }
    }

    private MessageAccess() {}

    public static void removeMessage(EventBus eventBus, String messageId) {
        removeMessage(eventBus, messageId, true);
    }

    public static void removeMessage(EventBus eventBus, String messageId, boolean forceRefresh) {
        eventBus.postSticky(new MessageRemoveEvent(messageId, forceRefresh));
        eventBus.postSticky(new NetconfCommandEvent(DataRefreshService.NetConfServiceCommand.DISMISS_MESSAGE, messageId));
    }
}
