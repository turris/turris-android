/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.main.routerInfo;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import cz.nic.turris.R;
import cz.nic.turris.RouterType;
import cz.nic.turris.utility.Utility;
import cz.nic.turris.utility.activity.AbstractTurrisActivity;
import cz.nic.turris.utility.fragment.AbstractTimeChartFragment;
import cz.nic.turris.connectors.container.RouterExtendedInfoFactoryData;
import cz.nic.turris.connectors.container.TemperatureSnapshotFactory;

public class RouterInfoFragmentChartTemperature extends Fragment {

    private static final int        BOARD_TEMPERATURE_CHART_COLOR   = R.color.chart_temperature_board;
    private static final boolean    BOARD_TEMPERATURE_CHART_FILLED  = false;
    private static final int        CPU_TEMPERATURE_CHART_COLOR     = R.color.chart_temperature_cpu;
    private static final boolean    CPU_TEMPERATURE_CHART_FILLED    = false;
    private static final int        CHART_LABELS_MAX_X_COUNT        = 3;
    private static final long       TEMPERATURE_CHART_MAXIMUM_VALUE = 120L;


//////////////////////////// ABSTRACT CHART FRAGMENT

    public static class TemperatureChartFragment extends AbstractTimeChartFragment {

        private enum FirmwareTemperatureAction {
            TURRIS  ((Activity activity, List<ChartData> chartDataList, TemperatureSnapshotFactory.TemperatureSnapshot temperatureSnapshot) -> {
                chartDataList.add(new ChartData(activity.getString(R.string.router_info_chart_temperature_board),   temperatureSnapshot.getBoard(), BOARD_TEMPERATURE_CHART_COLOR,  BOARD_TEMPERATURE_CHART_FILLED));
                chartDataList.add(new ChartData(activity.getString(R.string.router_info_chart_temperature_cpu),     temperatureSnapshot.getCpu(),   CPU_TEMPERATURE_CHART_COLOR,    CPU_TEMPERATURE_CHART_FILLED));
            }, RouterType.TURRIS_10, RouterType.TURRIS_11),
            OMNIA   ((Activity activity, List<ChartData> chartDataList, TemperatureSnapshotFactory.TemperatureSnapshot temperatureSnapshot) -> {
                Map<Double, Double> boardTemperature = temperatureSnapshot.getBoard();
                Map<Double, Double> cpuTemperature = temperatureSnapshot.getCpu();
                /* workaround for Turris Omnia bug -> cpu temperature value is sent in bord temperature NetConf tag */
                if (cpuTemperature.containsValue(0d) && !boardTemperature.containsValue(0d)) {
                    cpuTemperature = boardTemperature;
                }
                chartDataList.add(new ChartData(activity.getString(R.string.router_info_chart_temperature_cpu), cpuTemperature, CPU_TEMPERATURE_CHART_COLOR, CPU_TEMPERATURE_CHART_FILLED));
            }, RouterType.TURRIS_OMNIA_01, RouterType.TURRIS_OMNIA_01_1G, RouterType.TURRIS_OMNIA_01_2G, RouterType.TURRIS_OMNIA_01_1G_NW);

            private static FirmwareTemperatureAction    FALLBACK_VARIANT = TURRIS;

            private final RouterType[]                  routerTypes;
            private final TemperatureDataAction         dataAction;

            @FunctionalInterface
            private interface TemperatureDataAction {
                void doAction(Activity activity, List<ChartData> chartDataList, TemperatureSnapshotFactory.TemperatureSnapshot temperatureSnapshot);
            }

            FirmwareTemperatureAction(TemperatureDataAction dataAction, RouterType... routerTypes) {
                this.dataAction = dataAction;
                this.routerTypes = routerTypes;
            }

            private void doFirmwareAction(Activity activity, List<ChartData> chartDataList, TemperatureSnapshotFactory.TemperatureSnapshot temperatureSnapshot) {
                dataAction.doAction(activity, chartDataList, temperatureSnapshot);
            }

            private static FirmwareTemperatureAction getActionForRouterType(RouterType routerType) {
                for (FirmwareTemperatureAction firmwareTemperatureAction: values()) {
                    if (routerType != null && Arrays.asList(firmwareTemperatureAction.routerTypes).contains(routerType)) {
                        return firmwareTemperatureAction;
                    }
                }
                return FALLBACK_VARIANT;
            }
        }

        @Override
        protected int prepareTitle() {
            return R.string.temperatures;
        }

        @Override
        protected int prepareChartMaxXCount() {
            return CHART_LABELS_MAX_X_COUNT;
        }

        @Override
        protected String getYAxisFormattedValues(float value) {
            return getResources().getString(R.string.chart_temperature_y_unit, Utility.getOneDecimalFormattedNumber(value));
        }

        @Override
        protected Long prepareChartMaximum() {
            return TEMPERATURE_CHART_MAXIMUM_VALUE;
        }

        @Subscribe
        public void handleTemperatureSnapshotData(TemperatureSnapshotFactory.TemperatureSnapshot temperatureSnapshot) {
            updateChart(prepareChartDataList(temperatureSnapshot), true);
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            TemperatureSnapshotFactory.TemperatureSnapshot temperatureSnapshot = ((AbstractTurrisActivity) getActivity()).getTurrisApplication().getEventBus().getStickyEvent(TemperatureSnapshotFactory.TemperatureSnapshot.class);
            if (temperatureSnapshot != null) {
                updateChart(prepareChartDataList(temperatureSnapshot), false);
            }
        }

        private List<ChartData> prepareChartDataList(@NonNull TemperatureSnapshotFactory.TemperatureSnapshot temperatureSnapshot) {
            List<ChartData> chartDataList = new ArrayList<>();
            FirmwareTemperatureAction.getActionForRouterType(RouterType.getRouterTypeForModelId(((AbstractTurrisActivity) getActivity()).getTurrisApplication().getEventBus().getStickyEvent(RouterExtendedInfoFactoryData.RouterExtendedInfo.class).getBoardName())).doFirmwareAction(getActivity(), chartDataList, temperatureSnapshot);
            return chartDataList;
        }

        @Override
        public void onResume() {
            super.onResume();
            ((AbstractTurrisActivity) getActivity()).getTurrisApplication().getEventBus().register(this);
        }

        @Override
        public void onPause() {
            super.onPause();
            ((AbstractTurrisActivity) getActivity()).getTurrisApplication().getEventBus().unregister(this);
        }
    }


//////////////////////////// ANDROID LIFECYCLE
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.router_info_temperature_chart_placeholder, new TemperatureChartFragment());
            fragmentTransaction.commit();
        }
		return inflater.inflate(R.layout.router_info_fragment_charts_temperature, container, false);
	}
}
