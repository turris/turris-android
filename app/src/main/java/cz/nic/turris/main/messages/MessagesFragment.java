/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.main.messages;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import cz.nic.turris.R;
import cz.nic.turris.utility.Utility;
import cz.nic.turris.utility.activity.AbstractTurrisActivity;
import cz.nic.turris.connectors.DataRefreshService;
import cz.nic.turris.database.RouterProfile;
import cz.nic.turris.connectors.container.MessageFactoryData;
import cz.nic.turris.notification.NotificationUtility;

public class MessagesFragment extends Fragment {

    private static final long REMOVE_MESSAGE_REFRESH_TIME_OFFSET = 1000;

    private MessageAdapter  messageAdapter;
    private Long            thresholdTimestamp;
    private ViewGroup       loadingIndicator;
    private ViewGroup       emptyIndicator;
    private boolean         firstLoadingComplete;


//////////////////////////// LIST ADAPTER

    ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {

        @Override
        public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            if (!((MessageAdapter.MessageViewHolder) viewHolder).canBeClosed) return 0;
            return super.getSwipeDirs(recyclerView, viewHolder);
        }

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
            messageAdapter.removeMessage(((MessageAdapter.MessageViewHolder) viewHolder).messageId, false);
        }
    });

    private class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageViewHolder> {

        private List<MessageFactoryData.Message> messages = null;

        public class MessageViewHolder extends RecyclerView.ViewHolder {

            private final View      messageLayout;
            private final TextView  messageText;
            private final TextView  message_control;
            private final TextView  message_type_text;
            private final View      message_type;

            private       String    messageId;
            private       boolean   canBeClosed;

            public MessageViewHolder(View itemView) {
                super(itemView);

                messageLayout       = itemView;
                messageText         = (TextView) itemView.findViewById(R.id.message_item_text);
                message_control     = (TextView) itemView.findViewById(R.id.message_item_control_message);
                message_type_text   = (TextView) itemView.findViewById(R.id.message_item_type_text);
                message_type        = itemView.findViewById(R.id.message_item_type);

                messageId           = null;
                canBeClosed         = true;
            }
        }

        private void setNewMessages(MessageFactoryData.Message[] messages) {
            if (messages != null && messages.length > 0) {
                if (thresholdTimestamp == null || thresholdTimestamp < messages[0].getContainerUpdateTimestamp()) {
                    List<MessageFactoryData.Message> newMessages = new ArrayList<>();
                    for (MessageFactoryData.Message message :messages) {
                        if (message != null && !message.isDisplayed()) {
                            newMessages.add(message);
                        }
                    }
                    if (!newMessages.equals(this.messages)) {
                        //double check threshold time -> don't display already deleted messages
                        if (thresholdTimestamp == null || thresholdTimestamp < messages[0].getContainerUpdateTimestamp()) {
                            this.messages = newMessages;
                            notifyDataSetChanged();
                        }
                    }
                }
            }
            refreshEmptyIndicator();
        }

        private void removeMessageWithId(String messageId, boolean forceRefresh) {
            if (messages != null) {
                int position = -1;
                for (int i = 0; i < messages.size(); i++) {
                    if (messages.get(i).getId().equals(messageId)) {
                        position = i;
                    }
                }
                if (position >= 0) {
                    messages.remove(position);
                    notifyItemRemoved(position);
                    if (forceRefresh) {
                        notifyDataSetChanged();
                    }
                    setThresholdTimestampToCurrent();
                    Utility.SnackbarNotification.INFO.showSnackbar(getContext(), getView(), R.string.message_fragment_dismiss_sent, getClass()).show();
                    refreshEmptyIndicator();
                }
            }
        }

        private void setThresholdTimestampToCurrent() {
            long currentTimeMillis = System.currentTimeMillis();
            if (thresholdTimestamp == null || currentTimeMillis > thresholdTimestamp) {
                thresholdTimestamp = currentTimeMillis + REMOVE_MESSAGE_REFRESH_TIME_OFFSET;
            }
        }

        private void removeMessage(String messageId, boolean forceRefresh) {
            MessageAccess.removeMessage(((AbstractTurrisActivity) getActivity()).getTurrisApplication().getEventBus(), messageId, forceRefresh);
        }

        @Override
        public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View messageItemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.messages_item, parent, false);
            return new MessageViewHolder(messageItemView);
        }

        @Override
        public void onBindViewHolder(MessageViewHolder holder, int position) {
            if (messages != null) {
                MessageFactoryData.Message message = messages.get(position);
                holder.messageText.setText(message.getBody());

                MessageSeverity messageSeverity = MessageSeverity.getMessageSeverityForSeverity(message.getSeverity());
                holder.message_control.setText(messageSeverity.getControlMessage());
                holder.message_type_text.setText(messageSeverity.getDescription());
                holder.message_type.setBackgroundColor(ContextCompat.getColor(getContext(), messageSeverity.getColor()));

                holder.messageId = message.getId();
                holder.canBeClosed = messageSeverity.isCanBeClosed();

                holder.messageLayout.setOnClickListener(v -> {
                    Intent messageDetailIntent = new Intent(getContext(), MessageDetailActivity.class);
                    messageDetailIntent.putExtra(MessageDetailFragment.MESSAGE_ID_BUNDLE_KEY,       message.getId());
                    messageDetailIntent.putExtra(MessageDetailFragment.MESSAGE_SEVERITY_BUNDLE_KEY, messageSeverity.getSeverity());
                    messageDetailIntent.putExtra(MessageDetailFragment.MESSAGE_BODY_BUNDLE_KEY,     message.getBody());
                    getActivity().startActivity(messageDetailIntent);
                    getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                });
            }
        }

        @Override
        public int getItemCount() {
            return messages != null ? messages.size() : 0;
        }
    }


//////////////////////////// EMPTY INDICATOR

    private void refreshEmptyIndicator() {
        refreshEmptyIndicator(((AbstractTurrisActivity) getActivity()).getTurrisApplication().getEventBus().getStickyEvent(DataRefreshService.NetconfServiceConnectionEvent.class));
    }

    private void refreshEmptyIndicator(DataRefreshService.NetconfServiceConnectionEvent netconfServiceConnectionEvent) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    if (loadingIndicator != null && !firstLoadingComplete) {
                        loadingIndicator.setVisibility((netconfServiceConnectionEvent != null && netconfServiceConnectionEvent.isConnected()) ? View.VISIBLE : View.GONE);
                    }
                    boolean showEmptyIndicator = messageAdapter.getItemCount() == 0 &&
                            netconfServiceConnectionEvent != null &&
                            netconfServiceConnectionEvent.isConnected() &&
                            loadingIndicator.getVisibility() != View.VISIBLE;
                    emptyIndicator.setVisibility(showEmptyIndicator ? View.VISIBLE : View.GONE);
                }
            }
        });
    }

//////////////////////////// EVENTS HANDLER

    @Subscribe
    public void handleDismissMessageNetconfCommand(MessageAccess.MessageRemoveEvent messageRemovedEvent) {
        if (messageRemovedEvent != null) {
            messageAdapter.removeMessageWithId(messageRemovedEvent.getMessageId(), messageRemovedEvent.isForceRefresh());
        }
    }

    @Subscribe
    public void handleNetconfServiceConnectionEvent(DataRefreshService.NetconfServiceConnectionEvent netconfServiceConnectionEvent) {
        refreshEmptyIndicator(netconfServiceConnectionEvent);
    }


//////////////////////////// DATA HANDLER

    @Subscribe
    public void handleMessagesData(MessageFactoryData.Message[] messages) {
        updateListInUIThread(messages);
    }

    private void updateListInUIThread(MessageFactoryData.Message[] messages) {
        getActivity().runOnUiThread(() -> updateList(messages));
    }

    private void updateList(MessageFactoryData.Message[] messages) {
        firstLoadingComplete = true;
        loadingIndicator.setVisibility(View.GONE);
        messageAdapter.setNewMessages(messages);
        NotificationUtility.removeNotification(getContext(), RouterProfile.getActiveRouter(getContext()).getId(), messages);
    }


//////////////////////////// ANDROID LIFECYCLE

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        firstLoadingComplete = false;
        View layout = inflater.inflate(R.layout.messages_fragment, container, false);
        RecyclerView messagesList = (RecyclerView) layout.findViewById(R.id.messages_list);
        messagesList.setAdapter(messageAdapter = new MessageAdapter());

        loadingIndicator = (ViewGroup) layout.findViewById(R.id.messages_loading_group);
        emptyIndicator = (ViewGroup) layout.findViewById(R.id.messages_empty_group);

        itemTouchHelper.attachToRecyclerView(messagesList);
        thresholdTimestamp = null;
        return layout;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus eventBus = ((AbstractTurrisActivity) getActivity()).getTurrisApplication().getEventBus();
        handleDismissMessageNetconfCommand(eventBus.getStickyEvent(MessageAccess.MessageRemoveEvent.class));
        eventBus.register(this);
        refreshEmptyIndicator();
    }

    @Override
    public void onStop() {
        super.onStop();
        ((AbstractTurrisActivity) getActivity()).getTurrisApplication().getEventBus().unregister(this);
    }
}
