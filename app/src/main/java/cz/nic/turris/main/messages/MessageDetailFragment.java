/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.main.messages;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import cz.nic.turris.R;
import cz.nic.turris.TurrisLog;
import cz.nic.turris.utility.Utility;
import cz.nic.turris.utility.activity.AbstractTurrisActivity;
import cz.nic.turris.connectors.NetconfControl;

public class MessageDetailFragment extends Fragment {

    public static final String MESSAGE_ID_BUNDLE_KEY        = "message_id";
    public static final String MESSAGE_SEVERITY_BUNDLE_KEY  = "message_severity";
    public static final String MESSAGE_BODY_BUNDLE_KEY      = "message_body";

    private String                              messageId;
    private MessageSeverity                     messageSeverity;
    private String                              messageBody;


//////////////////////////// ANDROID LIFECYCLE


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Bundle arguments = getArguments();

        messageId       = arguments.getString(MESSAGE_ID_BUNDLE_KEY);
        messageSeverity = MessageSeverity.getMessageSeverityForSeverity(arguments.getString(MESSAGE_SEVERITY_BUNDLE_KEY));
        messageBody     = arguments.getString(MESSAGE_BODY_BUNDLE_KEY);

        View layout = inflater.inflate(R.layout.message_detail_fragment, container, false);
        layout.findViewById(R.id.message_detail_type).setBackgroundColor(ContextCompat.getColor(getContext(), messageSeverity.getColor()));
        ((TextView) layout.findViewById(R.id.message_detail_type_text)).setText(messageSeverity.getDescription());

        if (Utility.isNotStringEmpty(messageBody)) {
            ((TextView) layout.findViewById(R.id.message_detail_text)).setText(messageBody);
        }

        return layout;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        for (MessageDetailMenuDefinition messageDetailMenuDefinition: MessageDetailMenuDefinition.values()) {
            if (messageDetailMenuDefinition.isShowAction(this)) {
                MenuItem menuItem = menu.add(Menu.NONE, messageDetailMenuDefinition.id, messageDetailMenuDefinition.ordinal(), messageDetailMenuDefinition.menuTitleId);
                menuItem.setIcon(messageDetailMenuDefinition.iconId);
                menuItem.setShowAsAction(messageDetailMenuDefinition.showAsAction);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        MessageDetailMenuDefinition messageDetailMenuDefinition = MessageDetailMenuDefinition.getMessageDetailMenuDefinitionForId(item.getItemId());
        if (messageDetailMenuDefinition != null) {
            messageDetailMenuDefinition.itemAction(this);
            return true;
        }
        return false;
    }

    public enum MessageDetailMenuDefinition {

        DISMISS         (1, R.string.message_detail_action_dismiss, R.drawable.action_delete, MenuItem.SHOW_AS_ACTION_ALWAYS) {
            @Override
            public void itemAction(MessageDetailFragment fragment) {
                MessageAccess.removeMessage(((AbstractTurrisActivity) fragment.getActivity()).getTurrisApplication().getEventBus(), fragment.messageId);
                fragment.getActivity().finish();
            }
            @Override
            protected boolean isShowAction(MessageDetailFragment messageDetailFragment) {
                return messageDetailFragment.messageSeverity.isCanBeClosed();
            }
        },
        RESTART          (2, R.string.restart, R.drawable.action_reboot, MenuItem.SHOW_AS_ACTION_ALWAYS) {
            @Override
            public void itemAction(MessageDetailFragment fragment) {
                NetconfControl.reboot((AbstractTurrisActivity) fragment.getActivity(), () -> MessageDetailMenuDefinition.DISMISS.itemAction(fragment));
            }
            @Override
            protected boolean isShowAction(MessageDetailFragment messageDetailFragment) {
                return messageDetailFragment.messageSeverity.equals(MessageSeverity.RESTART);
            }
        };

        private int         id;
        private final int   menuTitleId;
        private final int   iconId;
        private final int   showAsAction;

        MessageDetailMenuDefinition(int id, int menuTitleId, int iconId, int showAsAction) {
            this.id             = id;
            this.menuTitleId    = menuTitleId;
            this.iconId         = iconId;
            this.showAsAction   = showAsAction;
        }

        public abstract void itemAction(MessageDetailFragment fragment);

        protected boolean isShowAction(MessageDetailFragment messageDetailFragment) {
            return true;
        }

        public static MessageDetailMenuDefinition getMessageDetailMenuDefinitionForId(long id) {
            for (MessageDetailMenuDefinition menuDefinition: values()) {
                if (menuDefinition.id == id) {
                    return menuDefinition;
                }
            }
            TurrisLog.error(MessageDetailMenuDefinition.class, "Cannot get MessageDetailMenuDefinition for id: " + id);
            return null;
        }
    }


//////////////////////////// INSTANCE ACCESS

    public static MessageDetailFragment newInstance(String messageId, String messageSeverity, String messageBody) {
        MessageDetailFragment messageDetailFragment = new MessageDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putString(MESSAGE_ID_BUNDLE_KEY, messageId);
        bundle.putString(MESSAGE_SEVERITY_BUNDLE_KEY, messageSeverity);
        bundle.putString(MESSAGE_BODY_BUNDLE_KEY, messageBody);
        messageDetailFragment.setArguments(bundle);
        return messageDetailFragment;
    }
}
