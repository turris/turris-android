/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.main.interfaces;

import android.os.Bundle;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import cz.nic.turris.R;
import cz.nic.turris.TurrisLog;
import cz.nic.turris.utility.Utility;
import cz.nic.turris.utility.activity.AbstractTurrisActivity;
import cz.nic.turris.connectors.container.NetworkSnapshotFactory;
import cz.nic.turris.utility.fragment.AbstractTimeChartFragment;

public class InterfaceChartFragment extends AbstractTimeChartFragment {

    private static final int        CHART_TX_COLOR              = R.color.chart_tx;
    private static final boolean    CHART_TX_FILLED             = false;
    private static final int        CHART_RX_COLOR              = R.color.chart_rx;
    private static final boolean    CHART_RX_FILLED             = false;


    protected static final String   INTERFACE_NAME_BUNDLE_KEY   = "interface_name";


    private String interfaceName = null;


//////////////////////////// ANDROID LIFECYCLE

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            interfaceName = savedInstanceState.getString(INTERFACE_NAME_BUNDLE_KEY);
        } else {
            Bundle bundle = getArguments();
            if (bundle != null) {
                interfaceName = bundle.getString(INTERFACE_NAME_BUNDLE_KEY);
            } else {
                TurrisLog.error(getClass(), "No interface name specified in bundle!");
            }
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        updateChart(((AbstractTurrisActivity) getActivity()).getTurrisApplication().getEventBus().getStickyEvent(NetworkSnapshotFactory.NetworkSnapshotsContainer.class), false);
    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putString(INTERFACE_NAME_BUNDLE_KEY, interfaceName);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AbstractTurrisActivity) getActivity()).getTurrisApplication().getEventBus().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        ((AbstractTurrisActivity) getActivity()).getTurrisApplication().getEventBus().unregister(this);
    }


//////////////////////////// DATA HANDLER

    @Subscribe
    public void handleMemorySnapshotData(NetworkSnapshotFactory.NetworkSnapshotsContainer networkSnapshotsContainer) {
        updateChart(networkSnapshotsContainer, true);
    }

    private void updateChart(NetworkSnapshotFactory.NetworkSnapshotsContainer networkSnapshotsContainer, boolean checkVisibility) {
        if (isAdded()) {
            if(interfaceName != null && networkSnapshotsContainer != null) {
                NetworkSnapshotFactory.NetworkSnapshot networkSnapshot = networkSnapshotsContainer.get(interfaceName);
                if (networkSnapshot != null) {
                    updateChart(new ArrayList<ChartData>() {{
                        add(new ChartData(getString(R.string.interface_chart_rx), networkSnapshot.getRx(), CHART_RX_COLOR, CHART_RX_FILLED));
                        add(new ChartData(getString(R.string.interface_chart_tx), networkSnapshot.getTx(), CHART_TX_COLOR, CHART_TX_FILLED));
                    }}, checkVisibility);
                }
            }
        }
    }


//////////////////////////// ABSTRACT CHART FRAGMENT

    @Override
    protected int prepareTitle() {
        return R.string.chart_traffic_title;
    }

    @Override
    protected String getYAxisFormattedValues(float value) {
        return getResources().getString(R.string.chart_bitrate_y_unit, Utility.getOneDecimalFormattedNumber(value));
    }


//////////////////////////// INSTANCE ACCESS

	public static InterfaceChartFragment newInstance(String interfaceName) {
		InterfaceChartFragment fragment = new InterfaceChartFragment();
		Bundle bundle = new Bundle();
		bundle.putString(INTERFACE_NAME_BUNDLE_KEY, interfaceName);
		fragment.setArguments(bundle);
		return fragment;
	}
}
