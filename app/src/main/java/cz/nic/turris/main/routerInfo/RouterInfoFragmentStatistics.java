/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.main.routerInfo;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.greenrobot.eventbus.Subscribe;
import org.joda.time.DurationFieldType;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.Seconds;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

import cz.nic.turris.R;
import cz.nic.turris.utility.Utility;
import cz.nic.turris.utility.activity.AbstractTurrisActivity;
import cz.nic.turris.connectors.container.LoadAverageFactoryData;
import cz.nic.turris.connectors.container.MemoryInfoFactoryData;
import cz.nic.turris.connectors.container.RouterExtendedInfoFactoryData;

public class RouterInfoFragmentStatistics extends Fragment {

    private static final PeriodType PERIOD_TYPES = PeriodType.forFields(new DurationFieldType[]{DurationFieldType.days(),
                                                                                                DurationFieldType.hours(),
                                                                                                DurationFieldType.minutes(),
                                                                                                DurationFieldType.seconds()});

    private TextView        uptimeTextView;
    private TextView        averageLoad1TextView;
    private TextView        averageLoad5TextView;
    private TextView        averageLoad15TextView;
    private TextView        averageLoad1LabelTextView;
    private TextView        averageLoad5LabelTextView;
    private TextView        averageLoad15LabelTextView;
    private TextView        totalMemoryTextView;
    private TextView        freeMemoryTextView;
    private TextView        cachedMemoryTextView;
    private TextView        bufferedMemoryTextView;
    private PeriodFormatter periodFormatter;


//////////////////////////// ANDROID LIFECYCLE

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.router_info_fragment_statistics, container, false);

        uptimeTextView              = (TextView) layout.findViewById(R.id.router_info_uptime);
        averageLoad1TextView        = (TextView) layout.findViewById(R.id.router_info_load_1);
        averageLoad5TextView        = (TextView) layout.findViewById(R.id.router_info_load_5);
        averageLoad15TextView       = (TextView) layout.findViewById(R.id.router_info_load_15);
        averageLoad1LabelTextView   = (TextView) layout.findViewById(R.id.router_info_load_1_label);
        averageLoad5LabelTextView   = (TextView) layout.findViewById(R.id.router_info_load_5_label);
        averageLoad15LabelTextView  = (TextView) layout.findViewById(R.id.router_info_load_15_label);
        totalMemoryTextView         = (TextView) layout.findViewById(R.id.router_info_total_memory);
        freeMemoryTextView          = (TextView) layout.findViewById(R.id.router_info_free_memory);
        cachedMemoryTextView        = (TextView) layout.findViewById(R.id.router_info_cached_memory);
        bufferedMemoryTextView      = (TextView) layout.findViewById(R.id.router_info_buffered_memory);

        return layout;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        handleRouterExtendedInfoData(((AbstractTurrisActivity) getActivity()).getTurrisApplication().getEventBus().getStickyEvent(RouterExtendedInfoFactoryData.RouterExtendedInfo.class));
        handleRouterMemoryInfoData(((AbstractTurrisActivity) getActivity()).getTurrisApplication().getEventBus().getStickyEvent(MemoryInfoFactoryData.MemoryInfo.class));
    }

    @Override
    public void onResume() {
        super.onResume();
        preparePeriodFormatter();
        prepareAverageLoadLabels();
        ((AbstractTurrisActivity) getActivity()).getTurrisApplication().getEventBus().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        ((AbstractTurrisActivity) getActivity()).getTurrisApplication().getEventBus().unregister(this);
    }


//////////////////////////// DATA HANDLERS

    @Subscribe
    public void handleRouterExtendedInfoData(RouterExtendedInfoFactoryData.RouterExtendedInfo routerExtendedInfo) {
        if (routerExtendedInfo != null) {
            updateExtendedInfoInUIThread(routerExtendedInfo);
        }
    }

    @Subscribe
    public void handleRouterMemoryInfoData(MemoryInfoFactoryData.MemoryInfo memoryInfo) {
        if (memoryInfo != null) {
            updateMemoryInfoInUIThread(memoryInfo);
        }
    }


//////////////////////////// CONTENT REFRESH

    private void updateExtendedInfoInUIThread(@NonNull RouterExtendedInfoFactoryData.RouterExtendedInfo routerExtendedInfo) {
        getActivity().runOnUiThread(() -> updateExtendedInfo(routerExtendedInfo));
    }

    private void updateMemoryInfoInUIThread(@NonNull MemoryInfoFactoryData.MemoryInfo memoryInfo) {
        getActivity().runOnUiThread(() -> updateMemoryInfo(memoryInfo));
    }

	private void updateExtendedInfo(@NonNull RouterExtendedInfoFactoryData.RouterExtendedInfo routerExtendedInfo) {
        if (isAdded()) {
            if (periodFormatter != null) {
                uptimeTextView.setText(periodFormatter.print(new Period(Seconds.seconds(Double.valueOf(routerExtendedInfo.getUptime()).intValue())).normalizedStandard(PERIOD_TYPES)));
            }
            LoadAverageFactoryData.LoadAverage loadAverage = routerExtendedInfo.getLoadAvg();
            if (loadAverage != null) {
                averageLoad1TextView.setText(loadAverage.getLoadAverage1());
                averageLoad5TextView.setText(loadAverage.getLoadAverage5());
                averageLoad15TextView.setText(loadAverage.getLoadAverage15());
            }
        }
    }

	private void updateMemoryInfo(@NonNull MemoryInfoFactoryData.MemoryInfo memoryInfo) {
        if (isAdded()) {
            freeMemoryTextView.setText(getString(R.string.router_info_memory, Utility.getOneDecimalFormattedNumber(memoryInfo.getFree())));
            cachedMemoryTextView.setText(getString(R.string.router_info_memory, Utility.getOneDecimalFormattedNumber(memoryInfo.getCached())));
            bufferedMemoryTextView.setText(getString(R.string.router_info_memory, Utility.getOneDecimalFormattedNumber(memoryInfo.getBuffered())));
            totalMemoryTextView.setText(getString(R.string.router_info_memory, Utility.getOneDecimalFormattedNumber(memoryInfo.getTotal())));
        }
	}


//////////////////////////// UTILITY

    private void preparePeriodFormatter() {
        periodFormatter = new PeriodFormatterBuilder()
                .appendDays()
                .appendSuffix(getString(R.string.day_abb))
                .appendSeparator(Utility.SPACE_STRING)
                .appendHours()
                .appendSuffix(getString(R.string.hour_abb))
                .appendSeparator(Utility.SPACE_STRING)
                .appendMinutes()
                .appendSuffix(getString(R.string.minute_abb))
                .appendSeparator(Utility.SPACE_STRING)
                .appendSeconds()
                .appendSuffix(getString(R.string.second_abb))
                .toFormatter();
    }

    @SuppressLint("SetTextI18n")
    private void prepareAverageLoadLabels() {
        if (isAdded()) {
            averageLoad1LabelTextView.setText(getString(R.string.average_load_1) + getString(R.string.minute_abb));
            averageLoad5LabelTextView.setText(getString(R.string.average_load_5) + getString(R.string.minute_abb));
            averageLoad15LabelTextView.setText(getString(R.string.average_load_15) + getString(R.string.minute_abb));
        }
    }
}
