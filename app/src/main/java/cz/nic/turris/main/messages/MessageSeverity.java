/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.main.messages;

import cz.nic.turris.R;
import cz.nic.turris.connectors.container.MessageFactoryData;

public enum MessageSeverity {
    RESTART (MessageFactoryData.Message.MESSAGE_SEVERITY_RESTART,   R.color.message_item_restart_background,    R.string.messages_item_type_restart,    R.string.message_item_control_restart,      false),
    ERROR   (MessageFactoryData.Message.MESSAGE_SEVERITY_ERROR,     R.color.message_item_error_background,      R.string.messages_item_type_error,      R.string.message_item_control_dismiss,      true),
    UPDATE  (MessageFactoryData.Message.MESSAGE_SEVERITY_UPDATE,    R.color.message_item_update_background,     R.string.messages_item_type_update,     R.string.message_item_control_dismiss,      true),
    NEWS    (MessageFactoryData.Message.MESSAGE_SEVERITY_NEWS,      R.color.message_item_news_background,       R.string.messages_item_type_news,       R.string.message_item_control_dismiss,      true);

    private static final MessageSeverity FALLBACK_SEVERITY = NEWS;

    private final String    severity;
    private final int       color;
    private final int       description;
    private final int       controlMessage;
    private final boolean   canBeClosed;

    MessageSeverity(String severity, int color, int description, int controlMessage, boolean canBeClosed) {
        this.severity           = severity;
        this.color              = color;
        this.description        = description;
        this.controlMessage     = controlMessage;
        this.canBeClosed        = canBeClosed;
    }

    public String getSeverity() {
        return severity;
    }

    public int getColor() {
        return color;
    }

    public int getDescription() {
        return description;
    }

    public int getControlMessage() {
        return controlMessage;
    }

    public boolean isCanBeClosed() {
        return canBeClosed;
    }

    public static MessageSeverity getMessageSeverityForSeverity(String severity) {
        for (MessageSeverity messageSeverity: values()) {
            if (messageSeverity.severity.equals(severity)) {
                return messageSeverity;
            }
        }
        return FALLBACK_SEVERITY;
    }
}
