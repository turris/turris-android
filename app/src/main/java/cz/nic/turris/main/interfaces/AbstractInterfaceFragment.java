/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.main.interfaces;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import org.greenrobot.eventbus.Subscribe;

import cz.nic.turris.TurrisLog;
import cz.nic.turris.utility.activity.AbstractTurrisActivity;
import cz.nic.turris.connectors.container.InterfaceListItemFactoryData;

public abstract class AbstractInterfaceFragment extends Fragment {

	protected static final String INTERFACE_NAME_BUNDLE_KEY = "interface_name";

    private String interfaceName;


//////////////////////////// ANDROID LIFECYCLE

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            interfaceName = savedInstanceState.getString(INTERFACE_NAME_BUNDLE_KEY);
        } else {
            Bundle bundle = getArguments();
            if (bundle != null) {
                interfaceName = bundle.getString(INTERFACE_NAME_BUNDLE_KEY);
            } else {
                TurrisLog.error(getClass(), "No interface name specified in bundle!");
            }
        }
	}

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        updateUIForInterfaceName(((AbstractTurrisActivity) getActivity()).getTurrisApplication().getEventBus().getStickyEvent(InterfaceListItemFactoryData.InterfaceListItem[].class), getInterfaceName(), false);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(INTERFACE_NAME_BUNDLE_KEY, interfaceName);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AbstractTurrisActivity) getActivity()).getTurrisApplication().getEventBus().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        ((AbstractTurrisActivity) getActivity()).getTurrisApplication().getEventBus().unregister(this);
    }


//////////////////////////// DATA HANDLERS

    @Subscribe
    public void handleRouterMoreInfoChange(InterfaceListItemFactoryData.InterfaceListItem[] interfaceListItems) {
        updateUIForInterfaceName(interfaceListItems, getInterfaceName(), true);
    }


//////////////////////////// UI UPDATE

    private void updateUIForInterfaceName(InterfaceListItemFactoryData.InterfaceListItem[] interfaceListItems, String interfaceName, boolean checkVisibility) {
        if (interfaceListItems != null && interfaceName != null) {
            for (InterfaceListItemFactoryData.InterfaceListItem interfaceListItem : interfaceListItems) {
                if(interfaceName.equals(interfaceListItem.getName())) {
                    if(!checkVisibility || isVisible()) {
                        updateViewInUIThread(interfaceListItem);
                    }
                    return;
                }
            }
        }
    }

    private void updateViewInUIThread(InterfaceListItemFactoryData.InterfaceListItem interfaceListItem) {
        getActivity().runOnUiThread(() -> updateView(interfaceListItem));
    }


//////////////////////////// ABSTRACT INTERFACE

    protected abstract void updateView(InterfaceListItemFactoryData.InterfaceListItem interfaceListItem);

    protected String getInterfaceName() {
        return interfaceName;
    }


//////////////////////////// INSTANCE ACCESS

    public static <T extends AbstractInterfaceFragment> T newInstance(Class<T> clazz, String interfaceName) {
        T fragment = null;
        try {
            fragment = clazz.newInstance();
            Bundle bundle = new Bundle();
            bundle.putString(INTERFACE_NAME_BUNDLE_KEY, interfaceName);
            fragment.setArguments(bundle);
        } catch (java.lang.InstantiationException e) {
            logNewInstanceError(clazz, e);
        } catch (IllegalAccessException e) {
            logNewInstanceError(clazz, e);
        }
        return fragment;
    }

    private static void logNewInstanceError(Class clazz, Exception e) {
        TurrisLog.error(clazz, "Cannot create new instance of class: " + clazz, e);
    }

}
