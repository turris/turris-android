/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.main.menu;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.LongSparseArray;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;

import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import cz.nic.turris.R;
import cz.nic.turris.TurrisSettings;
import cz.nic.turris.RouterType;
import cz.nic.turris.utility.Utility;
import cz.nic.turris.main.interfaces.InterfaceInfoFragment;
import cz.nic.turris.main.MainActivity;
import cz.nic.turris.utility.activity.AbstractTurrisActivity;
import cz.nic.turris.connectors.DataRefreshService;
import cz.nic.turris.database.RouterProfile;
import cz.nic.turris.main.interfaces.InterfaceInfoFragmentStatistics;
import cz.nic.turris.main.routerInfo.RouterInfoFragmentGeneral;
import cz.nic.turris.connectors.container.InterfaceListItemFactoryData;

public class MenuDrawerFragment extends Fragment {

    private static final String SELECTED_MENU_POSITION_BUNDLE_KEY               = "selectedMenuPosition";
    private static final MainActivity.TurrisMenuDefinition INITIAL_MENU_ITEM    = TurrisSettings.INITIAL_MENU_ITEM;
    private static final int MENU_DRAWER_NO_SELECTION_ID                        = -1;

    public  static boolean                                                  isDrawerOpened = false;

    private Drawer                                                          menuDrawer;
    private AccountHeader                                                   menuAccountHeader;
    private LongSparseArray<InterfaceListItemFactoryData.InterfaceListItem> interfaces;
    private MainActivity                                                    mainActivity;
    private long                                                            selectedMenuId;
    private boolean                                                         connected;
    private ViewGroup                                                       container;


//////////////////////////// MATERIAL DRAWER

    private void prepareDrawer(Bundle savedInstanceState) {
        menuAccountHeader = new AccountHeaderBuilder() {

            @Override
            protected void toggleSelectionList(Context context) {
                super.toggleSelectionList(context);
                loadLastEvents();
            }
        }
                .withActivity(mainActivity)
                .addProfiles(prepareDrawerProfiles())
                .withSelectionListEnabledForSingleProfile(true)
                .withSavedInstance(savedInstanceState)
                .withProfileImagesVisible(true)
                .withOnlyMainProfileImageVisible(true)
                .withCompactStyle(true)
                .withOnAccountHeaderListener((view, profile, current) -> {
                    ((RouterProfile) ((ProfileDrawerItem) profile).getTag()).setAsActive(mainActivity.getTurrisApplication(), true);
                    return false;
                })
                .build();

        menuAccountHeader.getHeaderBackgroundView().setBackgroundResource(R.color.basic_blue_dark);

        menuDrawer = new DrawerBuilder()
                .withActivity(mainActivity)
                .withToolbar((Toolbar) mainActivity.findViewById(R.id.toolbar))
                .withAccountHeader(menuAccountHeader)
                .withSavedInstance(savedInstanceState)
                .withDrawerItems(prepareDrawerTurrisMenu())
                .withOnDrawerItemClickListener((view, position, drawerItem) -> false)
                .withOnDrawerListener(new Drawer.OnDrawerListener() {
                    @Override
                    public void onDrawerOpened(View drawerView) {
                        MenuDrawerFragment.isDrawerOpened = true;
                    }

                    @Override
                    public void onDrawerClosed(View drawerView) {
                        MenuDrawerFragment.isDrawerOpened = false;
                    }

                    @Override
                    public void onDrawerSlide(View drawerView, float slideOffset) {}
                })
                .build();
        enableDrawerTurrisMenu(connected);
        selectMenuItem();
        selectProfileItem();
    }


//////////////////////////// PROFILE ITEMS

    private IProfile[] prepareDrawerProfiles() {
        List<RouterProfile> allRouterProfiles = RouterProfile.getAllRegisteredRouters();
        IProfile[] profileDrawerItems = new ProfileDrawerItem[allRouterProfiles.size()];
        //noinspection Convert2streamapi
        for (int i = 0; i < allRouterProfiles.size(); i++) {
            final RouterProfile routerProfile = allRouterProfiles.get(i);
            ProfileDrawerItem profileDrawerItem = new ProfileDrawerItem() {
                @Override
                public int getLayoutRes() {
                    return R.layout.menu_drawer_turris_item;
                }
            };
            profileDrawerItem
                    .withIdentifier(routerProfile.getId())
                    .withName(routerProfile.getName())
                    .withEmail(routerProfile.getHostName())
                    .withNameShown(true)
                    .withTag(routerProfile)
                    .withIcon(RouterInfoFragmentGeneral.RouterTypeResources.getRouterTypeResourceForRouterType(RouterType.getRouterTypeForModelId(routerProfile.getBoardName())).getModelIconResource())
                    .withPostOnBindViewListener((drawerItem, view) -> {
                        ImageButton editButton = (ImageButton) view.findViewById(R.id.menu_drawer_profile_edit);
                        ImageButton deleteButton = (ImageButton) view.findViewById(R.id.menu_drawer_profile_delete);
                        editButton.setOnClickListener(v -> showEditRouterProfileDialog(routerProfile));
                        deleteButton.setOnClickListener(v -> showDeleteRouterProfileDialog(routerProfile));
                        boolean selected = drawerItem.isSelected();
                        setColorFilterToProfileItemIcon(selected, editButton);
                        setColorFilterToProfileItemIcon(selected, deleteButton);
                    });
            profileDrawerItems[i] = profileDrawerItem;
        }
        return profileDrawerItems;
    }

    private void setColorFilterToProfileItemIcon(boolean isSelected, ImageButton imageButton) {
        imageButton.setColorFilter(isSelected ? ContextCompat.getColor(getContext(), R.color.menu_drawer_profile_edit_selected) : ContextCompat.getColor(getContext(), R.color.menu_drawer_profile_edit_unselected));
    }

    private void showEditRouterProfileDialog(RouterProfile routerProfile) {
        View dialogView = getActivity().getLayoutInflater().inflate(R.layout.edit_profile_dialog_fragment, container, false);
        EditText nameEditText = (EditText) dialogView.findViewById(R.id.edit_profile_name);
        nameEditText.setText(routerProfile.getName());

        /* build dialog */
        AlertDialog.Builder profileRenameDialogBuilder =  new  AlertDialog.Builder(getActivity())
                .setTitle(R.string.edit_profile_dialog_title)
                .setCancelable(false)
                .setPositiveButton(R.string.confirm_button, (dialog, whichButton) -> {
                    String nameValue = nameEditText.getText().toString().trim();
                    if (!routerProfile.getName().equals(nameValue)) {
                        routerProfile.rename(mainActivity.getTurrisApplication(), nameValue);
                    }
                })
                .setNegativeButton(R.string.cancel_button, (dialog, whichButton) -> dialog.dismiss());
        profileRenameDialogBuilder.setView(dialogView);
        AlertDialog profileRenameDialog = profileRenameDialogBuilder.create();
        profileRenameDialog.setOnKeyListener((arg0, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                profileRenameDialog.dismiss();
            }
            return true;
        });

        /* validate profile name */
        nameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void afterTextChanged(Editable s) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String name = s.toString().trim();
                if (Utility.isStringEmpty(name)) {
                    nameTextError(R.string.edit_profile_dialog_error_empty);
                } else if (!name.equals(routerProfile.getName()) && RouterProfile.existsRouterWithName(name)) {
                    nameTextError(R.string.edit_profile_dialog_error_exists);
                } else {
                    profileRenameDialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(true);
                }
            }

            private void nameTextError(int errorTextResource) {
                nameEditText.setError(getString(errorTextResource));
                profileRenameDialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(false);
            }
        });

        /* request focus */
        Window window = profileRenameDialog.getWindow();
        if (window != null) {
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        }

        profileRenameDialog.show();
    }

    private void showDeleteRouterProfileDialog(RouterProfile routerProfile) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(getString(R.string.delete_profile_confirm_message, routerProfile.getName()))
                .setPositiveButton(R.string.delete_button, (dialog, id) -> routerProfile.delete(mainActivity.getTurrisApplication()))
                .setNegativeButton(R.string.cancel_button, (dialog, id) -> {});
        builder.show();
    }

    private void refreshRouterProfiles() {
        menuAccountHeader.clear();
        menuAccountHeader.addProfiles(prepareDrawerProfiles());
        selectProfileItem();
    }


//////////////////////////// MENU ITEMS

    private List<IDrawerItem> prepareDrawerTurrisMenu() {
        List<IDrawerItem> result = new ArrayList<>();
        for (MainActivity.TurrisMenuDefinition menuDefinition: MainActivity.TurrisMenuDefinition.values()) {
            PrimaryDrawerItem staticMenuItem = new PrimaryDrawerItem()
                    .withIdentifier(menuDefinition.getId())
                    .withName(getString(menuDefinition.getMenuTitleId()))
                    .withSelectable(menuDefinition.isSelectable())
                    .withOnDrawerItemClickListener((view, position, drawerItem1) -> {
                        menuDefinition.switchToItem(mainActivity);
                        return true;
                    });
            result.add(staticMenuItem);
        }
        result.add(new DividerDrawerItem());
        return result;
    }

    private void enableDrawerTurrisMenu(boolean enabled) {
        for (MainActivity.TurrisMenuDefinition menuDefinition: MainActivity.TurrisMenuDefinition.values()) {
            IDrawerItem drawerItem = menuDrawer.getDrawerItem(menuDefinition.getId());
            if (menuDefinition.isDisabling() && drawerItem != null) {
                drawerItem.withEnabled(enabled);
                menuDrawer.updateItem(drawerItem);
            }
        }
        selectMenuItem();
    }

    private void removeMenuInterfaces(LongSparseArray<InterfaceListItemFactoryData.InterfaceListItem> interfacesToRemove) {
        if (interfacesToRemove != null) {
            for(int i = 0; i < interfacesToRemove.size(); i++) {
                menuDrawer.removeItem(interfacesToRemove.keyAt(i));
            }
        }
    }

    private void refreshDrawerInterfaces(InterfaceListItemFactoryData.InterfaceListItem[] interfaceListItems) {
        if (!menuAccountHeader.isSelectionListShown()) {
            LongSparseArray<InterfaceListItemFactoryData.InterfaceListItem> newInterfaces = refreshInterfacesMap(interfaceListItems);
            if (!Utility.equalsLongSparseArrays(newInterfaces, interfaces)) {
                removeMenuInterfaces(interfaces);
                if (addMenuInterfaces(newInterfaces)) {
                    interfaces = newInterfaces;
                } else {
                    interfaces = null;
                }
            }
        }
    }

    private boolean addMenuInterfaces(LongSparseArray<InterfaceListItemFactoryData.InterfaceListItem> interfacesToShow) {
        if (!connected || interfacesToShow == null) {
            return false;
        }
        for(int i = 0; i < interfacesToShow.size(); i++) {
            long interfaceMenuId = interfacesToShow.keyAt(i);
            InterfaceListItemFactoryData.InterfaceListItem interfaceListItem = interfacesToShow.get(interfaceMenuId);
            if (!TurrisSettings.SHOW_ONLY_UP_INTERFACES || interfaceListItem.isUp()) {
                PrimaryDrawerItem drawerItem = new PrimaryDrawerItem()
                        .withIdentifier(interfaceMenuId)
                        .withName(interfaceListItem.getName())
                        .withSelectable(true)
                        .withOnDrawerItemClickListener((view, position, item) -> {
                            mainActivity.getTurrisApplication().getEventBus().postSticky(new MainActivity.TurrisMenuActionEvent(interfaceMenuId, InterfaceInfoFragment.newInstance(interfaceListItem.getName(), interfaceListItem.getType())));
                            return true;
                        });
                Integer interfaceIconId = InterfaceInfoFragmentStatistics.MenuInterfaceType.getMenuInterfaceTypeForInterfaceType(interfaceListItem.getType()).getInterfaceIconId();
                if (interfaceIconId != null) {
                    drawerItem.withIcon(interfaceIconId);
                }
                menuDrawer.addItem(drawerItem);
            }
        }
        selectMenuItem();
        return true;
    }

    private void selectMenuItem() {
        menuDrawer.setSelection(connected ? selectedMenuId : MENU_DRAWER_NO_SELECTION_ID, false);
    }

    private void selectProfileItem() {
        RouterProfile activeRouterProfile = RouterProfile.getActiveRouter(getContext());
        if (activeRouterProfile != null) {
            menuAccountHeader.setActiveProfile(activeRouterProfile.getId());
        }
    }

    private LongSparseArray<InterfaceListItemFactoryData.InterfaceListItem> refreshInterfacesMap(InterfaceListItemFactoryData.InterfaceListItem[] interfaceListItems) {
        LongSparseArray<InterfaceListItemFactoryData.InterfaceListItem> interfaces = new LongSparseArray<>();
        if (interfaceListItems != null) {
            long id = MainActivity.TurrisMenuDefinition.getMaxId();
            for (InterfaceListItemFactoryData.InterfaceListItem interfaceListItem: interfaceListItems) {
                interfaces.put(++id, interfaceListItem);
            }
        }
        return interfaces;
    }


//////////////////////////// EVENTS

    public static class MenuDrawerActionEvent {

        private final boolean drawerOpened;

        public MenuDrawerActionEvent(boolean drawerOpened) {
            this.drawerOpened = drawerOpened;
        }

        private boolean isDrawerOpened() {
            return drawerOpened;
        }
    }

    @Subscribe
    public void handleMenuDrawerActionEvent(MenuDrawerActionEvent menuDrawerActionEvent) {
        if (menuDrawerActionEvent != null) {
            if (!menuDrawerActionEvent.isDrawerOpened() && menuDrawer.isDrawerOpen()) {
                menuDrawer.closeDrawer();
            } else if (menuDrawerActionEvent.isDrawerOpened() && !menuDrawer.isDrawerOpen()) {
                menuDrawer.openDrawer();
            }
        }
    }

    @Subscribe
    public void handleTurrisMenuActionEvent(MainActivity.TurrisMenuActionEvent menuDefinition) {
        if (menuDefinition != null) {
            mainActivity.runOnUiThread(() -> {
                if (!menuAccountHeader.isSelectionListShown()) {
                    selectedMenuId = menuDefinition.getId();
                    selectMenuItem();
                }
            });
        }
    }

    @Subscribe
    public void handleRouterProfileActiveEvent(RouterProfile.RouterProfileActiveEvent routerProfileActiveEvent) {
        mainActivity.runOnUiThread(() -> {
            if (!menuAccountHeader.isSelectionListShown()) {
                RouterProfile activeRouter = routerProfileActiveEvent.getActiveRouterProfile();
                if (activeRouter != null) {
                    menuAccountHeader.setActiveProfile(activeRouter.getId());
                }
            }
        });
    }

    @Subscribe
    public void handleNetconfServiceConnectionEvent(DataRefreshService.NetconfServiceConnectionEvent netconfServiceConnectionEvent) {
        mainActivity.runOnUiThread(() -> refreshDrawerState(netconfServiceConnectionEvent));
    }

    @Subscribe
    public void handleInterfaceListItemsData(InterfaceListItemFactoryData.InterfaceListItem[] interfaceListItems) {
        mainActivity.runOnUiThread(() -> refreshDrawerInterfaces(interfaceListItems));
    }

    @Subscribe
    public void handleRouterProfileRenamedEvent(RouterProfile.RouterProfileRenamedEvent routerProfileRenamedEvent) {
        refreshRouterProfiles();
    }

    @Subscribe
    public void handleRouterProfileDeletedEvent(RouterProfile.RouterProfileDeletedEvent routerProfileDeletedEvent) {
        refreshRouterProfiles();
    }

    private void loadLastEvents() {
        handleNetconfServiceConnectionEvent(mainActivity.getTurrisApplication().getEventBus().getStickyEvent(DataRefreshService.NetconfServiceConnectionEvent.class));
        handleInterfaceListItemsData(mainActivity.getTurrisApplication().getEventBus().getStickyEvent(InterfaceListItemFactoryData.InterfaceListItem[].class));
    }


//////////////////////////// DRAWER STATE

    private void refreshDrawerState() {
        refreshDrawerState(((AbstractTurrisActivity) getActivity()).getTurrisApplication().getEventBus().getStickyEvent(DataRefreshService.NetconfServiceConnectionEvent.class));
    }

    private void refreshDrawerState(DataRefreshService.NetconfServiceConnectionEvent netconfServiceConnectionEvent) {
        refreshRouterProfiles();
        if (!menuAccountHeader.isSelectionListShown()) {
            boolean connected = netconfServiceConnectionEvent != null ? netconfServiceConnectionEvent.isConnected() : this.connected;
            if (connected != this.connected) {
                this.connected = connected;
                if (!connected) {
                    refreshDrawerInterfaces(null);
                }
                enableDrawerTurrisMenu(connected);
            }
        }
    }

//////////////////////////// ANDROID LIFECYCLE

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.container = container;
        return inflater.inflate(R.layout.menudrawer_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainActivity = ((MainActivity) getActivity());
        prepareDrawer(savedInstanceState);
        selectedMenuId = savedInstanceState != null ? savedInstanceState.getLong(SELECTED_MENU_POSITION_BUNDLE_KEY) : INITIAL_MENU_ITEM.getId();
        loadLastEvents();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(SELECTED_MENU_POSITION_BUNDLE_KEY, selectedMenuId);
        menuDrawer.saveInstanceState(outState);
        menuAccountHeader.saveInstanceState(outState);
    }

    @Override
    public void onStart() {
        super.onStart();
        refreshDrawerState();
        handleTurrisMenuActionEvent(mainActivity.getTurrisApplication().getEventBus().getStickyEvent(MainActivity.TurrisMenuActionEvent.class));
        mainActivity.getTurrisApplication().getEventBus().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        mainActivity.getTurrisApplication().getEventBus().unregister(this);
    }
}
