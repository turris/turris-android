/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.main.interfaces;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import cz.nic.turris.R;
import cz.nic.turris.connectors.container.InterfaceListItemFactoryData;

public class InterfaceInfoFragmentUnknown extends AbstractInterfaceFragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.interface_info_fragment_unknown, container, false);
		((TextView) view.findViewById(R.id.interface_info_type)).setText(getText(R.string.unkown));
		return view;
	}


//////////////////////////// CONTENT REFRESH

	@Override
	protected void updateView(InterfaceListItemFactoryData.InterfaceListItem interfaceListItem) {}
}
