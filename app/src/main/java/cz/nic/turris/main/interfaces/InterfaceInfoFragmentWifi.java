/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.main.interfaces;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import cz.nic.turris.R;
import cz.nic.turris.TurrisLog;
import cz.nic.turris.connectors.container.InterfaceListItemFactoryData;
import cz.nic.turris.connectors.container.WifiClientFactoryData;
import cz.nic.turris.connectors.container.WifiInfoFactoryData;

public class InterfaceInfoFragmentWifi extends AbstractInterfaceFragment {

    public enum WifiRole {

        ROLE_AP     ("AP",      R.string.wifi_role_ap),
        ROLE_STATION("STA",     R.string.wifi_role_client),
        ROLE_ADHOC  ("ADHOC",   R.string.wifi_role_adhoc),
        ROLE_WDS    ("WDS",     R.string.wifi_role_wds),
        ROLE_MONITOR("MONITOR", R.string.wifi_role_monitor),
        ROLE_MESH   ("MESH",    R.string.wifi_role_mmesh),
        ROLE_UNKNOWN("UNKNOWN", R.string.wifi_role_unknown);

        private static final WifiRole FALLBACK_ROLE_NAME = ROLE_UNKNOWN;

        private String  roleName;
        private Integer descriptionId;

        WifiRole(String roleName, Integer descriptionId) {
            this.roleName       = roleName;
            this.descriptionId  = descriptionId;
        }

        public String getDescription(Context context) {
            return context != null ? context.getString(descriptionId) : roleName;
        }

        public static WifiRole getWifiRoleForRoleName(String roleName) {
            for (WifiRole wifiRole : values()) {
                if (wifiRole.roleName.equals(roleName)) {
                    return wifiRole;
                }
            }
            TurrisLog.warning(WifiRole.class, String.format("Cannot find WIFI role for role name: %s -> using: %s", roleName, FALLBACK_ROLE_NAME.roleName));
            return FALLBACK_ROLE_NAME;
        }
    }

    private static final int        CLIENT_TABLE_HEADER_TEXT_PADDING    = 5;
    private static final Typeface   CLIENT_TABLE_HEADER_TEXT_TYPE       = Typeface.DEFAULT_BOLD;
    private static final int        CLIENT_TABLE_CLIENT_TEXT_GRAVITY    = Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL;

    private TextView    wifiRoleText;
    private TextView    wifiFrequencyText;
    private TextView    wifiChannelText;
    private TableLayout wifiClientsTable;


//////////////////////////// ANDROID LIFECYCLE

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.interface_info_fragment_wifi, container, false);

        this.wifiChannelText = (TextView) linearLayout.findViewById(R.id.interface_info_wifi_channel);
        this.wifiFrequencyText = (TextView) linearLayout.findViewById(R.id.interface_info_wifi_frequency);
        this.wifiRoleText = (TextView) linearLayout.findViewById(R.id.interface_info_wifi_role);
        this.wifiClientsTable = (TableLayout) linearLayout.findViewById(R.id.interface_info_wifi_clients);
        ((TextView) linearLayout.findViewById(R.id.interface_info_type)).setText(getText(R.string.wifi));

        return linearLayout;
    }


//////////////////////////// CONTENT REFRESH

    @Override
    protected void updateView(InterfaceListItemFactoryData.InterfaceListItem interfaceListItem) {
        if (isAdded()) {
            WifiInfoFactoryData.WifiInfo wifiInfo = interfaceListItem.getWifiInfo();

            this.wifiRoleText.setText(WifiRole.getWifiRoleForRoleName(wifiInfo.getRole()).getDescription(getActivity()));
            this.wifiChannelText.setText(wifiInfo.getChannel());
            this.wifiFrequencyText.setText(wifiInfo.getFrequency());

            updateClientsTable(wifiInfo);
        }
    }

    private void updateClientsTable(WifiInfoFactoryData.WifiInfo wifiInfo) {
        Context context = getActivity();
        wifiClientsTable.removeAllViews();

        TableRow headerTableRow = new TableRow(context);
        headerTableRow.addView(prepareHeaderTextView(context, getString(R.string.wifi_info_client_mac)));
        headerTableRow.addView(prepareHeaderTextView(context, getString(R.string.wifi_info_client_signal)));
        headerTableRow.addView(prepareHeaderTextView(context, getString(R.string.wifi_info_client_rx)));
        headerTableRow.addView(prepareHeaderTextView(context, getString(R.string.wifi_info_client_tx)));
        wifiClientsTable.addView(headerTableRow);

        for (WifiClientFactoryData.WifiClient client : wifiInfo.getWifiClients()) {
            TableRow wifiClientTableRow = new TableRow(context);
            wifiClientTableRow.addView(prepareClientTextView(context, client.getMac()));
            wifiClientTableRow.addView(prepareClientTextView(context, client.getSignal()));
            wifiClientTableRow.addView(prepareClientTextView(context, client.getRxBitrate()));
            wifiClientTableRow.addView(prepareClientTextView(context, client.getTxBitrate()));

            wifiClientsTable.addView(wifiClientTableRow);
        }
    }

    private TextView prepareHeaderTextView(Context context, String text) {
        TextView textView = new TextView(context);
        textView.setTypeface(CLIENT_TABLE_HEADER_TEXT_TYPE);
        textView.setPadding(CLIENT_TABLE_HEADER_TEXT_PADDING,
                            CLIENT_TABLE_HEADER_TEXT_PADDING,
                            CLIENT_TABLE_HEADER_TEXT_PADDING,
                            CLIENT_TABLE_HEADER_TEXT_PADDING);
        //TODO mates: use color and size from style
        textView.setTextColor(ContextCompat.getColor(getContext(), R.color.text_dark));
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size_tiny));
        textView.setText(text);

        return textView;
    }

    private TextView prepareClientTextView(Context context, String text) {
        TextView textView = new TextView(context);
        textView.setGravity(CLIENT_TABLE_CLIENT_TEXT_GRAVITY);
        //TODO mates: use color and size from style
        textView.setTextColor(ContextCompat.getColor(getContext(), R.color.text_light));
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size_small));
        textView.setText(text);

        return textView;
    }
}
