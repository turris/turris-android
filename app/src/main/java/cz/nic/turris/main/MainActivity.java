/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import org.greenrobot.eventbus.Subscribe;

import cz.nic.turris.R;
import cz.nic.turris.TurrisLog;
import cz.nic.turris.TurrisSettings;
import cz.nic.turris.utility.Utility;
import cz.nic.turris.about.AboutActivity;
import cz.nic.turris.utility.activity.AbstractNetconfServiceActivity;
import cz.nic.turris.utility.activity.AbstractTurrisActivity;
import cz.nic.turris.connectors.DataRefreshService;
import cz.nic.turris.intro.IntroActivity;
import cz.nic.turris.scanner.ScannerActivity;
import cz.nic.turris.connectors.NetconfControl;
import cz.nic.turris.connectors.event.NetconfCommandEvent;
import cz.nic.turris.database.RouterProfile;
import cz.nic.turris.main.menu.MenuDrawerFragment;
import cz.nic.turris.main.messages.MessagesFragment;
import cz.nic.turris.main.routerInfo.RouterInfoFragment;
import cz.nic.turris.notification.NotificationUtility;

public class MainActivity extends AbstractNetconfServiceActivity {


    public static final String ACTIVE_ROUTER_ID_KEY = "active_router_id";

    private Menu                                                toolbarMenu;
    private Toolbar                                             toolbar;


//////////////////////////// EVENTS

    public static class TurrisMenuActionEvent {

        private final long id;
        private final Fragment fragment;

        public TurrisMenuActionEvent(long id, Fragment fragment) {
            this.id = id;
            this.fragment = fragment;
        }

        public long getId() {
            return id;
        }

        public Fragment getFragment() {
            return fragment;
        }
    }

    public enum TurrisMenuDefinition {

        EXTENDED_INFO   (1, R.string.router_details,    RouterInfoFragment.class,   true,   true),
        MESSAGES        (2, R.string.messages,          MessagesFragment.class,     true,   true);

        public static final String                          MENU_DEFINITION_KEY = "turris_menu_definition";

        private int id;
        private final int                                   menuTitleId;
        private Class<? extends Fragment>                   fragmentClass;
        private final boolean                               selectable;
        private boolean                                     disabling;

        private static TurrisMenuDefinition                 activeMenu;

        TurrisMenuDefinition(int id, int menuTitleId, Class<? extends Fragment> fragmentClass, boolean selectable, boolean disabling) {
            this.id             = id;
            this.menuTitleId    = menuTitleId;
            this.fragmentClass  = fragmentClass;
            this.selectable     = selectable;
            this.disabling      = disabling;
        }

        public void switchToItem(AbstractTurrisActivity activity) {
            activity.getTurrisApplication().getEventBus().postSticky(new TurrisMenuActionEvent(getId(), getFragment()));
        }

        public int getMenuTitleId() {
            return menuTitleId;
        }

        public boolean isSelectable() {
            return selectable;
        }

        public boolean isSelected() {
            return this.equals(activeMenu);
        }

        public Fragment getFragment() {
            if (fragmentClass != null) {
                //noinspection TryWithIdenticalCatches
                try {
                    return fragmentClass.newInstance();
                } catch (InstantiationException e) {
                    TurrisLog.error(getClass(), "Cannot create instance of menu fragment: " + fragmentClass, e);
                } catch (IllegalAccessException e) {
                    TurrisLog.error(getClass(), "Cannot create instance of menu fragment: " + fragmentClass, e);
                }

            }
            return null;
        }

        public long getId() {
            return id;
        }

        public boolean isDisabling() {
            return disabling;
        }

        public static long getMaxId() {
            return TurrisMenuDefinition.values()[TurrisMenuDefinition.values().length - 1].getId();
        }

        public static TurrisMenuDefinition getTurrisMenuDefinitionForId(long id) {
            return getTurrisMenuDefinitionForId(id, false);
        }

        public static TurrisMenuDefinition getTurrisMenuDefinitionForId(long id, boolean useFallbackVariant) {
            for (TurrisMenuDefinition menuDefinition: TurrisMenuDefinition.values()) {
                if (menuDefinition.id == id) {
                    return menuDefinition;
                }
            }
            TurrisLog.error(TurrisMenuDefinition.class, "Cannot get TurrisMenuDefinition for id: " + id );
            if (useFallbackVariant) {
                TurrisLog.error(TurrisMenuDefinition.class, "using initial screen: " + TurrisSettings.INITIAL_MENU_ITEM);
                return TurrisSettings.INITIAL_MENU_ITEM;
            } else {
                return null;
            }
        }
    }


//////////////////////////// EVENT HANDLERS

	@Subscribe
	public void handleNetconfErrorEvent(DataRefreshService.NetconfErrorEvent netconfErrorEvent) {
		runOnUiThread(() -> Utility.SnackbarNotification.ERROR.showSnackbar(getApplicationContext(), findViewById(R.id.main_layout), getString(R.string.error) + netconfErrorEvent.getMessage(), getClass()).show());
	}

    @Subscribe
    public void handleRouterProfileActiveEvent(RouterProfile.RouterProfileActiveEvent routerProfileActiveEvent) {
        switchActiveRouter(routerProfileActiveEvent.getActiveRouterProfile(), routerProfileActiveEvent.isGoToInitialScreen(), false);
    }

    @Subscribe
    public void handleShowInfoFragmentControlEvent(TurrisMenuActionEvent menuDefinition) {
        runOnUiThread(() -> {
            Fragment fragment = menuDefinition.getFragment();
            if (fragment != null) {
                showFragmentInMainView(fragment);
                TurrisMenuDefinition.activeMenu = TurrisMenuDefinition.getTurrisMenuDefinitionForId(menuDefinition.getId());
            }
        });
    }

    @Subscribe
    public void handleNetconfServiceConnectionEvent(DataRefreshService.NetconfServiceConnectionEvent netconfServiceConnectionEvent) {
        runOnUiThread(() -> refreshToolbarMenu(netconfServiceConnectionEvent));
    }

    @Subscribe
    public void handleRouterProfileRenamedEvent(RouterProfile.RouterProfileRenamedEvent routerProfileRenamedEvent) {
        setToolbarActiveRouterName();
        NotificationUtility.updateNotification(this, routerProfileRenamedEvent.getRouterProfile().getId());
    }

    @Subscribe
    public void handleRouterProfileDeletedEvent(RouterProfile.RouterProfileDeletedEvent routerProfileDeletedEvent) {
        loadActiveRouter(null);
    }


//////////////////////////// UTILITY

    private Long getActiveRouterIdFromIntent(Intent intent) {
        if (intent == null) {
            return null;
        }
        return intent.getLongExtra(ACTIVE_ROUTER_ID_KEY, -1);
    }

    private boolean trySwitchToIntentMenuCommand(Intent intent) {
        if (intent == null) {
            return false;
        }
        TurrisMenuDefinition menuDefinitionForId = TurrisMenuDefinition.getTurrisMenuDefinitionForId(intent.getLongExtra(TurrisMenuDefinition.MENU_DEFINITION_KEY, -1));
        if (menuDefinitionForId == null) {
            return false;
        }
        menuDefinitionForId.switchToItem(this);
        return true;
    }

    private boolean trySwitchToInitialMenu(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            return false;
        }
        TurrisSettings.INITIAL_MENU_ITEM.switchToItem(this);
        return true;
    }

    private void showFragmentInMainView(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_scrollview, fragment);
        fragmentTransaction.commit();
    }

    private void registerEventBus() {
        if (!getTurrisApplication().getEventBus().isRegistered(this)) {
            getTurrisApplication().getEventBus().register(this);
        }
    }

    private void unregisterEventBus() {
        if (getTurrisApplication().getEventBus().isRegistered(this)) {
            getTurrisApplication().getEventBus().unregister(this);
        }
    }


//////////////////////////// ACTIVE ROUTER

    private void switchActiveRouter(RouterProfile router, final boolean goToInitialScreen, boolean saveActive) {
        if (router != null) {
            loadActiveRouterData(router, goToInitialScreen, saveActive);
        } else {
            loadActiveRouter(null);
        }
    }

    private void loadActiveRouterData(RouterProfile router, boolean goToInitialScreen, boolean saveActive) {
        if (router != null) {
            if (saveActive) router.setAsActive(getTurrisApplication(), goToInitialScreen);
            setToolbarActiveRouterName();
            if (goToInitialScreen) TurrisSettings.INITIAL_MENU_ITEM.switchToItem(this);
            getTurrisApplication().getEventBus().postSticky(new NetconfCommandEvent(DataRefreshService.NetConfServiceCommand.RECONNECT));
        }
    }

    private void loadActiveRouter(Long routerId) {
        if (checkLoadedRouter(RouterProfile.getRegisteredRouterWithId(routerId)) ||
            checkLoadedRouter(RouterProfile.getActiveRouter(this)) ||
            checkLoadedRouter(RouterProfile.getLastRegisteredRouter())) {

            TurrisLog.info(getClass(), "LOADED ACTIVE ROUTER");
        } else {
            TurrisLog.info(getClass(), "NO LAST ROUTER FOUND -> STARTING INTRO");
            startActivity(new Intent(this, IntroActivity.class));
            finish();
        }
    }

    private boolean checkLoadedRouter(RouterProfile router) {
        if (router != null) {
            switchActiveRouter(router, true, true);
            return true;
        } else {
            return false;
        }
    }


//////////////////////////// ANDROID LIFECYCLE

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);
        prepareToolbar();
        registerEventBus();
        loadActiveRouter(getActiveRouterIdFromIntent(getIntent()));

        if (trySwitchToIntentMenuCommand(getIntent())) return;
        trySwitchToInitialMenu(savedInstanceState);
	}

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        registerEventBus();
        RouterProfile routerProfile = RouterProfile.getRegisteredRouterWithId(getActiveRouterIdFromIntent(intent));
        if (routerProfile != null) {
            routerProfile.setAsActive(getTurrisApplication(), false);
        }
        trySwitchToIntentMenuCommand(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        refreshToolbarMenu();
        registerEventBus();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setToolbarActiveRouterName();
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterEventBus();
    }

    @Override
	protected void onDestroy() {
		super.onDestroy();
        unregisterEventBus();
	}


//////////////////////////// BACK BUTTON ACTIONS

    @Override
    public void onBackPressed() {
        BackButtonPriorityDefinition.backButtonPressed(this);
    }

    private enum BackButtonPriorityDefinition {
        CLOSE_MENU_DRAWER {
            @Override
            protected boolean doAction(MainActivity mainActivity) {
                if (MenuDrawerFragment.isDrawerOpened) {
                    mainActivity.getTurrisApplication().getEventBus().post(new MenuDrawerFragment.MenuDrawerActionEvent(false));
                    return true;
                }
                return false;
            }
        },
        GO_TO_INITIAL_SCREEN {
            @Override
            protected boolean doAction(MainActivity mainActivity) {
                if (!TurrisSettings.INITIAL_MENU_ITEM.isSelected()) {
                    TurrisSettings.INITIAL_MENU_ITEM.switchToItem(mainActivity);
                    return true;
                }
                return false;
            }
        },
        CLOSE_APP {
            @Override
            protected boolean doAction(MainActivity mainActivity) {
                mainActivity.finish();
                return true;
            }
        };

        protected abstract boolean doAction(MainActivity mainActivity);

        private static void backButtonPressed(MainActivity mainActivity) {
            for (BackButtonPriorityDefinition backButtonPriorityDefinition: values()) {
                if (backButtonPriorityDefinition.doAction(mainActivity)) {
                    return;
                }
            }
        }
    }


//////////////////////////// TOOLBAR MENU

    public enum ToolbarMenuDefinition {

        NEW_ROUTER      (1, R.string.new_router, MenuItem.SHOW_AS_ACTION_NEVER, false) {
            @Override
            public void itemAction(AbstractTurrisActivity activity) {
                activity.startActivity(new Intent(activity, ScannerActivity.class));
                activity.finish();
            }
        },
        RESTART         (2, R.string.restart, MenuItem.SHOW_AS_ACTION_NEVER, true) {
            @Override
            public void itemAction(AbstractTurrisActivity activity) {
                NetconfControl.reboot(activity, null);
            }
        },
        ABOUT           (3, R.string.about_screen, MenuItem.SHOW_AS_ACTION_NEVER, false) {
            @Override
            public void itemAction(AbstractTurrisActivity activity) {
                activity.startActivity(new Intent(activity, AboutActivity.class));
                activity.overridePendingTransition(R.anim.slide_in_right, R.anim.stay);
            }
        };

        private int                                         id;
        private final int                                   menuTitleId;
        private final int                                   showAsAction;
        private boolean                                     disabling;

        ToolbarMenuDefinition(int id, int menuTitleId, int showAsAction, boolean disabling) {
            this.id             = id;
            this.menuTitleId    = menuTitleId;
            this.showAsAction   = showAsAction;
            this.disabling      = disabling;
        }

        public abstract void itemAction(AbstractTurrisActivity activity);

        public static ToolbarMenuDefinition getTurrisMenuDefinitionForId(long id) {
            for (ToolbarMenuDefinition menuDefinition: values()) {
                if (menuDefinition.id == id) {
                    return menuDefinition;
                }
            }
            TurrisLog.error(TurrisMenuDefinition.class, "Cannot get ToolbarMenuDefinition for id: " + id );
            return null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.toolbarMenu = menu;
        for (ToolbarMenuDefinition toolbarMenuDefinition: ToolbarMenuDefinition.values()) {
            MenuItem menuItem = menu.add(Menu.NONE, toolbarMenuDefinition.id, toolbarMenuDefinition.ordinal(), toolbarMenuDefinition.menuTitleId);
            menuItem.setShowAsAction(toolbarMenuDefinition.showAsAction);
        }
        refreshToolbarMenu();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        ToolbarMenuDefinition toolbarMenuDefinition = ToolbarMenuDefinition.getTurrisMenuDefinitionForId(item.getItemId());
        if (toolbarMenuDefinition != null) {
            toolbarMenuDefinition.itemAction(this);
            return true;
        }
        return false;
    }

    private void refreshToolbarMenu() {
        refreshToolbarMenu(getTurrisApplication().getEventBus().getStickyEvent(DataRefreshService.NetconfServiceConnectionEvent.class));
    }

    private void refreshToolbarMenu(DataRefreshService.NetconfServiceConnectionEvent serviceConnectionEvent) {
        refreshToolbarMenu(serviceConnectionEvent != null && serviceConnectionEvent.isConnected());
    }

    private void refreshToolbarMenu(boolean connected) {
        for (ToolbarMenuDefinition toolbarMenuDefinition: ToolbarMenuDefinition.values()) {
            if (toolbarMenu != null) {
                MenuItem item = toolbarMenu.findItem(toolbarMenuDefinition.id);
                if (item != null) {
                    item.setEnabled(!toolbarMenuDefinition.disabling || connected);
                }
            }
        }
    }


//////////////////////////// TOOLBAR

    private void prepareToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void setToolbarActiveRouterName() {
        RouterProfile activeRouterProfile = RouterProfile.getActiveRouter(this);
        if (toolbar != null && activeRouterProfile != null) {
            toolbar.setTitle(activeRouterProfile.getName() + " (" + activeRouterProfile.getHostName() + ")");
        }
    }
}
