/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.main.routerInfo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import cz.nic.turris.R;
import cz.nic.turris.utility.Utility;
import cz.nic.turris.utility.activity.AbstractTurrisActivity;
import cz.nic.turris.utility.fragment.AbstractTimeChartFragment;
import cz.nic.turris.connectors.container.MemoryInfoFactoryData;
import cz.nic.turris.connectors.container.MemorySnapshotFactory;

public class RouterInfoFragmentChartMemory extends Fragment {

    private static final int        BUFFERED_MEMORY_CHART_COLOR     = R.color.chart_memory_buffered;
    private static final boolean    BUFFERED_MEMORY_CHART_FILLED    = false;
    private static final int        CACHED_MEMORY_CHART_COLOR       = R.color.chart_memory_cached;
    private static final boolean    CACHED_MEMORY_CHART_FILLED      = false;
    private static final int        FREE_MEMORY_CHART_COLOR         = R.color.chart_memory_free;
    private static final boolean    FREE_MEMORY_CHART_FILLED        = false;

    private static final int        KB_TO_MB_RATIO                  = 1000000;
    private static final int        CHART_LABELS_MAX_X_COUNT        = 3;


//////////////////////////// ABSTRACT CHART FRAGMENT

    public static class MemoryChartFragment extends AbstractTimeChartFragment {

        @Override
        protected int prepareTitle() {
            return R.string.memory_usage;
        }

        @Override
        protected int prepareChartMaxXCount() {
            return CHART_LABELS_MAX_X_COUNT;
        }

        @Override
        protected String getYAxisFormattedValues(float value) {
            return getResources().getString(R.string.chart_memory_y_unit, Utility.getOneDecimalFormattedNumber(value / KB_TO_MB_RATIO));
        }

        @Subscribe
        public void handleMemorySnapshotData(MemorySnapshotFactory.MemorySnapshot memorySnapshot) {
            updateChart(prepareChartDataList(memorySnapshot), true);
        }

        @Subscribe
        public void handleRouterMemoryInfoData(MemoryInfoFactoryData.MemoryInfo memoryInfo) {
            if (memoryInfo != null) {
                updateChartMaximum((long) memoryInfo.getTotal());
            }
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            MemorySnapshotFactory.MemorySnapshot memorySnapshot = ((AbstractTurrisActivity) getActivity()).getTurrisApplication().getEventBus().getStickyEvent(MemorySnapshotFactory.MemorySnapshot.class);
            if (memorySnapshot != null) {
                updateChart(prepareChartDataList(memorySnapshot), false);
            }
        }

        private List<ChartData> prepareChartDataList(@NonNull MemorySnapshotFactory.MemorySnapshot memorySnapshot) {
            return new ArrayList<ChartData>() {{
                add(new ChartData(getString(R.string.router_info_chart_memory_buffered), memorySnapshot.getBuffered(),   BUFFERED_MEMORY_CHART_COLOR,   BUFFERED_MEMORY_CHART_FILLED));
                add(new ChartData(getString(R.string.router_info_chart_memory_cached),   memorySnapshot.getCached(),     CACHED_MEMORY_CHART_COLOR,     CACHED_MEMORY_CHART_FILLED));
                add(new ChartData(getString(R.string.router_info_chart_memory_free),     memorySnapshot.getFree(),       FREE_MEMORY_CHART_COLOR,       FREE_MEMORY_CHART_FILLED));
            }};
        }

        @Override
        public void onResume() {
            super.onResume();
            ((AbstractTurrisActivity) getActivity()).getTurrisApplication().getEventBus().register(this);
        }

        @Override
        public void onPause() {
            super.onPause();
            ((AbstractTurrisActivity) getActivity()).getTurrisApplication().getEventBus().unregister(this);
        }
    }


//////////////////////////// ANDROID LIFECYCLE
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.router_info_memory_chart_placeholder,  new MemoryChartFragment());
            fragmentTransaction.commit();
        }
		return inflater.inflate(R.layout.router_info_fragment_charts_memory, container, false);
	}
}
