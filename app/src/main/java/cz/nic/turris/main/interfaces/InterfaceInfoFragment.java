/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.main.interfaces;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import cz.nic.turris.utility.fragment.AbstractInfoFragment;
import cz.nic.turris.connectors.container.InterfaceListItemFactoryData;

public class InterfaceInfoFragment extends AbstractInfoFragment {

    private static final String INTERFACE_NAME_BUNDLE_KEY = "interface_item";
    private static final String INTERFACE_TYPE_BUNDLE_KEY = "interface_type";

    private String                                                          interfaceName = null;
    private InterfaceListItemFactoryData.InterfaceListItem.InterfaceType    interfaceType = null;

    protected Fragment prepareTopFragment() {
        return InterfaceInfoFragmentStatistics.newInstance(interfaceName);
    }

    protected Fragment prepareMiddleFragment() {
        return InterfaceChartFragment.newInstance(interfaceName);
    }

    protected Fragment prepareBottomFragment() {
        return InterfaceTypeFragment.getInterfaceTypeFragmentForInterfaceType(interfaceType).newFragment(interfaceName);
    }


//////////////////////////// ANDROID LIFECYCLE

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            interfaceName = savedInstanceState.getString(INTERFACE_NAME_BUNDLE_KEY);
            interfaceType = InterfaceListItemFactoryData.InterfaceListItem.InterfaceType.getInterfaceTypeForNumber(savedInstanceState.getInt(INTERFACE_TYPE_BUNDLE_KEY));
        } else {
            interfaceName = getArguments().getString(INTERFACE_NAME_BUNDLE_KEY);
            interfaceType = InterfaceListItemFactoryData.InterfaceListItem.InterfaceType.getInterfaceTypeForNumber(getArguments().getInt(INTERFACE_TYPE_BUNDLE_KEY));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(INTERFACE_NAME_BUNDLE_KEY, interfaceName);
        outState.putInt(INTERFACE_TYPE_BUNDLE_KEY, interfaceType.getNumber());
    }


//////////////////////////// INTERFACE TYPE

    private enum InterfaceTypeFragment {

        BRIDGE_FRAGMENT     (InterfaceListItemFactoryData.InterfaceListItem.InterfaceType.BRIDGE,   InterfaceInfoFragmentBridge.class),
        WIRELESS_FRAGMENT   (InterfaceListItemFactoryData.InterfaceListItem.InterfaceType.WIRELESS, InterfaceInfoFragmentWifi.class),
        UNKNOWN_FRAGMENT    (InterfaceListItemFactoryData.InterfaceListItem.InterfaceType.GENERIC,  InterfaceInfoFragmentUnknown.class);

        private final InterfaceListItemFactoryData.InterfaceListItem.InterfaceType interfaceType;
        private Class<? extends AbstractInterfaceFragment> interfaceInfoFragmentClass;

        InterfaceTypeFragment(InterfaceListItemFactoryData.InterfaceListItem.InterfaceType interfaceType, Class<? extends AbstractInterfaceFragment> interfaceInfoFragmentClass) {
            this.interfaceType = interfaceType;
            this.interfaceInfoFragmentClass = interfaceInfoFragmentClass;
        }

        Fragment newFragment(String interfaceName) {
            return AbstractInterfaceFragment.newInstance(interfaceInfoFragmentClass, interfaceName);
        }

        public static InterfaceTypeFragment getInterfaceTypeFragmentForInterfaceType(InterfaceListItemFactoryData.InterfaceListItem.InterfaceType interfaceType) {
            for (InterfaceTypeFragment interfaceTypeFragment: values()) {
                if (interfaceTypeFragment.interfaceType.equals(interfaceType)) {
                    return interfaceTypeFragment;
                }
            }
            return UNKNOWN_FRAGMENT;
        }
    }


//////////////////////////// INSTANCE ACCESS

    public static InterfaceInfoFragment newInstance(String interfaceName, InterfaceListItemFactoryData.InterfaceListItem.InterfaceType interfaceType) {
        InterfaceInfoFragment fragment = new InterfaceInfoFragment();
        Bundle bundle = new Bundle();
        bundle.putString(INTERFACE_NAME_BUNDLE_KEY, interfaceName);
        bundle.putInt(INTERFACE_TYPE_BUNDLE_KEY, interfaceType.getNumber());
        fragment.setArguments(bundle);
        return fragment;
    }
}
