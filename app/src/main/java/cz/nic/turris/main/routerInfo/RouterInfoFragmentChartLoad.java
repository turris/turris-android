/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.turris.main.routerInfo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.Subscribe;

import java.util.Collections;
import java.util.List;

import cz.nic.turris.R;
import cz.nic.turris.utility.Utility;
import cz.nic.turris.utility.activity.AbstractTurrisActivity;
import cz.nic.turris.utility.fragment.AbstractTimeChartFragment;
import cz.nic.turris.connectors.container.LoadSnapshotFactory;

public class RouterInfoFragmentChartLoad extends Fragment {

    private static final int        LOAD_CHART_COLOR                = R.color.chart_color_3;
    private static final boolean    LOAD_CHART_FILLED               = true;
    private static final long       LOAD_CHART_MAXIMUM_VALUE        = 100L;


//////////////////////////// ABSTRACT CHART FRAGMENT

    public static class LoadChartFragment extends AbstractTimeChartFragment {

        @Override
        protected int prepareTitle() {
            return R.string.average_load;
        }

        @Override
        protected String getYAxisFormattedValues(float value) {
            return getResources().getString(R.string.chart_load_y_unit, Utility.getOneDecimalFormattedNumber(value));
        }

        @Subscribe
        public void handleLoadSnapshotData(LoadSnapshotFactory.LoadSnapshot loadSnapshot) {
            updateChart(prepareChartDataList(loadSnapshot), true);
        }

        @Override
        protected Long prepareChartMaximum() {
            return LOAD_CHART_MAXIMUM_VALUE;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            LoadSnapshotFactory.LoadSnapshot loadSnapshot = ((AbstractTurrisActivity) getActivity()).getTurrisApplication().getEventBus().getStickyEvent(LoadSnapshotFactory.LoadSnapshot.class);
            if (loadSnapshot != null) {
                updateChart(prepareChartDataList(loadSnapshot), false);
            }
        }

        private List<ChartData> prepareChartDataList(@NonNull LoadSnapshotFactory.LoadSnapshot loadSnapshot) {
            return Collections.singletonList(new ChartData(getString(R.string.router_info_load), loadSnapshot.getLoad(), LOAD_CHART_COLOR, LOAD_CHART_FILLED));
        }

        @Override
        public void onResume() {
            super.onResume();
            ((AbstractTurrisActivity) getActivity()).getTurrisApplication().getEventBus().register(this);
        }

        @Override
        public void onPause() {
            super.onPause();
            ((AbstractTurrisActivity) getActivity()).getTurrisApplication().getEventBus().unregister(this);
        }
    }


//////////////////////////// ANDROID LIFECYCLE
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.router_info_load_chart_placeholder, new LoadChartFragment());
            fragmentTransaction.commit();
        }
		return inflater.inflate(R.layout.router_info_fragment_charts_load, container, false);
	}
}
